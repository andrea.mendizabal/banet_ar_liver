# banet_AR_liver

## Description
This repository contains all the scripts needed for running BA-Net 2.0. It is divided in three folders :
- The BA-Net 2.0 architecture and training relying on the DeepPhysX project [](https://github.com/mimesis-inria/DeepPhysX)
- The FEM synthetic data generation scripts relying on the SpringSimulation project [](https://gitlab.com/altairLab/deformable-simulation/springsimulation) and the SOFA framework [](https://github.com/sofa-framework/sofa)
- RGB-D point clouds of a human liver during an open surgery acquired at Paul Brousse Hospital (Paris, France)

## Usage

## Support
If you need help, do not hesitate to contact us by email at andrea.mendizabal.echeverria@gmail.com

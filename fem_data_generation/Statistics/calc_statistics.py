import numpy as np
from vtk import *
from vtk.util import numpy_support
import os
import math
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),"../Utils"))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),"../Voxelization"))
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from generalutils import *
from vtkutils import *
import voxelize_displacement
from SurfaceExtraction import revert_surface_noise

def statsForSample( curDir, sampleID, volume, deformedNames, partialDeformedNames, denoise=True ):
    """
    Given the sample data, create a dictionary holding key information about this sample.

    Arguments:
    ----------
    curDir (str):
        Path to current directory
    sampleID (int):
        Current sample number
    volume (vtkUnstructuredGrid):
        Preoperative volume of the mesh
    deformedNames (list of str):
        List of filenames for the intraoperative, deformed volume/s
    partialDeformedNames (list of str):
        List of filenames intraoperative partial surface/s
    denoise (bool):
        If the amount of visible surface has not been already saved at surface extraction time,
        estimated the percentage of visible surface by first removing noise. If false, denoising 
        is not applied, which usually leads to an overestimation of visible surface. Default: true.

    Returns:
    ----------
    dict:
        Statistical values (for this single sample) as a dictionary.
    """
    stats = {}
    maxDisplList = []
    meanDisplList = []
    partialSurfaceAreaList = []
    partialSurfacePointsList = []

    num_frames = len(deformedNames)

    # Load precomputed visible surface amount
    computeVisible = False
    try:
        visibleStatsFile   = Config["intraoperative_surface"]["visible_stats_filename"]
        visibleSurfaceFile = os.path.join( curDir, visibleStatsFile )
        partialSurfaceAreaList.append(np.loadtxt( visibleSurfaceFile ).tolist())

        assert len(partialSurfaceAreaList) == num_frames
    except:
        computeVisible = True

    # Load deformed mesh and extracted surface
    deformedVolumes = [0]*num_frames
    partialDeformedSurfaces = [0]*num_frames
    for n in range(num_frames):
        deformedVolumes[n] = loadMesh( os.path.join( curDir, deformedNames[n] ) )
        partialDeformedSurfaces[n] = loadMesh( os.path.join( curDir, partialDeformedNames[n] ) )
    
    # Pre-pend volume at rest
    deformedVolumes.insert(0, volume)

    # Loop on the deformed states
    for n in range(num_frames):
        # Compute displacement wrt previous frame
        displacementSum = 0
        maxDisplacement = 0
        displacementMesh = voxelize_displacement.calcDisplacement( deformedVolumes[n], deformedVolumes[n+1] )
        displacement = displacementMesh.GetPointData().GetArray("displacement")
        for i in range(displacement.GetNumberOfTuples()):
            displ = displacement.GetTuple3( i )
            mag = math.sqrt( displ[0]**2 + displ[1]**2 + displ[2]**2 )
            displacementSum += mag
            if mag > maxDisplacement:
                maxDisplacement = mag
        
        maxDisplList.append( maxDisplacement )
        meanDisplList.append( displacementSum/displacement.GetNumberOfTuples() )

        # Compute visible surface if needed
        if computeVisible:
            surface = extractSurface( deformedVolumes[n+1] )
            fullSurfaceArea = surfaceArea( surface )

            # Make a copy of the partial surface where the noise is reoved. If the 
            # partial surface wasn't noisy in the first place, this shouldn't do much.
            if denoise:
                denoisedPartialSurface = revert_surface_noise.revertSurfaceNoise( surface,
                                partialDeformedSurfaces[n] )

                if Config["io"]["save_debug_data"]:
                    writer = vtkXMLPolyDataWriter()
                    writer.SetFileName( os.path.join( curDir, "noisyPartialSurface.vtp" ) )
                    writer.SetInputData( maxPartialDefSurface )
                    writer.Update()
                    writer = vtkXMLUnstructuredGridWriter()
                    writer.SetFileName( os.path.join( curDir, "denoisedPartialSurface.vtu" ) )
                    writer.SetInputData( denoisedPartialSurface )
                    writer.Update()
                
                partialSurfaceArea = surfaceArea( denoisedPartialSurface )
            else:
                partialSurfaceArea = surfaceArea( partialDeformedSurfaces[n] )
            partialSurfaceAreaList.append( partialSurfaceArea/fullSurfaceArea )

        partialSurfacePointsList.append( partialDeformedSurfaces[n].GetNumberOfPoints() )
    

    if num_frames > 1:
        # Calculate the maximal and average displacement for the max deformation (last one)
        displacementMesh = voxelize_displacement.calcDisplacement( deformedVolumes[0], deformedVolumes[-1] )
        displacement = displacementMesh.GetPointData().GetArray("displacement")
        displacementSum = 0
        maxDisplacement = 0
        # Iterate over displacemnt of every point:
        for i in range(displacement.GetNumberOfTuples()):
            displ = displacement.GetTuple3( i )
            mag = math.sqrt( displ[0]**2 + displ[1]**2 + displ[2]**2 )
            displacementSum += mag
            if mag > maxDisplacement:
                maxDisplacement = mag
        stats["MaxDisplacement"] = maxDisplacement
        stats["MeanDisplacement"] = displacementSum/displacement.GetNumberOfTuples()

        # Save statistics per frame
        stats["MaxDisplacementPerFrame"]  = maxDisplList
        stats["MeanDisplacementPerFrame"] = meanDisplList
        stats["VisibleSurfacePerFrame"]   = partialSurfaceAreaList
        stats["VisiblePointsPerFrame"]    = partialSurfacePointsList
    else:
        stats["MaxDisplacement"] = maxDisplList[0]
        stats["MeanDisplacement"] = meanDisplList[0]

    stats["VisibleSurfaceAmount"] = partialSurfaceAreaList[-1]
    stats["VisiblePointsAmount"] = partialDeformedSurfaces[-1].GetNumberOfPoints()

    surface = extractSurface( deformedVolumes[-1] )
    fullSurfaceArea = surfaceArea( surface )
    stats["SurfaceArea"] = fullSurfaceArea
    
    # Compute statistics relative to stiffness data
    if deformedVolumes[-1].GetPointData().HasArray("stiffness"):
        stiffness = deformedVolumes[-1].GetPointData().GetArray("stiffness")
        stiffness = numpy_support.vtk_to_numpy(stiffness)

        # Percentage of springs wrt the total number of surface points
        nonzero_stiffness = np.ravel(np.nonzero(stiffness))
        numSprings = len(nonzero_stiffness) 
        stiffnessValues = np.unique(stiffness[nonzero_stiffness])
        numOccurrences = np.array([])
        for val in stiffnessValues:
            num = np.count_nonzero(stiffness == val) / numSprings
            numOccurrences = np.append(numOccurrences, num)
        # Spring values and relative occurrences (wrt number of springs)
        stiffnessAndAmount = np.append(stiffnessValues, numOccurrences)
        stiffnessAndAmount = np.reshape(stiffnessAndAmount, (-1,2)).T
        stats["numSprings"] = numSprings / surface.GetNumberOfPoints()
        stats["stiffnessAndAmount"] = stiffnessAndAmount.tolist()

    stats["sampleID"] = sampleID

    return stats
    
def globalStatistics( statsList ):

    assert len(statsList) > 0, "Global statistics can only be calculated if at least one sample is given!"
    meanDisplacementSum = 0
    visibleSurfaceAmountSum = 0
    visiblePointsAmountSum = 0
    maxDisplacement = 0
    meanMaxDisplacementSum = 0
    maxDisplacementID = -1
    maxDisplPerFrame = 0
    meanNumSprings = 0
    maxNumSprings = 0
    maxNumSpringsID = -1
    maxStiffness = 0
    meanAreaSum = 0
    maxArea = 0
    minArea = 1
    minAreaID = -1

    for i, s in enumerate(statsList):
        meanDisplacementSum += s["MeanDisplacement"]
        meanMaxDisplacementSum += s["MaxDisplacement"]
        visibleSurfaceAmountSum += s["VisibleSurfaceAmount"]
        visiblePointsAmountSum += s["VisiblePointsAmount"]
        
        if s["MaxDisplacement"] > maxDisplacement:
            maxDisplacement = s["MaxDisplacement"]
            maxDisplacementID = s["sampleID"]
        if "SurfaceArea" in s:
            meanAreaSum += s["SurfaceArea"]
            if s["SurfaceArea"] > maxArea:
                maxArea = s["SurfaceArea"]
                maxAreaID = s["sampleID"]
            if s["SurfaceArea"] < minArea:
                minArea = s["SurfaceArea"]
                minAreaID = s["sampleID"]
        if "MaxDisplacementPerFrame" in s:
            for mdf in s["MaxDisplacementPerFrame"]:
                maxDisplPerFrame += mdf
        if "numSprings" in s:
            meanNumSprings += s["numSprings"]
            if s["numSprings"] > maxNumSprings:
                maxNumSprings = s["numSprings"]
                maxNumSpringsID = s["sampleID"]
            for k in range(len(s["stiffnessAndAmount"])):
                if s["stiffnessAndAmount"][k][0] > maxStiffness:
                    maxStiffness = s["stiffnessAndAmount"][k][0]

    meanDisplacement = meanDisplacementSum/len(statsList)
    meanMaxDisplacement = meanMaxDisplacementSum/len(statsList)
    meanVisibleSurfaceAmount = visibleSurfaceAmountSum/len(statsList)
    meanVisiblePointsAmount = visiblePointsAmountSum/len(statsList)
    meanNumSprings = meanNumSprings/len(statsList)
    meanArea = meanAreaSum/len(statsList)

    gStats = {
                "MaxDisplacement":maxDisplacement,
                "MaxDisplacementID":maxDisplacementID,
                "MeanDisplacement":meanDisplacement,
                "MeanMaxDisplacement":meanMaxDisplacement,
                "MeanVisibleSurfaceAmount":meanVisibleSurfaceAmount,
                "MeanVisiblePointsAmount":meanVisiblePointsAmount,
                }

    if "SurfaceArea" in statsList[0]:
        gStats["MeanSurfaceArea"]=meanArea
        gStats["MaxSurfaceArea"]=maxArea
        gStats["MaxSurfaceAreaID"]=maxAreaID
        gStats["MinSurfaceArea"]=minArea
        gStats["MinSurfaceAreaID"]=minAreaID

    if "numSprings" in statsList[0]:
        gStats["MeanNumSprings"]=meanNumSprings
        gStats["MaxNumSprings"]=maxNumSprings
        gStats["MaxNumSpringsID"]=maxNumSpringsID
        gStats["MaxStiffness"]=maxStiffness

    if "MaxDisplacementPerFrame" in statsList[0]:
        meanMaxDisplacementPerFrame = maxDisplPerFrame / ( len(statsList)*len(statsList[0]["MaxDisplacementPerFrame"]))
        gStats["MeanMaxDisplacementPerFrame"] = meanMaxDisplacementPerFrame

    return gStats

def printGlobalStats( gStats ):

    print("Mean displacement over all samples: {:.4f} m".format(
        gStats["MeanDisplacement"] ))

    print("Maximum displacement: {:.4f} m (in sample {})".format(
        gStats["MaxDisplacement"], gStats["MaxDisplacementID"]))

    print("Mean MAX displacement over all samples: {:.4f} m".format(
        gStats["MeanMaxDisplacement"] ))

    print("Mean visible surface: {:.2f}%".format(
        gStats["MeanVisibleSurfaceAmount"]*100))
    
    print("Mean num points in the visible surface: {:.0f}".format(
        gStats["MeanVisiblePointsAmount"]))
    
    if "MeanSurfaceArea" in gStats:
        print("Mean surface area: {:.2f} m2".format(
            gStats["MeanSurfaceArea"]))
        
        print("Max surface area: {:.4f} m2 (in sample {})".format(
            gStats["MaxSurfaceArea"], gStats["MaxSurfaceAreaID"]))
        
        print("Min surface area: {:.4f} m2 (in sample {})".format(
            gStats["MinSurfaceArea"], gStats["MinSurfaceAreaID"]))

    if "MeanNumSprings" in gStats:
        print("Mean number of springs over the surface over all samples: {:.2f}%".format(
        gStats["MeanNumSprings"]*100 ))

        print("Maximum number of springs over the surface: {:.2f}% (in sample {})".format(
        gStats["MaxNumSprings"]*100, gStats["MaxNumSpringsID"] ))

    if "MeanMaxDisplacementPerFrame" in gStats:
        print("Mean MAX displacement per frame: {:.4f}".format(gStats["MeanMaxDisplacementPerFrame"]))


def mergeStats( statsList, prevStats={} ):
    """
    Merge all the statistics generated from individual folders into a single
    dictionary for quicker access.
    """

    stats = {}
    for s in statsList:
        sampleID = s["sampleID"]
        stats[sampleID] = s
    return stats



import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter

def plotSurfaceAmounts( stats, outputFolder, histProps=None):
    
    visAmountsAll = [s["VisibleSurfaceAmount"]*100 for s in stats]
    visAmountsPruned = []
    skippedSamples = 0
    for v in visAmountsAll:
        if v <= 100:
            visAmountsPruned.append( v )
        else:
            skippedSamples += 1

    # TODO: Avoid this even happening, by checking earlier on:
    if skippedSamples > 0:
        print("WARNING: {} samples were skipped because their visible surface amount was larger than 100% (probably due to an unstable simulation).".format(skippedSamples))

#    maxVisAmount = -1
#    maxVisSample = -1
#    for d in stats:
#        if d["VisibleSurfaceAmount"] > maxVisAmount:
#            maxVisAmount = d["VisibleSurfaceAmount"]
#            maxVisSample = d["sampleID"]
#    print(maxVisAmount, maxVisSample) 

    plt.clf()
    plt.figure(figsize=(8,3))
    if histProps:
        plt.hist(visAmountsPruned, **histProps)
    else:
        plt.hist(visAmountsPruned)
    plt.xlabel( "Visible amount (%)" )
    plt.ylabel( "Number of samples" )
    plt.savefig( os.path.join( outputFolder, "VisibleAmountsHistogramm.png"), bbox_inches='tight')

def plotMaxDisplacement( stats, outputFolder , histProps=None):
    
    maxDispl = [s["MaxDisplacement"] for s in stats]
    plt.clf()
    plt.figure(figsize=(8,3))
    if histProps:
        plt.hist(maxDispl, **histProps)
    else:
        plt.hist(maxDispl)
    plt.xlabel( "Maximum displacement (m)" )
    plt.ylabel( "Number of samples" )
    plt.savefig( os.path.join( outputFolder, "MaxDisplacementHistogramm.png"), bbox_inches='tight')

def plotMeanDisplacement( stats, outputFolder, histProps=None):
    
    meanDispl = [s["MeanDisplacement"] for s in stats]
    plt.clf()
    plt.figure(figsize=(8,3))
    if histProps:
        plt.hist(meanDispl, **histProps)
    else:
        plt.hist(meanDispl)
    plt.xlabel( "Mean displacement (m)" )
    plt.ylabel( "Number of samples" )
    plt.savefig( os.path.join( outputFolder, "MeanDisplacementHistogramm.png"), bbox_inches='tight')

def plotMaxDisplFrame( stats, outputFolder, histProps=None):
    
    meanDispl = [f for s in stats for f in s["MaxDisplacementPerFrame"]]
    plt.clf()
    plt.figure(figsize=(8,3))
    if histProps:
        plt.hist(meanDispl, **histProps)
    else:
        plt.hist(meanDispl)
    plt.xlabel( "Max displacement per frame (m)" )
    plt.ylabel( "Number of samples" )
    plt.savefig( os.path.join( outputFolder, "MaxDisplPerFrameHistogramm.png"), bbox_inches='tight')

def plotNumVisiblePoints( stats, outputFolder, histProps=None):
    
    meanPoints = [s["VisiblePointsAmount"] for s in stats]
    plt.clf()
    plt.figure(figsize=(8,3))
    if histProps:
        plt.hist(meanPoints, **histProps)
    else:
        plt.hist(meanPoints)
    plt.xlabel( "Mean number of visible points" )
    plt.ylabel( "Number of samples" )
    plt.savefig( os.path.join( outputFolder, "VisiblePointsHistogramm.png"), bbox_inches='tight')

def plotNumSprings( stats, outputFolder, histProps=None ):
    
    visAmountsAll = [s["numSprings"]*100 for s in stats]
    visAmountsPruned = []
    skippedSamples = 0
    for v in visAmountsAll:
        if v <= 100:
            visAmountsPruned.append( v )
        else:
            skippedSamples += 1

    # TODO: Avoid this even happening, by checking earlier on:
    if skippedSamples > 0:
        print("WARNING: {} samples were skipped because their visible surface amount was larger than 100% (probably due to an unstable simulation).".format(skippedSamples))

    plt.clf()
    plt.figure(figsize=(8,3))
    if histProps:
        plt.hist(visAmountsPruned, **histProps)
    else:
        plt.hist(visAmountsPruned)
    plt.xlabel( "Springs amount (%)" )
    plt.ylabel( "Number of samples" )
    plt.savefig( os.path.join( outputFolder, "SpringsAmountsHistogramm.png"), bbox_inches='tight')

def plotSurfaceArea( stats, outputFolder, histProps=None):
    
    totSurface = [s["SurfaceArea"] for s in stats]

    plt.clf()
    plt.figure(figsize=(8,3))
    if histProps:
        plt.hist(totSurface, **histProps)
    else:
        plt.hist(totSurface)
    plt.xlabel( "Total surface area (m2)" )
    plt.ylabel( "Number of samples" )
    plt.savefig( os.path.join( outputFolder, "SurfaceAreaHistogramm.png"), bbox_inches='tight')

def plotSurfaceAmountsAndArea( stats, outputFolder, histProps=None):
    
    totSurface = [s["SurfaceArea"] for s in stats]
    visAmountsAll = [s["VisibleSurfaceAmount"]*100 for s in stats]
    
    #visAmountsPruned = []
    #skippedSamples = 0
    # for v in visAmountsAll:
    #     if v <= 100:
    #         visAmountsPruned.append( v )
    #     else:
    #         skippedSamples += 1

    # # TODO: Avoid this even happening, by checking earlier on:
    # if skippedSamples > 0:
    #     print("WARNING: {} samples were skipped because their visible surface amount was larger than 100% (probably due to an unstable simulation).".format(skippedSamples))

    plt.clf()
    plt.figure(figsize=(8,3))
    plt.scatter(totSurface, visAmountsAll)
    plt.xlabel( "Total surface area (m2)" )
    plt.ylabel( "Visible amount (%)" )
    plt.savefig( os.path.join( outputFolder, "VisibleAmountsVsArea.png"), bbox_inches='tight')

def plotMaxDisplacementAndArea( stats, outputFolder, histProps=None):
    
    totSurface = [s["SurfaceArea"] for s in stats]
    maxDispl = [s["MaxDisplacement"] for s in stats]

    #visAmountsPruned = []
    #skippedSamples = 0
    # for v in visAmountsAll:
    #     if v <= 100:
    #         visAmountsPruned.append( v )
    #     else:
    #         skippedSamples += 1

    # # TODO: Avoid this even happening, by checking earlier on:
    # if skippedSamples > 0:
    #     print("WARNING: {} samples were skipped because their visible surface amount was larger than 100% (probably due to an unstable simulation).".format(skippedSamples))

    plt.clf()
    plt.figure(figsize=(8,3))
    plt.scatter(totSurface, maxDispl)
    plt.xlabel( "Total surface area (m2)" )
    plt.ylabel( "Maximum displacement (m)" )
    plt.savefig( os.path.join( outputFolder, "MaxDisplVsArea.png"), bbox_inches='tight')
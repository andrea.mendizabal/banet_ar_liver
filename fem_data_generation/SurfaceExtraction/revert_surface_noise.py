from vtk import *
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),"../Utils"))
from vtkutils import *
from generalutils import *

def revertSurfaceNoise( originalFullSurface, partialNoisySurface ):
    """
    Take a noisy partial surface and try to estimate a "de-noised" partial surface.
    This is done by taking each point of the noisy partial surface, finding the nearest
    neighbor in the full original surface and afterwards discarding all original surface
    points which were not the nearest neighbor to any of the noisy points. What's left is an
    approximation for the partial surface, without noise.
    The goal of this is to get a better approximation of what amount of the original surface
    was used as a partial surface.
    """

    locator = vtkPointLocator()
    locator.SetDataSet( originalFullSurface )
    locator.BuildLocator()

    hit = vtkFloatArray()
    hit.SetName( "hit" )
    hit.SetNumberOfComponents(1)
    hit.SetNumberOfTuples( originalFullSurface.GetNumberOfPoints() )
    hit.Fill( 0 )
    originalFullSurface.GetPointData().AddArray( hit )

    for i in range( partialNoisySurface.GetNumberOfPoints() ):
        p = partialNoisySurface.GetPoint( i )
        nearestPointID = locator.FindClosestPoint( p )
        if nearestPointID >= 0 and nearestPointID < hit.GetNumberOfTuples():
            hit.SetTuple1( nearestPointID, 1 )

    thresh = vtkThreshold()
    thresh.SetInputData( originalFullSurface )
    thresh.SetInputArrayToProcess( 0,0,0,
            vtkDataObject.FIELD_ASSOCIATION_POINTS, "hit" )
    thresh.ThresholdByUpper( 0.5 ) # Everything lower than 0.5 will be removed
    thresh.AllScalarsOff()
    thresh.Update()

    partialDenoisedSurface = thresh.GetOutput()

    #print("Difference in surface amounts:\n\tNoisy: {}\n\tDenoised: {}".format(
    #    surfaceArea( partialNoisySurface ), surfaceArea( partialDenoisedSurface ) ))

    return partialDenoisedSurface




import random
import numpy as np
from vtk import *
import os
import math
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),"../Utils"))
from generalutils import *
from vtkutils import *

def randomSurface( fullSurface, wDistance=1, wNormal=1, wNoise=1, surfaceAmount=None, centerPointID=None ):
    """
    Extract a random part of a surface mesh. Starting from a (random) center node c, the
    surrounding points p are assigned three values:
        - Geodesic distance from the center node c
        - Angle difference between the normal of p and the normal of c
        - Random perlin noise value, sampled at the positon p.
    From these three values, we build a weighted sum, which acts as the likelyhood of a point
    being removed. We then select a threshold and remove all points whose likelyhood exceeds
    this threshold. The remaining points are the selected surface.
    The weights in the weighted sum can be used to influence whether the geodesic distance,
    the normals or the random perlin noise value should have a higher influence on the
    removal.

    Arguments:
    ----------
    fullSurface (vtkPolyData):
        Full surface. A part of this mesh will be returned. Point IDs and Cell IDs will not
        be kept.
    wDistance (float):
        Influence of the geodesic distance of a point to the center point c.
        If this value is > 0 and the other weights are == 0, only the distance will be
        taken into account.
    wNormal (float):
        Influece of the angle between normals.
        If this value is > 0 and the other weights are == 0, only points with a similar
        normal as the center point will be selected.only points with a similar
        normal as the center point will be selected.
    wNoise (float):
        Influence of the highest noise.
        If this value is > 0 and the other weights are == 0, entirely random parts of the
        surface will be selected.
    surfaceAmount (float):
        Amount of the surface which we want to select. If None, a random amount
        between 0 and 1 will be selected.
        Valid range: (0,1)
    centerPointID (int):
        Index of the center point c. If None, a random index of all the surface indices will be selected.

    Returns:
    ----------
    vtkPolyData
        Describes the part of the fullSurface which was selected
    """

    # Decide how many points we want to select:
    if surfaceAmount is None:
        surfaceAmount = random.random()
    assert surfaceAmount <= 1 and surfaceAmount > 0, "surfaceAmount must be between 0 and 1."

    pointsToSelect = surfaceAmount*fullSurface.GetNumberOfPoints()

    
    fullSurface = generatePointNormals( fullSurface )
    normals = fullSurface.GetPointData().GetArray( "Normals" )

    # Store the IDs of all triangles:
    #triangleIDs = []
    #for i in range(0,fullSurface.GetNumberOfCells()):
    #    if fullSurface.GetCell(i).GetCellType() == VTK_TRIANGLE:
    #        triangleIDs.append(i)

    # Select a random point on the surface around which to center the selected part, if it is not provided:
    if centerPointID is None:
        centerPointID = random.randint(0,fullSurface.GetNumberOfPoints()-1)
    centerPoint = fullSurface.GetPoint( centerPointID )

    distance = geodesicDistance( fullSurface, centerPointID )
    fullSurface.GetPointData().AddArray( distance )


    # Get normal of that point:
    centerPointNormal = normals.GetTuple3( centerPointID )


    # Decrease with:
    # - distance from center point
    # - normal difference
    # - perlin noise

    noise = vtkPerlinNoise()
    noise.SetFrequency( 15, 15, 15 )
    noise.SetPhase( random.random()*150, random.random()*150, random.random()*150 )

    # Create an array which will be filled and then used for thresholding
    likelyhood = vtkDoubleArray()
    likelyhood.SetNumberOfComponents(1)
    likelyhood.SetNumberOfTuples(fullSurface.GetNumberOfPoints())
    likelyhood.SetName("likelyhood")

    minVal = 99999
    maxVal = -1
    for i in range( fullSurface.GetNumberOfPoints() ):
        pt = fullSurface.GetPoint( i )
        dist = math.sqrt( vtkMath.Distance2BetweenPoints(centerPoint, pt) )

        normal = normals.GetTuple3( i )
        dot = vtkMath.Dot( centerPointNormal, normal )
        normalAng = math.acos( max( min( dot, 1 ), -1 ) )

        rnd = abs(noise.EvaluateFunction( pt ))

        curLikelyhood = wDistance*dist + wNormal*normalAng + wNoise*rnd
        likelyhood.SetTuple1( i, curLikelyhood )
        minVal = min( minVal, curLikelyhood )
        maxVal = max( maxVal, curLikelyhood )

    #print("Likelyhood range:", minVal, maxVal)

    # Build histogramm of likelyhoods:
    histBins = 50
    hist = [0]*histBins
    for i in range( fullSurface.GetNumberOfPoints() ):
        l = likelyhood.GetTuple1( i )
        curBin = int(l/maxVal*(histBins-1))
        hist[curBin] += 1
   
    # Find out where to set the threshold so that surfaceAmount points are selected.
    # We do this by going through the histogram and summing up the values in the bins. As
    # soon as more than surfaceAmount points are selected, 
    thresholdedPoints = 0
    threshold = maxVal  # Start with default of selecting everything
    for i in range( histBins ):
        thresholdedPoints += hist[i]
        if thresholdedPoints >= pointsToSelect:
            threshold = (i+1)/histBins*maxVal
            break
    #print("Selected threshold", threshold)

    fullSurface.GetPointData().AddArray( likelyhood )

    likelyhoodRange = maxVal - minVal

    #rndThreshold = (random.random()*0.75 + 0.25)*likelyhoodRange + minVal

    thresh = vtkThreshold()
    thresh.SetInputData( fullSurface )
    thresh.SetInputArrayToProcess( 0,0,0,
            vtkDataObject.FIELD_ASSOCIATION_POINTS, "likelyhood" )
    thresh.ThresholdBetween( 0, threshold )
    thresh.Update()

    # Write resulting surface to file:
    # Debug output:
    # writer = vtkXMLPolyDataWriter()
    # writer.SetFileName( "partialSurfaceLikelyhood.vtp" )
    # writer.SetInputData( fullSurface )
    # writer.Update()

    partialSurface = unstructuredGridToPolyData( thresh.GetOutput() )

    fullArea = surfaceArea( fullSurface )
    partialArea = surfaceArea( partialSurface )

    #print( "Original area:", fullArea )
    #print( "Partial area:", partialArea )
    #print( "Selected amount: {:.2f}% (Target was {:.2f}%)".format( 100*partialArea/fullArea,
    #    100*surfaceAmount ) )

    return partialSurface


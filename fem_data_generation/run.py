import argparse
import sys
import os
import subprocess
import shutil
import yaml
import copy
import time
import traceback
from flatten_dict import flatten, unflatten
import numpy as  np
from Utils.vtkutils import unstructuredGridToPolyData

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),"Utils"))
from generalutils import *
from vtkutils import *
from self_intersection_check import checkSelfIntersection

from Meshing import gmsh_meshing
from BoundarySetup import setup_ligament, setup_tissue, setup_forces
from SurfaceExtraction import extract_surface, add_surface_noise
from Statistics import calc_statistics, plot_statistics

parser = argparse.ArgumentParser(description="Generate a dataset of simulated organ deformations.")
parser.add_argument("outdir", type=path, help="Output folder")
parser.add_argument("num", type=int, help="Number of samples to create")
parser.add_argument("--start_num", type=int, default=0, help="ID of first sample to be created (default: %(default)d).")
parser.add_argument("--mesh", type=filepath, help="Surface mesh to use for all samples. If omitted, a random mesh will be generated for every sample.")
parser.add_argument("--config", type=filepath, default="Config/input_parameters_liver_fixed.yml", help="Path to config yaml (default: %(default)s).")
parser.add_argument("--gui", action="store_true", help="If set, runs simulation with in a GUI. Warning: If running with a GUI, the simulations will not be run in parallel, which may drastically increase the data generation time.")
parser.add_argument("--num_trial", type=int, default=1, help="If > 1, goes through an already existing output folder and runs simulations with new inputs for previously discarded samples, updating the default random seed by multiplying for num_trial.")
parser.add_argument("--force_stats_recalc", default=False, action='store_true', help="Force recalculation of statistics for each sample, even if a stats file has been found")

skipGroup = parser.add_argument_group( title="Pipeline Jumping", description="The following arguments let you skip parts of the pipeline. For example, if you have already generated your whole data set, but want to change parameters in the surface extraction, you can skip everything up to the surface extraction by passing --skip_simulation. Note that setting one of these parameters will automatically also skip all other pipeline steps coming before it." )
skipGroup.add_argument("--skip_surface_generation", action="store_true", help="")
skipGroup.add_argument("--skip_meshing", action="store_true", help="")
skipGroup.add_argument("--skip_simulation_setup", action="store_true", help="")
skipGroup.add_argument("--skip_simulation", action="store_true", help="")
skipGroup.add_argument("--skip_simulation_result_check", action="store_true", help="")
skipGroup.add_argument("--skip_surface_extraction", action="store_true", help="")
skipGroup.add_argument("--skip_voxelization", action="store_true", help="")

args = parser.parse_args()

# If we are in retry mode, skip surface generation and meshing
if args.num_trial > 1:
    args.skip_meshing = True

# If any skipping is done, also skip all steps before:
if args.skip_voxelization:
    args.skip_surface_extraction = True
if args.skip_surface_extraction:
    args.skip_simulation_result_check = True
# if args.skip_simulation_result_check:
#     args.skip_simulation = True
if args.skip_simulation:
    args.skip_simulation_setup = True
if args.skip_simulation_setup:
    args.skip_meshing = True
# if args.skip_meshing:
#     args.skip_surface_generation = True

## Load the default and the given config files
Config.load( "Config/default_input_parameters.yml" )
with open( args.config, 'r') as stream:
    givenConfig = yaml.load(stream, Loader=yaml.SafeLoader)

# Update the default config file with given values
default_config = flatten( Config.data )
given_config   = flatten(givenConfig)
Config.data    = unflatten( {key: given_config.get(key, default_config[key]) for key in default_config} )

# Save updated config file to output folder
filename = os.path.join( args.outdir, os.path.basename( args.config ) )
with open(filename, 'w') as outfile:
    yaml.dump(Config.data , outfile, default_flow_style=False)
# Log information about the current run
logRunInfo( args.outdir, args )

# Find out how many processes to run in parallel for those parts of the pipeline
# which start multiple processes:
maxNumProcesses = Config["parallel_processes"]
maxNumProcesses = max( maxNumProcesses, 1 )

logdir = Config["io"]["logdir"]
os.makedirs( logdir, exist_ok=True )

# Output filenames
voxelization_fname = Config["voxelization"]["voxelization_filename"]
stats_fname        = Config["io"]["stats_filename"]

# Setup range of dirs
samples_range = list(range(args.start_num,args.start_num+args.num))
if args.num_trial > 1:
    retry_samples = []
    for i in samples_range:
        curDir = directoryForIndex( i, args.outdir )
        if not os.path.isfile( os.path.join( curDir, voxelization_fname ) ):
            retry_samples.append( i )
            removeFiles( curDir, 'eformed' )
        else:
            continue
    samples_range = retry_samples
print(f"Running pipeline for {len(samples_range)} samples")

########################################################################
## If any pipeline step which could change the data is executed, then 
## remove the statistics in that folder.
## If only steps are carried out which don't influence the stats
## (for example, voxelization), we can leave the stats untouched.
if not (args.skip_surface_generation and args.skip_meshing and
        args.skip_simulation_setup and args.skip_simulation and
        args.skip_surface_extraction):
    print("Removing outdated stats")
    for i in samples_range:
        curDir = directoryForIndex( i, args.outdir )
        # Remove old data:
        removeFiles( curDir, stats_fname )


########################################################################
## Surface Generation:
## Create random surface meshes or, optionally, use the same predefined
## mesh for each simulation
# If a mesh is given, copy it:
if not args.skip_surface_generation:
    if args.mesh:
        for i in samples_range:
            curDir = directoryForIndex( i, args.outdir )
            surfaceFilename = os.path.join( curDir, "surface.stl" )
            shutil.copyfile( args.mesh, surfaceFilename )
            print("Copied", args.mesh, surfaceFilename )
    else:

        # Remove old surface files:
        print("Removing outdated surface files")
        for i in samples_range:
            curDir = directoryForIndex( i, args.outdir )
            removeFiles( curDir, "surface" )
            
        print("Generating {:d} random meshes".format(args.num))
        a = ["blender"]
        a += ["--background"]
        a += ["-noaudio"]
        a += ["--python"]
        a += [Config["io"]["mesh_generation_script"]]
        a += ["--"]
        a += ["--num"]
        a += [str(args.num)]
        a += ["--start_num"]
        a += [str(args.start_num)]
        a += ["--outdir"]
        a += [args.outdir]
        print("Running Blender: " + " ".join(a))
        p = subprocess.call(a)

########################################################################
## Mesh Generation:
## Create a volume mesh for each surface mesh

minAllowedArea      = Config["cleaning"]["min_surface_area"]
maxAllowedArea      = Config["cleaning"]["max_surface_area"]
hexas_used_for_simulation = Config["simulation"]["use_hexas"]

if hexas_used_for_simulation :
    args.skip_meshing = True

if not args.skip_meshing:
    if args.mesh:
        # If a mesh is given, only mesh it once and copy it for the other samples:
        firstDir = directoryForIndex( args.start_num, args.outdir )
        gmsh_meshing.surface2Volume( os.path.join( firstDir, "surface.stl" ) )
        volumeFilenameSrc = os.path.join( firstDir, "volume.vtk" )

        for i in samples_range[1:]:
            try:
                curDir = directoryForIndex( i, args.outdir )
                volumeFilenameDst = os.path.join( curDir, "volume.vtk" )
                shutil.copyfile( volumeFilenameSrc, volumeFilenameDst )
            except KeyboardInterrupt as e:
                exit()

    else:
        # Generate volume mesh for every sample:
        for i in samples_range:
            curDir = directoryForIndex( i, args.outdir )
            surfaceMeshName = getSurfaceMeshName( curDir )
            
            try:
                print("Meshing:", i)

                meshArea = surfaceArea( loadMesh( os.path.join( curDir, surfaceMeshName ) ) )
                if meshArea < minAllowedArea or meshArea > maxAllowedArea:
                    raise ValueError
                gmsh_meshing.surface2Volume( os.path.join( curDir, surfaceMeshName ) )

            except ValueError:
                print("Initial mesh area is either too small or too big: {:5f} m2, SKIPPING case {}".format(meshArea, i))
            except subprocess.TimeoutExpired:
                print("Timeout during meshing. Skipping sample")
            except KeyboardInterrupt as e:
                exit()
            except Exception as e:
                print("During meshing, the following error occurred. Skipping sample.")
                print(e)

# ########################################################################
## Boundary Conditions:
## Create random boundary conditions for each case:

if not args.skip_simulation_setup:

    # Load configured material/topology values:
    ligActive       = Config["boundary_conditions"]["ligament"]["active"]
    lengthMin       = Config["boundary_conditions"]["ligament"]["length_min"]
    lengthMax       = Config["boundary_conditions"]["ligament"]["length_max"]
    ligStiffnessMin = Config["boundary_conditions"]["ligament"]["stiffness_min"]
    ligStiffnessMax = Config["boundary_conditions"]["ligament"]["stiffness_max"]
    inflationMin    = Config["boundary_conditions"]["ligament"]["inflation_min"]
    inflationMax    = Config["boundary_conditions"]["ligament"]["inflation_max"]

    fixedActive          = Config["boundary_conditions"]["fixed"]["active"]
    fixedValue           = Config["boundary_conditions"]["fixed"]["spring_value"]
    surfaceAmountMin     = Config["boundary_conditions"]["fixed"]["surface_amount_min"]
    surfaceAmountMax     = Config["boundary_conditions"]["fixed"]["surface_amount_max"]
    weightDistanceMin    = Config["boundary_conditions"]["fixed"]["weight_distance_min"]
    weightDistanceMax    = Config["boundary_conditions"]["fixed"]["weight_distance_max"]
    weightNormalMin      = Config["boundary_conditions"]["fixed"]["weight_normal_min"]
    weightNormalMax      = Config["boundary_conditions"]["fixed"]["weight_normal_max"]
    weightNoiseMin       = Config["boundary_conditions"]["fixed"]["weight_noise_min"]
    weightNoiseMax       = Config["boundary_conditions"]["fixed"]["weight_noise_max"]

    tisActive            = Config["boundary_conditions"]["tissue"]["active"]
    tisUniform           = Config["boundary_conditions"]["tissue"]["uniform"]
    tisStiffnessMin      = Config["boundary_conditions"]["tissue"]["stiffness_min"]
    tisStiffnessMax      = Config["boundary_conditions"]["tissue"]["stiffness_max"]
    tisTopologyOffsetMax = Config["boundary_conditions"]["tissue"]["topology_offset_max"]
    tisTopologyNoiseMax  = Config["boundary_conditions"]["tissue"]["topology_noise_max"]
    tisLengthMax         = Config["boundary_conditions"]["tissue"]["length_max"]
    tisNumRays           = Config["boundary_conditions"]["tissue"]["num_rays"]

    forcesActive         = Config["boundary_conditions"]["forces"]["active"]

    for i in samples_range:
        try:
            random.seed( i * args.num_trial )
            
            curDir = directoryForIndex( i, args.outdir )            
            surfaceMeshName = getSurfaceMeshName( curDir )

            # Clear out old files:
            removeFiles( curDir, "^ligament.*", exact_pattern=True )
            removeFiles( curDir, "^surroundingTissue.*", exact_pattern=True )

            surface_filename = os.path.join( curDir, surfaceMeshName )
            surface = loadMesh( surface_filename )
            surface = generatePointNormals( surface )

            bounds = surface.GetBounds()
            pMin = np.asarray( (bounds[0],bounds[2],bounds[4]) )
            pMax = np.asarray( (bounds[1],bounds[3],bounds[5]) )
            diag = np.linalg.norm( pMax - pMin )

            ######################################
            # Create ligaments:
            if ligActive:
                numLigamentsMin = Config["boundary_conditions"]["ligament"]["ligaments_min"]
                numLigamentsMax = Config["boundary_conditions"]["ligament"]["ligaments_max"]

                rndRadius = random.uniform( inflationMin, inflationMax )*diag

                cx = (bounds[1]-bounds[0])*random.random() + bounds[0]
                cy = (bounds[3]-bounds[2])*random.random() + bounds[2]
                cz = (bounds[5]-bounds[4])*random.random() + bounds[4]
                rndCenter = np.asarray( (cx,cy,cz) )

                print("Creating Ligaments for sample {}".format(i))
                numLigaments = random.randint( numLigamentsMin, numLigamentsMax )
                print("\tSelecting {} random paths along surface".format(numLigaments))
                paths = setup_ligament.randomSurfaceConnections( surface, numLigaments,
                        lengthMin=lengthMin, lengthMax=lengthMax )
                print("\tCreating ligaments from surface paths:")
                ligaments = []
                for p in paths:
                    stiffness = random.uniform( ligStiffnessMin, ligStiffnessMax )
                    l = setup_ligament.extrudeLigament( surface, p,
                            center=rndCenter, radius=rndRadius,
                            constStiffness=stiffness )
                    ligaments.append(l)
                print("\tRemoving springs to ensure only one spring is connected per node")
                ligamentsPruned = []
                for l in ligaments:
                    pruned = setup_ligament.pruneLigament( volume, surface, l )
                    ligamentsPruned.append( pruned )

                # For debugging purposes, write out the surface ligaments:
                if Config["io"]["save_debug_data"]:
                    for j in range(len(ligaments)):
                        filename = "ligament{}.vtp".format(j)
                        writeSpringsVTK( ligaments[j], os.path.join( curDir, filename ) )
                        filename = "ligamentSparse{}.vtp".format(j)
                        writeSpringsVTK( ligamentsPruned[j], os.path.join( curDir, filename ) )

                # Write out the final results:
                for j in range(len(ligamentsPruned)):
                    filename = "ligament{}.npz".format(j)
                    writeSprings( ligamentsPruned[j], os.path.join( curDir, filename ) )

            ######################################
            # Extract points towards -Y, now referred to surface indices
            surfaceLowestIDs = setup_tissue.gridRayCast( surface, numSteps=tisNumRays ) 

            ######################################
            # Create random input nodal forces:
            if forcesActive:
                removeFiles( curDir, "^forces.*", exact_pattern=True )
                print("Creating input forces for sample {}".format(i))
                pullOnly        = Config["boundary_conditions"]["forces"]["pull_only"]
                forcesMinMag    = Config["boundary_conditions"]["forces"]["min_magnitude"]
                forcesMaxMag    = Config["boundary_conditions"]["forces"]["max_magnitude"]
                forcesRoiMin    = Config["boundary_conditions"]["forces"]["roi_min"]
                forcesRoiMax    = Config["boundary_conditions"]["forces"]["roi_max"]
                numberOfNonConsecutiveDeformations = Config["boundary_conditions"]["forces"]["nb_non_consecutive_def"]

                for num_def in range (numberOfNonConsecutiveDeformations):
                    # Select points on the upper surface
                    #volumeSurfaceIDs = getClosestPoints( surface, volume )

                    forcesIDs, forcesR = setup_forces.getForceIndices(surface, forcesRoiMin, forcesRoiMax)
                    #print("Selected force indices: {}".format(len(forcesIDs)))

                    forcesDir = None
                    if pullOnly:
                        f_center = np.asarray( surface.GetPoint(forcesIDs[0]) )
                        forcesDir = f_center - np.zeros_like( f_center )
                        forcesDir = forcesDir / np.linalg.norm(forcesDir)
                        forcesDir = setup_forces.perturbDirection( forcesDir, minAlpha=-45., maxAlpha=45. )

                    # Scale max allowed force depending on the distance between force and closest index on the volume
                    # maxAABBdim = np.amax( pMax - pMin )
                    # distanceWithclosestFixedPoint = distance( volume[forcesIDs[0]] and volume[centerID])
                    # scale = distanceWithclosestFixedPoint / maxAABBdim
                    # maxF = maxF * scale
                    forces = setup_forces.generateForces(forcesIDs, minForce=forcesMinMag, maxForce=forcesMaxMag, direction=forcesDir)
                    f = forces["forces"]
                    forceMag = np.linalg.norm(f)
                    print(f"Force magnitude: {forceMag}")

                    # We might decide here the number of simulation steps, depending on force magnitude. Or after the
                    # spring value has been picked.
                    # Write out the final results:
                    filename = "forces" + str(num_def) +".npz"
                    setup_forces.writeForces( forces, os.path.join( curDir, filename ) )

            ######################################
            ## Create random surrounding tissues:
            print("Creating surrounding tissue for sample {}".format(i))
            tissues = []
            allIDs = []

            # Create fixed boundary points on a random patch of the lowest surface
            if fixedActive:
                curFixedIDs = []
                candidateFixedIDs = surfaceLowestIDs
                if forcesActive:
                    # If forces are active, try to select the center of fixed points not too close
                    # to the force application point, to allow some deformation
                    forceID = forcesIDs[0]
                    excludeIDs = setup_tissue.getIndicesInROI( surface, forceID, 0.02 )
                    candidateFixedIDs = setup_tissue.removeDuplicatedIDs( candidateFixedIDs, excludeIDs )

                    if not len(candidateFixedIDs):
                        candidateFixedIDs = surfaceLowestIDs

                surfaceCenterID = candidateFixedIDs[random.randint(0, len(candidateFixedIDs)-1)] #centerID is a volume index
                # surfaceCenterID = getClosestPoints( volume, surface, [centerID] )[0] #corresponding surface index needed for extraction of surface patch

                getOut = 0
                while(not len(curFixedIDs)): #stay here until at least some points are selected
                    rndSurfaceAmount = np.abs(random.gauss(mu=0.01,sigma=0.04))
                    #rndSurfaceAmount = random.uniform( 0.0, 1.0)
                    rndSurfaceAmount = surfaceAmountMin + rndSurfaceAmount * (surfaceAmountMax - surfaceAmountMin)
                    # rndWeightDistance = 1.0 - random.random()**2
                    rndWeightDistance = random.uniform( weightDistanceMin, weightDistanceMax )
                    rndWeightNormal = random.uniform( weightNormalMin, weightNormalMax )
                    rndWeightNoise = random.uniform( weightNoiseMin, weightNoiseMax )
                
                    springSurface = extract_surface.randomSurface( surface,
                                                                wDistance = rndWeightDistance,
                                                                wNormal = rndWeightNormal,
                                                                wNoise = rndWeightNoise,
                                                                surfaceAmount = rndSurfaceAmount,
                                                                centerPointID = surfaceCenterID )
                    # Get surface indices corresponding to selected surface indices
                    curFixedIDs = getClosestPoints( springSurface, surface, discardDuplicate=True )
                    #curSpringIDs = setup_tissue.removeDuplicatedIDs( curSpringIDs, allIDs )
                    allIDs.extend( curFixedIDs )

                    getOut += 1
                    if getOut > 10:
                        raise IOError("Impossible to select fixed points!")

                # Create boundary points as springs associated with value fixedValue
                springs = setup_tissue.generateSprings( surface, curFixedIDs,
                            topologyOffset=tisTopologyOffsetMax,
                            maxTopologyNoise=tisTopologyNoiseMax,
                            minStiffness=fixedValue,
                            maxStiffness=fixedValue,
                            maxSpringLength=tisLengthMax )
                tissues.append(springs)

            ######################################
            # Define springs simulating surrounding tissues pulling from below
            if tisActive:
                curSpringIDs = setup_tissue.removeDuplicatedIDs( surfaceLowestIDs, allIDs )
                allIDs.extend( curSpringIDs )
                
                minStiff     = tisStiffnessMin
                maxStiff     = tisStiffnessMax

                # Update stiffness ranges based on selected force magnitude for stability reasons
                if forcesActive:
                    alphaMin = tisStiffnessMin / forcesMinMag
                    alphaMax = tisStiffnessMax / forcesMaxMag
                    minStiff = forceMag * alphaMin
                    maxStiff = forceMag * alphaMax

                if tisUniform:
                    k = random.uniform( minStiff, maxStiff )
                    minStiff = k
                    maxStiff = k

                springs = setup_tissue.generateSprings( surface, curSpringIDs,
                        topologyOffset=tisTopologyOffsetMax,
                        maxTopologyNoise=tisTopologyNoiseMax,
                        minStiffness=minStiff,
                        maxStiffness=maxStiff,
                        maxSpringLength=tisLengthMax )
                tissues.append(springs)


            # Write out the final results:
            for j in range(len(tissues)):
                filename = "surroundingTissue{}.npz".format(j)
                writeSprings( tissues[j], os.path.join( curDir, filename ) )
                if Config["io"]["save_debug_data"]:
                    filename = "surroundingTissue{}.vtp".format(j)
                    writeSpringsVTK( tissues[j], os.path.join( curDir, filename ) )

            # if Config["io"]["save_debug_data"]:
            #     writeMesh(surface, os.path.join( curDir, "perlinNoise.vtu" ))
            
        except KeyboardInterrupt as e:
            exit()
        except IOError as e:
            print("CAUGHT ERROR: '{}' - Skipping case {}".format(e,i))
            # Sample is invalid, remove it from sample list
            samples_range.remove(i)

########################################################################
## Setup SOFA Simulations

if not args.skip_simulation_setup:
    for i in samples_range:
        try:
            curDir = directoryForIndex( i, args.outdir )
            random.seed( i )

            curConfig = copy.deepcopy( Config.data )

            # Get simulation scene name
            scene_name = Config["simulation"]["scene_name"]

            # Choose random Young's modulus:
            minRho = Config["model"]["rho_min"]
            maxRho = Config["model"]["rho_max"]
            rndRho = random.uniform( minRho, maxRho )
            curConfig["model"]["rho"] = rndRho
            del curConfig["model"]["rho_min"]
            del curConfig["model"]["rho_max"]

            # Choose random Young's modulus:
            minE = Config["model"]["fem"]["E_min"]
            maxE = Config["model"]["fem"]["E_max"]
            rndE = random.uniform( minE, maxE )
            curConfig["model"]["fem"]["E"] = rndE
            del curConfig["model"]["fem"]["E_min"]
            del curConfig["model"]["fem"]["E_max"]

            # Choose random Poissons's ratio:
            minNu = Config["model"]["fem"]["nu_min"]
            maxNu = Config["model"]["fem"]["nu_max"]
            rndNu = random.uniform( minNu, maxNu )
            curConfig["model"]["fem"]["nu"] = rndNu
            del curConfig["model"]["fem"]["nu_min"]
            del curConfig["model"]["fem"]["nu_max"]

            # Find how many ligament files there are and write their names in the config:
            ligamentFiles = findFiles( curDir, "ligament\d*.npz", exact_pattern=True )
            fileList = []
            for f in ligamentFiles:
                fileList.append( "%BASEDIR/" + f )
            curConfig["filenames"]["ligaments"] = fileList

            # Find how many surrounding tissue files there are and write their names in the config:
            tissueFiles = findFiles( curDir, "surroundingTissue\d*.npz", exact_pattern=True )
            fileList = []
            for f in tissueFiles:
                fileList.append( "%BASEDIR/" + f )
            curConfig["filenames"]["surrounding_tissue"] = fileList


            filename = os.path.join( curDir, "input_parameters.yml" )
            with open(filename, 'w') as outfile:
                yaml.dump(curConfig, outfile, default_flow_style=False)
            #shutil.copy( "Config/base_input_parameters.yml", os.path.join(curDir, "input_parameters.yml") )

        except KeyboardInterrupt as e:
            exit()
        
########################################################################
## Run SOFA Simulations
if not args.skip_simulation:

    maxNumSimulationProcesses = maxNumProcesses
    if args.gui and maxNumProcesses > 1:
        print("WARNING: --gui is set, but parrallel_processes is > 1.\n" + 
                "Will reduce number of parallel simulations to 1." )
        maxNumSimulationProcesses = 1

    # List of IDs of samples which have not started yet:
    queuedProcesses = copy.deepcopy(samples_range)
    # List of currently running processes:
    runningProcesses = []
    # Number of simulations which have already finished:
    finishedProcesses = 0
    numberOfProcesses = len(queuedProcesses)

    # Get simulation scene name
    scene_name = Config["simulation"]["scene_name"]

    print("Running SOFA simulations.")
    print("Will run {} simulations in parallel.".format( maxNumSimulationProcesses ))

    try:
        try:
            while len(queuedProcesses) > 0 or len(runningProcesses) > 0:

                # If we're not running enough simulations at the moment, spawn a new process:
                if len(runningProcesses) < maxNumSimulationProcesses:
                    if len(queuedProcesses) > 0:   # if there are still unstarted samples
                        i = queuedProcesses.pop(0)

                        curDir = directoryForIndex( i, args.outdir )

                        # Remove previous samples
                        removeFiles( curDir, "eformed" )
                        
                        # Get simulation scene name
                        scene_name = Config["simulation"]["scene_name"]

                        print("Starting SOFA simulation {}".format(curDir))
                        a = ["python3"]
                        a += [scene_name]
                        a += [ os.path.abspath( os.path.join( curDir, "input_parameters.yml" ) ) ]
                        if args.gui:
                            a += ["--gui"]
                        print("\t" + " ".join(a))

                        # If we're running more than one process in parallel, send output
                        # to log file. Otherwise, send it to stdout.
                        logfile = None
                        if maxNumSimulationProcesses > 1:
                            logfilename = os.path.join( logdir, "sofa_{:06d}.txt".format(i))
                            logfile = open(logfilename, "w")
                            print("\tWriting logs to {:}".format(logfilename))
                        p = subprocess.Popen( a, cwd="Sofa/scenes",
                                stdout=logfile,
                                stderr=subprocess.STDOUT )
                        runningProcesses.append( {"id":i, "process":p, "logfile":logfile} )

                for j, proc in enumerate(runningProcesses):
                    if proc["process"].poll() is not None:
                        finishedProcesses += 1
                        print("Simulation {} done ({} of {} simulations finished)".format(
                            proc["id"], finishedProcesses, numberOfProcesses))
                        if proc["logfile"] is not None:
                            proc["logfile"].close()
                        del runningProcesses[j]
                        break
                    
                time.sleep(0.01)

        #except system_exit as e:
        #    print("Sofa exited normally.")
        except Exception as e:
            print("Exception. Killing simulations...")
            for j, proc in enumerate(runningProcesses):
                proc["process"].kill()
                if proc["logfile"] is not None:
                    proc["logfile"].close()
            raise e

    except KeyboardInterrupt as e:
        print("Keyboard interrupt. Stopping...")
        exit()

# ########################################################################
# ## Setup filenames needed for all the processes further in the pipeline
exported_frames = Config["io"]["frames_to_export"]
numberOfNonConsecutiveDeformations = Config["boundary_conditions"]["forces"]["nb_non_consecutive_def"]
deformed_mesh = ["deformed.vtu"]
partial_deformed_mesh = ["partialDeformedSurface.vtp"]

if exported_frames > 1:
    deformed_mesh_list = [ deformed_mesh[0][:-4]+str(n)+deformed_mesh[0][-4:] for n in range(exported_frames) ]
    partial_deformed_mesh_list = [ partial_deformed_mesh[0][:-4]+str(n)+partial_deformed_mesh[0][-4:] for n in range(exported_frames) ]
    deformed_mesh = deformed_mesh_list
    partial_deformed_mesh = partial_deformed_mesh_list

elif numberOfNonConsecutiveDeformations > 1:
    deformed_mesh_list = [ deformed_mesh[0][:-4]+str(n)+deformed_mesh[0][-4:] for n in range(numberOfNonConsecutiveDeformations) ]
    partial_deformed_mesh_list = [ partial_deformed_mesh[0][:-4]+str(n)+partial_deformed_mesh[0][-4:] for n in range(numberOfNonConsecutiveDeformations) ]
    deformed_mesh = deformed_mesh_list
    partial_deformed_mesh = partial_deformed_mesh_list
#######################################################################
# Do a quality check on the simulation result
if not args.skip_simulation_result_check:
    mesh_to_check = deformed_mesh[-1]

    print(f"Checking self intersection on {len(samples_range)} meshes")
    a = ["blender"]
    a += ["--background"]
    a += ["-noaudio"]
    a += ["--python"]
    a += ["Utils/self_intersection_check.py"]
    a += ["--"]
    a += [str(args.num)]
    a += ["--start_num"]
    a += [str(args.start_num)]
    a += ["--outdir"]
    a += [args.outdir]
    a += ["--in_filename"]
    a += [mesh_to_check]
    print("Running Blender: " + " ".join(a))
    p = subprocess.call(a)


########################################################################
## Extract (partial) surface of result:

if not args.skip_surface_extraction:

    surfaceAmountMin    = Config["intraoperative_surface"]["surface_amount_min"]
    surfaceAmountMax    = Config["intraoperative_surface"]["surface_amount_max"]
    weightDistanceMin   = Config["intraoperative_surface"]["weight_distance_min"]
    weightDistanceMax   = Config["intraoperative_surface"]["weight_distance_max"]
    weightNormalMin     = Config["intraoperative_surface"]["weight_normal_min"]
    weightNormalMax     = Config["intraoperative_surface"]["weight_normal_max"]
    weightNoiseMin      = Config["intraoperative_surface"]["weight_noise_min"]
    weightNoiseMax      = Config["intraoperative_surface"]["weight_noise_max"]

    maxSurfacePieces    = Config["intraoperative_surface"]["max_surface_pieces"]
    surfaceProbability  = Config["intraoperative_surface"]["additional_surface_probability"]

    rigidOffsetActive   = Config["intraoperative_surface"]["rigid_offset"]["active"]
    
    subdivActive        = Config["intraoperative_surface"]["subdivide_surface"]["active"]
    subdivFactor        = Config["intraoperative_surface"]["subdivide_surface"]["subdiv_factor"]
    
    sparsifyActive      = Config["intraoperative_surface"]["sparsify_surface"]["active"]
    sparsifyShiftMin    = Config["intraoperative_surface"]["sparsify_surface"]["shift_min"]
    sparsifyShiftMax    = Config["intraoperative_surface"]["sparsify_surface"]["shift_max"]
    sparsifyScaleMin    = Config["intraoperative_surface"]["sparsify_surface"]["scale_min"]
    sparsifyScaleMax    = Config["intraoperative_surface"]["sparsify_surface"]["scale_max"]
    
    noiseAmount         = Config["intraoperative_surface"]["noise_amount"]
    
    minAllowedDispl     = Config["cleaning"]["min_displacement"]
    maxAllowedDispl     = Config["cleaning"]["max_displacement"]
    minAllowedArea      = Config["cleaning"]["min_surface_area"]

    visibleStatsFile    = Config["intraoperative_surface"]["visible_stats_filename"]

    for i in samples_range.copy():
        print("sample", i)
        try:
            curDir = directoryForIndex( i, args.outdir )
            random.seed( i )
            print("curdir",curDir)
            
            # Clean from previous samples
            removeFiles( curDir, "partialDeformedSurface" )
            removeFiles( curDir, voxelization_fname )

            print("Extracting surface of deformed state:", i)

            if not args.skip_simulation_result_check:
                checkSelfIntersection( curDir )

            # Load undeformed state
            initialSurface = loadMesh(os.path.join(curDir, "surface.stl"))
            areaInit = surfaceArea(initialSurface)
            print("Rest shape area", areaInit)

            print("loading deformed mesh", os.path.join( curDir, deformed_mesh[-1] ))

            # Discard sample based on the highest deformation mesh (if any).
            deformedSurface = loadMesh( os.path.join( curDir, deformed_mesh[-1] ) )
            areaDef = surfaceArea(deformedSurface)
            print("Area of deformed mesh", areaDef)

            # Check if surface area has drastically increased or is too small. If so, assume this sample to be invalid:
            if areaDef > areaInit*1.5:
                raise ValueError("Surface area increased considerably during simulation, from {:.4f} to {:.4f}.".format(areaInit, areaDef) )
            elif areaDef < minAllowedArea:
                raise ValueError("Surface area is too small: {:.4f}, something went wrong.".format(areaDef) )

            maxDispl = getMaximumDisplacement( initialSurface, deformedSurface )

            print("maxDispl", maxDispl)
            print("minAllowedDispl", minAllowedDispl)
            print("maxAllowedDispl", maxAllowedDispl)

            if maxDispl < minAllowedDispl:
                raise ValueError("Organ did not deform enough during simulation, maximum displacement is {:.4f}.".format(maxDispl) )
            elif maxDispl > maxAllowedDispl:
                raise ValueError("Organ deformed too much during simulation, maximum displacement is {:.4f}.".format(maxDispl) )
            
            # Select a percentage of the surface:
            rndSurfaceAmount = random.uniform( surfaceAmountMin, surfaceAmountMax )
            rndWeightDistance = random.uniform( weightDistanceMin, weightDistanceMax )
            rndWeightNormal = random.uniform( weightNormalMin, weightNormalMax )
            rndWeightNoise = random.uniform( weightNoiseMin, weightNoiseMax )
            # Select the seed point for random surface extraction. In this way surface extraction
            # starts from the same ID in case of multiple frames
            centerSurfaceID = random.randint(0,deformedSurface.GetNumberOfPoints()-1)

            visibleAmounts = []

            # Select a rigid transform to apply, if required
            if rigidOffsetActive:
                angleDegreeMax      = Config["intraoperative_surface"]["rigid_offset"]["angle_degree_max"]
                offsetMax           = Config["intraoperative_surface"]["rigid_offset"]["offset_max"]
                rigidTransform = createRandomRigidTransform( angleDegreeMax, offsetMax )
            
            # Extract the partial deformed surface for each exported configuration
            for num_frame in range(max(exported_frames, numberOfNonConsecutiveDeformations)):
                deformedSurface = loadMesh( os.path.join( curDir, deformed_mesh[num_frame] ) )
                deformedSurface = unstructuredGridToPolyData(deformedSurface)

                # Optionally add the chosen rigid offset to the extracted surface
                if rigidOffsetActive:
                    deformedSurface = applyTransform( deformedSurface, rigidTransform )
                    
                    # Save the deformed mesh, which has potentially been changed by the rigid offset:
                    writeMesh( deformedSurface, os.path.join( curDir, "deformedFinal.vtu" ) )

                # deformedSurface = extractSurface( deformed )
                deformedSurfaceArea = surfaceArea( deformedSurface )

                # Determine how many surface pieces to extract:
                numSurfacePieces = 1
                for j in range(1,maxSurfacePieces):
                    if random.random() < surfaceProbability:
                        numSurfacePieces += 1
                print("Selecting {} surface pieces (Target surface to extract: {:.2f}%)".format(
                    numSurfacePieces, rndSurfaceAmount ))

                residualSurfaceAmount = rndSurfaceAmount
                partialDeformedSurface = vtkPolyData()
                for j in range( numSurfacePieces ):
                    # If this is the last piece to select, select enough to fill
                    # rndSurfaceAmount. Otherwise select a random subsection.
                    if j == numSurfacePieces-1:
                        curSurfaceAmount = residualSurfaceAmount
                    else:
                        curSurfaceAmount = random.random()*residualSurfaceAmount

                    curSurfacePiece = extract_surface.randomSurface(
                            deformedSurface,
                            wDistance = rndWeightDistance,
                            wNormal = rndWeightNormal,
                            wNoise = rndWeightNoise,
                            surfaceAmount = curSurfaceAmount,
                            centerPointID=centerSurfaceID )
                    
                    residualSurfaceAmount -= surfaceArea( curSurfacePiece )

                    # Append the surface to the initial one:
                    partialDeformedSurface = appendSurfaces( partialDeformedSurface,
                            curSurfacePiece)

                    area = surfaceArea( partialDeformedSurface )
                    print( f"\tSelected surface area: {area:.3f}, ratio: {area/areaDef:.3}")

                # Keep track of visible surface per frame before noise is applied,
                # for statistics
                visibleAmounts.append( area/deformedSurfaceArea )

                # Add noise to extracted surface
                # NB: noise varies between the different exported frames
                if subdivActive:
                    factor = random.uniform( 1, subdivFactor )
                    partialDeformedSurface = add_surface_noise.subdivideSurface(
                        partialDeformedSurface,
                        subdivFactor = factor )

                if sparsifyActive:
                    print("Sparsifying intraoperative partial surface")
                    scale = sparsifyScaleMin + \
                        random.random()*(sparsifyScaleMax - sparsifyScaleMin)
                    shift = sparsifyShiftMin + \
                        random.random()*(sparsifyShiftMax - sparsifyShiftMin)
                    sparsifiedSurface = add_surface_noise.sparsifySurface(
                            partialDeformedSurface,
                            scale = scale,
                            shift = shift,
                            frequency = random.random()*5 + 3 )

                    if sparsifiedSurface.GetNumberOfPoints() > 100: #60
                        partialDeformedSurface = sparsifiedSurface
                    else:
                        print("Warning: Sparsification resulted in too few points. Will continue with non-sparsified surface.")

                partialDeformedSurface = add_surface_noise.addGaussNoiseToSurface(
                    partialDeformedSurface,
                    sigma = random.uniform( 0, noiseAmount ) )
            
                partialDeformedSurface = unstructuredGridToPolyData(
                        partialDeformedSurface )
                writeMesh( partialDeformedSurface, os.path.join( curDir, partial_deformed_mesh[num_frame] ) )

            # Save visible amount to file
            np.savetxt( os.path.join( curDir, visibleStatsFile ), visibleAmounts, fmt='%.4f' )

            # Write resulting surface to file:
            if Config["io"]["save_debug_data"]:
                writeMesh( deformedSurface, ( os.path.join( curDir, "deformedSurface.vtp" ) ) )

        except KeyboardInterrupt as e:
            exit()
        except IOError as e:
            print("CAUGHT ERROR: '{}' - Skipping case {}".format(e,i))
            samples_range.remove(i) # Sample is invalid, remove it from sample list
        except TypeError as e:  # Occurs if NaN was found in simulation result
            print("CAUGHT ERROR: '{}' - Skipping case {}".format(e,i))
            samples_range.remove(i) # Sample is invalid, remove it from sample list
        except ValueError as e:  # Occurs if simulation result is not valid
            print("CAUGHT ERROR: '{}' - Skipping case {}".format(e,i))
            samples_range.remove(i) # Sample is invalid, remove it from sample list
        except AssertionError as e:  # Occurs if something went wrong in partial surface extraction
            print("CAUGHT ERROR: '{}' - Skipping case {}".format(e,i))
            samples_range.remove(i) # Sample is invalid, remove it from sample list

########################################################################
## Voxelize results:

if not args.skip_voxelization:

    # List of IDs of samples which have not started yet:
    queuedProcesses = copy.deepcopy(samples_range)
    # List of currently running processes:
    runningProcesses = []
    # Number of simulations which have already finished:
    finishedProcesses = 0
    numberOfSamples = len(queuedProcesses)

    # Get voxelization script name
    voxelization_script = Config["voxelization"]["voxelization_script"]

    print("Voxelization")
    print("Will run {} voxelization processes in parallel.".format( maxNumProcesses ))

    try:
        try:
            while len(queuedProcesses) > 0 or len(runningProcesses) > 0:

                # If we're not running enough processes at the moment, spawn a new process:
                if len(runningProcesses) < maxNumProcesses:
                    if len(queuedProcesses) > 0:   # if there are still unstarted samples
                        i = queuedProcesses.pop(0)

                        curDir = directoryForIndex( i, args.outdir )
           
                        try:
                            if not args.skip_simulation_result_check:
                                checkSelfIntersection( curDir )

                            print("Starting voxelization {}".format(curDir))
                            a = ["python3"]
                            a += [voxelization_script]
                            a += [curDir]
                            a += ["--voxelization_fname"]
                            a += [voxelization_fname]
                            a += ["--exported_frames"]
                            a += [str(exported_frames)]
                            a += ["--non_consecutive_deformations"]
                            a += [str(numberOfNonConsecutiveDeformations)]
                            print("\t" + " ".join(a))

                            # If we're running more than one process in parallel, send output
                            # to log file. Otherwise, send it to stdout.
                            logfile = None
                            if maxNumProcesses > 1:
                                logfilename = os.path.join( logdir, "vox_{:06d}.txt".format(i))
                                logfile = open(logfilename, "w")
                                print("\tWriting logs to {:}".format(logfilename))
     
                            p = subprocess.Popen( a,
                                    stdout=logfile,
                                    stderr=subprocess.STDOUT )
                            runningProcesses.append( {"id":i, "process":p, "logfile":logfile} )
                        except ValueError as e:
                            print(f"Exception. Could not start voxelization for sample {i}")
                            print("Reason:", e)

                for j, proc in enumerate(runningProcesses):
                    if proc["process"].poll() is not None:
                        finishedProcesses += 1
                        print("Voxelization {} done ({} of {} finished)".format(
                            proc["id"], finishedProcesses, numberOfSamples))
                        if proc["logfile"] is not None:
                            proc["logfile"].close()
                        del runningProcesses[j]
                        break
                    
                time.sleep(0.01)

        except Exception as e:
            print("Exception. Killing processes...")
            for j, proc in enumerate(runningProcesses):
                proc["process"].kill()
                if proc["logfile"] is not None:
                    proc["logfile"].close()
            raise e

    except KeyboardInterrupt as e:
        print("Keyboard interrupt. Stopping...")
        exit()



################################################################
## Statistics calculation
## The following collects data on every sample and then prints and plots the results.

statsList = []
for i in samples_range:
    try:
        print("Calculating statistics for sample {}".format(i))
        
        curDir = directoryForIndex( i, args.outdir )
        # First, check if the voxelized file exists. Otherwise, something went wrong and we'll
        # skip this sample:
        if not os.path.exists( os.path.join( curDir, voxelization_fname ) ):
            raise IOError("No voxelized file found.")

        # If the stats file already exists, open it:
        statsFile = os.path.join( curDir, stats_fname ) 
        if not args.force_stats_recalc and os.path.exists( statsFile ): 
            #removeFiles( curDir, stats_fname )
            with open(statsFile, 'r') as stream:
                stats = yaml.load(stream, Loader=yaml.SafeLoader)
                statsList.append( stats )
        else:   # Otherwise, calculate the stats for this sample:

            surface = loadMesh( os.path.join( curDir, "surface.stl" ) )

            stats = calc_statistics.statsForSample( curDir, i, surface, deformed_mesh,
                    partial_deformed_mesh, denoise=True)

            filename = os.path.join( curDir, stats_fname )
            with open(filename, 'w') as outfile:
                yaml.dump(stats, outfile, default_flow_style=False)

            statsList.append( stats )
    except KeyboardInterrupt as e:
        exit()
    except IOError as e:
        print("CAUGHT ERROR: '{}' - Skipping case {}".format(e,i))

print("Merging all statistics into a single file (for quicker access):")
mergedStatsFile = os.path.join( args.outdir, stats_fname ) 
print("\t",mergedStatsFile)
# not used
# prevStats = {}
# if os.path.exists( mergedStatsFile ):
#     with open(mergedStatsFile, 'r') as stream:
#         prevStats = yaml.load(stream, Loader=yaml.SafeLoader)
mergedStats = calc_statistics.mergeStats( statsList )
with open(mergedStatsFile, 'w') as outfile:
   yaml.dump(mergedStats, outfile, default_flow_style=False)

print( "Statistics of generated samples:" )
gStats = calc_statistics.globalStatistics( statsList )
print( "Number of valid samples:", len(statsList) )
calc_statistics.printGlobalStats( gStats )
print("Plotting results")
hist_props = dict(bins=25, rwidth=0.95, stacked=True)
plot_statistics.plotSurfaceAmounts( statsList, args.outdir, histProps=hist_props)
plot_statistics.plotMaxDisplacement( statsList, args.outdir, histProps=hist_props )
plot_statistics.plotMaxDisplacementAndArea( statsList, args.outdir, histProps=hist_props )
plot_statistics.plotMeanDisplacement( statsList, args.outdir, histProps=hist_props )
plot_statistics.plotNumVisiblePoints( statsList, args.outdir, histProps=hist_props )
plot_statistics.plotSurfaceArea( statsList, args.outdir, histProps=hist_props )
plot_statistics.plotSurfaceAmountsAndArea( statsList, args.outdir )
if "MeanNumSprings" in gStats:
    plot_statistics.plotNumSprings( statsList, args.outdir, histProps=hist_props )
if "MeanMaxDisplacementPerFrame" in gStats:
    plot_statistics.plotMaxDisplFrame( statsList, args.outdir, histProps=hist_props )
print("Done")


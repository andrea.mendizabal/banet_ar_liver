import vtk
import os
import sys
import numpy as np
import math
from Utils.vtkutils import applyTransform, writeMesh

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), "../Utils"))
# sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),"../Voxelization"))
from generalutils import *
from vtkutils import *
import voxelize
import voxelize_displacement


def compute_grid_resolution(minbb, maxbb, max_res = 64):
    """
        Compute the voxelization grid resolution for a given bounding box.

        Arguments:
        ---------
        max_res :
            Maximal number of cells along the longest edge of the bounding box. Optimal values are powers of 2.
            Default: 64
        minbb and maxbb:
            Bounding box of the object to voxelize. It usually contains margins to allow large deformations inside it.

        Returns:
        --------
        grid_resolution :
            Regular grid resolution for the desired max_res.
        s :
            Size of the global bounding box of the regular grid.
        cell_size :
            Size of the cell of the regular grid in meters.
        """

    s = [math.fabs(maxbb[i] - minbb[i]) for i in range(3)]  # Size in the x, y and z-axis

    ## Compute cell size and new bounding box
    cell_size = max([elem for elem in s]) / max_res
    print("Cell size = {}x{}x{}".format(cell_size, cell_size, cell_size))

    new_s = [cell_size * ((s[i]//cell_size) + ((s[i] % cell_size )!=0)) for i in range(3)]
    new_minbb = minbb
    new_maxbb = minbb + new_s

    s = [math.fabs(new_maxbb[i] - new_minbb[i]) for i in range(3)]  # Updated size in the x, y and z-axis

    ## Compute grid resolution
    # grid_resolution = [int(s[i]/cell_size) for i in range(3)]
    nx = int(s[0] / cell_size)  # Number of cells in the x-axis
    ny = int(s[1] / cell_size)  # Number of cells in the y-axis
    nz = int(s[2] / cell_size)  # Number of cells in the z-axis
    grid_resolution = [nx, ny, nz]

    return grid_resolution, s, cell_size


def voxelizeDirectory(curDir, meshDir, voxName, numFrames, numNonConsecDef):
    # If meshDir is not specified, assume it is the same as curDir
    if meshDir is None:
        meshDir = curDir

    # Remove old data:
    removeFiles(curDir, voxName)

    #################################################
    ## Create a grid around the origin containing the preoperative mesh

    # Preoperative
    print("Voxelizing preoperative (undeformed) state")
    preopSurface = loadMesh(os.path.join(meshDir, "surface.stl"))

    # Find bounds of the object
    bounds = [0] * 6;
    preopSurface.GetBounds(bounds)  # Bounds in the form (xmin, xmax, ymin, ymax, zmin, zmax)
    reordered_bounds = [bounds[0], bounds[2], bounds[4],
                        bounds[1], bounds[3], bounds[5]]  # Bounds in the form (xmin, ymin, zmin, xmax, ymax, zmax)
    s = [math.fabs(reordered_bounds[i+3] - reordered_bounds[i]) for i in range(3)]  # Size in the x, y and z-axis

    # Add margins in each direction  to contain all realistic deformations
    margins = np.array([0.05, 0.05, 0.05])  # The beam is of size 0.05*0.05*0.2, large deformations happen in x and y
    minbb = np.array(reordered_bounds[0:3]) - margins
    maxbb = np.array(reordered_bounds[3:6]) + margins

    # Compute grid resolution for a chosen max resolution in one direction (usually a power of 2)
    max_res = 32
    gridSize, size, cellSize = compute_grid_resolution(minbb, maxbb, max_res)
    print("Grid resolution is", gridSize)
    # Create grid
    grid = voxelize.createGrid(size, gridSize)

    #################################################
    ## Center mesh:
    tf = vtkTransform()
    dx = -(bounds[1] + bounds[0]) * 0.5
    dy = -(bounds[3] + bounds[2]) * 0.5
    dz = -(bounds[5] + bounds[4]) * 0.5
    tf.Translate((dx, dy, dz))

    ##################
    # Apply a random rotation before voxelization
    # w = random.uniform(0, 360)
    # x = random.uniform(0, 1)
    # y = random.uniform(0, 1)
    # z = random.uniform(0, 1)
    # v = np.array([x, y, z])
    # norm = np.linalg.norm(v)
    # axis = (v / norm).tolist()

    # tf.PostMultiply()
    # tf.RotateWXYZ( w, axis )
    ##################

    preopSurface = applyTransform(preopSurface, tf)

    # Write the applied transform into a field data array:
    voxelize.storeTransformationMatrix(grid, tf)

    # # Calculate the signed distance field for the preoperative surface, store it in grid:
    # voxelize.distanceField(preopSurface, grid, "preoperativeSurface", signed=True)

    #################################################
    ## Intraoperative
    deformed_mesh = "deformed.vtu"
    partial_deformed_mesh = ["partialDeformedSurface.vtp"]
    intraop_array_name = "intraoperativeSurface"

    # Cases handled : - various consecutive frames
    #                 - one frame and several nonconsecutive deformations
    #                 - one frame
    if numFrames > 1:
        deformed_mesh = deformed_mesh[:-4] + str(numFrames - 1) + deformed_mesh[-4:]
        print(deformed_mesh)
        partial_deformed_mesh_list = [partial_deformed_mesh[0][:-4] + str(n) + partial_deformed_mesh[0][-4:] for n in
                                      range(numFrames)]
        partial_deformed_mesh = partial_deformed_mesh_list
        intraop_array_name_list = [intraop_array_name + str(n) for n in range(numFrames)]

    elif numNonConsecDef > 1:
        deformed_mesh = deformed_mesh[:-4] + str(numNonConsecDef - 1) + deformed_mesh[-4:]
        print(deformed_mesh)
        partial_deformed_mesh_list = [partial_deformed_mesh[0][:-4] + str(n) + partial_deformed_mesh[0][-4:] for n in
                                      range(numNonConsecDef)]
        partial_deformed_mesh = partial_deformed_mesh_list
        intraop_array_name_list = [intraop_array_name + str(n) for n in range(numNonConsecDef)]

    elif numFrames == 1 and numNonConsecDef == 1:
        intraop_array_name_list = [intraop_array_name + str(0)]

    else:
        print("NOT IMPLEMENTED YET : Several frames of several nonconsecutive deformations is not implemented yet.")
    # intraop_array_name_list = [intraop_array_name + str(n) for n in range(numNonConsecDef)]
    intraop_array_name = intraop_array_name_list

    print(f"Voxelizing {numFrames} intraoperative (deformed) state/s")
    for n, pdf_name in enumerate(partial_deformed_mesh):

        # Load and center the intraop surface as the preop
        intraopSurface = loadMesh(os.path.join(curDir, pdf_name))
        intraopSurface = applyTransform(intraopSurface, tf)

        # Calculate the distance field for the intraoperative surface, store it in grid:
        voxelize.distanceField(intraopSurface, grid, intraop_array_name[n], signed=False)

    #################################################
    ## Ground truth springs

    # Load deformed surface and center it
    deformedSurface = loadMesh(os.path.join(curDir, deformed_mesh))
    deformedSurface = applyTransform(deformedSurface, tf)

    # Stiffness must be referred to the undeformed volume to be then correctly interpolated
    stiffness = deformedSurface.GetPointData().GetArray("stiffness")
    preopSurface.GetPointData().AddArray(stiffness)

    # Spread spring values on grid points. No interpolation here. Vertices at the boundaries between two regions
    # are assigned to one or the other based on the order in which they are processed
    print("Spreading stiffness to grid")
    grid = voxelize_displacement.spreadValuesOnGridPoints(preopSurface, grid, "stiffness")#, radius=cellSize/2)

    # # Interpolate stiffness to grid.
    # print("Interpolating stiffness to grid")
    # grid = voxelize_displacement.interpolateToGrid(preopSurface, grid, cellSize)#, radius=cellSize * 2, sharpness=10)

    #################################################
    ## Write output:
    filename = os.path.join(curDir, voxName)
    print("Writing to {}".format(filename))
    writeMesh(grid, filename)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Voxelizes all necessary components inside  a given directory. Writes out results as a 'voxelized.vts' structured grid.")
    parser.add_argument("datadir", type=path, help="Directory in which to process files.")
    parser.add_argument("--meshdir", type=path, default=None,
                        help="Directory with surface and volume mesh to use for all samples. If omitted, it is assumed that such meshes are inside each folder.")
    parser.add_argument("--voxelization_fname", type=str, default="voxelized.vts",
                        help="Name of the voxelization filename.")
    parser.add_argument("--exported_frames", type=int, default=1, help="Number of exported frames.")
    parser.add_argument("--non_consecutive_deformations", type=int, default=1, help="Number of non consecutive deformations.")


    args = parser.parse_args()

    voxelizeDirectory(args.datadir, meshDir=args.meshdir, voxName=args.voxelization_fname,
                      numFrames=args.exported_frames, numNonConsecDef=args.non_consecutive_deformations)

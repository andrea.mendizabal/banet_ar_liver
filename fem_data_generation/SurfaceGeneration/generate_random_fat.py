import bpy
import mathutils
import random
import math
import os
import sys
import bmesh
import argparse
import traceback

d = os.path.dirname(bpy.data.filepath)
print("path:", d, bpy.data.filepath)
if not d in sys.path:
    sys.path.append(d)
    sys.path.append(os.path.join(d, "..", "Utils"))
    sys.path.append(os.path.join(d, "Utils"))
from generalutils import *
from blenderutils import *

# Remove all arguments passed to Blender, only take those after the double dash '--' into account:
argv = sys.argv
if "--" in argv:
    argv = argv[argv.index("--") + 1:]
else:
    argv = []

parser = argparse.ArgumentParser(description="Generate a random mesh using Blender functionality. Tested with Blender 2.82.")
parser.add_argument("--random_seed", type=int, default=None, help="Number to seed the random generator with. By default, the ID of the mesh is used as seed." )
parser.add_argument("--num", type=int, default=1, help="Number of meshes to generate.")
parser.add_argument("--start_num", type=int, default=0, help="Optionally start from this simulation number.")
parser.add_argument("--outdir", type=path, default=".", help="Folder in which to save the random mesh.")
parser.add_argument("--bounds", type=float, default=0.3, help="Edge of the cube defining the volume where the object must lie.")
parser.add_argument("--thickness", type=tuple, default=(0.0047,0.06), help="Min and max thickness allowed for the generated flat mesh.")
parser.add_argument("--predeform", type=bool, default=True, help="If true, the generated surface will be pre-deformed.")

args = parser.parse_args(argv)


###########################################################################
# MAIN

samplesIdx = list(range(args.start_num, args.start_num+args.num))
num_attempt = 1

while len(samplesIdx):
    i = samplesIdx[0]

    try:
        outdir = path( os.path.join( args.outdir, "{:06d}".format(i) ) )
        
        if num_attempt == 1:
            if args.random_seed:
                # if a seed is manually specified
                random.seed(args.random_seed + i)
            else:
                random.seed( i )
        else:
            # Randomize seed since default seed did not lead to a valid mesh
            random.seed( i * num_attempt )

        # Go to object mode if possible:
        if bpy.context.object != None:
            if bpy.context.object.mode != 'OBJECT':
                setMode( "OBJECT" )

        # Clear previous meshes:
        for o in bpy.data.objects:
            o.select_set(False)
        for o in bpy.data.objects:
            if "RandomMesh" in o.name:
                o.select_set(True)
        bpy.ops.object.delete()
        
        scene = bpy.context.scene
        
        # Create an empty mesh and the object.
        mesh = bpy.data.meshes.new('RandomMesh')
        obj = bpy.data.objects.new("RandomMesh", mesh)

        # Add the object into the scene.
        bpy.context.collection.objects.link( obj )
        bpy.context.view_layer.objects.active = obj
        obj.select_set(True)

        # Construct the bmesh sphere and assign it to the blender mesh.
        bm = bmesh.new()
        bmesh.ops.create_icosphere(bm, subdivisions=2 , diameter=random.uniform(0.05,0.15))
        bm.to_mesh(mesh)

        #bpy.ops.object.shade_smooth()
        

        #bpy.ops.object.select_all(action='DESELECT')
        #object.select = True
        #bpy.context.scene.objects.active = object
        #setMode( "EDIT" )
        #bpy.ops.mesh.select_all(action = 'DESELECT')

        #bpy.ops.object.mode_set(mode = 'OBJECT')
        #object.data.polygons[5].select = True
        #setMode( "EDIT" )

        # Extrude the mesh a random number of times:
        extrusions = random.randint(1,4)
        bm.faces.ensure_lookup_table()
        
        setMode( "EDIT" )
        for i in range( 0, extrusions ):
            
            for v in bm.verts:
                v.select_set(False)
            bm.select_flush(False)
            bm.faces.ensure_lookup_table()
            id = random.randint(0, len(bm.faces)-1)
            face = bm.faces[id]
            face.select_set(True)
            
            select_more(bm, ntimes=random.randint(1,2))
            
            # Randomly scale the selected face:
            #randomScale( bm, i, extrusions, face.verts )
            
            #bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 0.05), "constraint_axis":(False, False, True), "constraint_orientation":'NORMAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "gpencil_strokes":False, "texture_space":False, "remove_on_cancel":False, "release_confirm":False})
            #bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value":(0.05,0,0)})
            selection = [f for f in bm.faces if f.select]
            extruded = bmesh.ops.extrude_face_region(bm, geom=selection)
            vec = -selection[0].normal*0.05
            bmesh.ops.delete(bm, geom=selection, context="FACES_ONLY") # Delete previous faces
            verts = [v for v in extruded['geom'] if isinstance(v, bmesh.types.BMVert)]
            faces = [f for f in extruded['geom'] if isinstance(f, bmesh.types.BMFace)]
            #vec = mathutils.Vector((0.05, 0, 0))
            bmesh.ops.translate(bm, vec=vec, verts=verts)
            

        setMode( "OBJECT" )
        bm.to_mesh(obj.data)
        bm.free()
        
        
        # Randomly rotate the object:
        setMode( "OBJECT" )
        eul = mathutils.Euler((random.uniform(0,2*math.pi),random.uniform(0,2*math.pi),random.uniform(0,2*math.pi)), 'XYZ')
        obj.rotation_mode = "QUATERNION"
        obj.rotation_quaternion = eul.to_quaternion()
        bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)
        obj.rotation_euler = eul
        
        # Duplicate object:
        # me_copy = obj.data.copy()
        # duplicated = bpy.data.objects.new("RandomMeshCopy", me_copy)
        # vec = mathutils.Vector((0,0,0))
        # while vec.length < 0.05:
        #     x = random.uniform(-0.15,0.15 )
        #     y = random.uniform(-0.15,0.15 )
        #     z = random.uniform(-0.15,0.15 )
        #     vec = mathutils.Vector((x,y,z))
        # duplicated.location = vec
        # x=random.uniform(-math.pi,math.pi)
        # y=random.uniform(-math.pi,math.pi)
        # z=random.uniform(-math.pi,math.pi)
        # duplicated.rotation_euler = (x,y,z)
        # bpy.context.collection.objects.link( duplicated )
        bpy.context.view_layer.update()

        
        # Subtract the duplicate from this object:
#         boolModifier = obj.modifiers.new(type="BOOLEAN", name="bool")
#         boolModifier.object = duplicated
#         boolModifier.operation = "DIFFERENCE"
#         bpy.ops.object.modifier_apply(apply_as="DATA",modifier="bool")
    
        # Move the object (and the curve) to fit into a predefined bounding box:
        local_bbox_center = 0.125 * sum((mathutils.Vector(b) for b in obj.bound_box), mathutils.Vector())
        origOffset = -local_bbox_center     # Offset to move object into world origin
        obj.location = origOffset

        # Offset to randomly place object inside bounding box:
        maxOffset = (mathutils.Vector((args.bounds, args.bounds, args.bounds)) - obj.dimensions)*0.5
        randOffset = mathutils.Vector(( random.uniform(-maxOffset.x,maxOffset.x),
                            random.uniform(-maxOffset.y,maxOffset.y),
                            random.uniform(-maxOffset.z,maxOffset.z) ))
        obj.location += randOffset

        # Bisects and triangulate
        setMode( "EDIT" )
        bpy.ops.mesh.select_all(action='SELECT')      

        bbox = [mathutils.Vector(a) for a in obj.bound_box]
        #print('dim: ', obj.dimensions)
        x = random.uniform(-0.15,0.15 )
        y = random.uniform(-0.15,0.15 )
        z = random.uniform(-0.15,0.15 )
        plane_co = (x, y, z)
        
        plane_no=(0.0, 1.0, 0.0)
        bpy.ops.mesh.bisect(plane_co=plane_co, plane_no=plane_no, use_fill=True, clear_outer=True)

        me = obj.data
        bm = bmesh.from_edit_mesh(me)
        selection = [f for f in bm.faces if f.select]
        bmesh.ops.triangulate(bm, faces=selection, quad_method="BEAUTY", ngon_method="BEAUTY")
        bmesh.update_edit_mesh(me, True)

        bpy.ops.mesh.select_all(action='SELECT')
        # offset difficult to decide... 
        offset = args.thickness
        y_offset = random.uniform(-offset[1], -offset[0])
        y = y + y_offset
        plane_co = (x, y, z)
        bpy.ops.mesh.bisect(plane_co=plane_co, plane_no=plane_no, use_fill=True, clear_inner=True)

        selection = [f for f in bm.faces if f.select]
        bmesh.ops.triangulate(bm, faces=selection, quad_method="BEAUTY", ngon_method="BEAUTY")
        bmesh.update_edit_mesh(me, True)
         
        # Remeshing until the average edge length is lower than grid voxel diagonal
        scale_factor = [0.7, 0.8, 0.9, 1.0]
        for s in scale_factor:
            cleanMesh( obj, remesh_scale=s, add_crease=False )
            mean_edge_len = getAverageEdgeLength(obj)
            #print('Average edge length: ', mean_edge_len)
            #print('Number of vertices: ', len(object.data.vertices))

            if mean_edge_len < math.sqrt(3)*args.thickness[0]:
                #print('EXITING, scale factor: ', s)
                break

        # Add predeformation if needed
        if args.predeform:
            setMode( "OBJECT" )
            twist_deform = obj.modifiers.new(type="SIMPLE_DEFORM", name="twist")
            twist_deform.deform_method = 'TWIST'
            twist_deform.angle = random.uniform(-math.pi/2., math.pi/2.)
            bpy.ops.object.modifier_apply(apply_as="DATA",modifier="twist")

            bend_deform = obj.modifiers.new(type="SIMPLE_DEFORM", name="bend")
            bend_deform.deform_method = 'BEND'
            bend_deform.angle = random.uniform(-math.pi/2., math.pi/2.)
            bpy.ops.object.modifier_apply(apply_as="DATA",modifier="bend")

        #######################
        # Rotate again
        # setMode( "OBJECT" )
        # eul = mathutils.Euler((random.uniform(0,2*math.pi),random.uniform(0,2*math.pi),random.uniform(0,2*math.pi)), 'XYZ')
        # object.rotation_mode = "QUATERNION"
        # object.rotation_quaternion = eul.to_quaternion()
        # bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)
        # object.rotation_euler = eul
        #######################

        # Clean up possible non-manifolds
        removeNonManifolds( obj )
    
        setMode('OBJECT')
        # Scale object down if necessary:
        for dim in obj.dimensions:
            if dim > args.bounds:
                print("Object is greater than maximum bounds ({:f}, current size {:f}). Scaling down.".format(args.bounds, dim))
                scale = 0.9*args.bounds/dim
                obj.scale = (scale,scale,scale)
                #raise ValueError("Object is greater than maximum bounds ({:f})".format(args.bounds))

        # Remove all by the wanted object:
        for o in bpy.data.objects:
            o.select_set(True)
            if "RandomMesh" in o.name and not "RandomMeshCopy" in o.name:
                o.select_set(False)
            
        bpy.ops.object.delete()

        #########################################################
        ## Export mesh:
        for ob in bpy.data.objects:
            ob.select_set(True)

        filepath = os.path.join(outdir,str('surface.stl'))
        bpy.ops.export_mesh.stl(filepath=filepath, check_existing=False, filter_glob="*.stl", ascii=True, use_mesh_modifiers=True, axis_forward='Y', axis_up='Z', global_scale=1.0)
        print("Generated random mesh. Exported to " + filepath)
        
        # Successfull saving: remove idx from list
        i = samplesIdx.pop(0)
        num_attempt = 1
            
    except:
        print( f"Could not generate model using seed {i}. Trying with a new random seed." )
        print( "Unexpected error:", sys.exc_info()[0] )
        #print( traceback.format_exc() )

        #raise
        num_attempt += 1

# #bpy.ops.wm.quit_blender()


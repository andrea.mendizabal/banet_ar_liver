import bpy
import mathutils
import random
import math
import os
import sys
import bmesh
import argparse
import traceback

d = os.path.dirname(bpy.data.filepath)
#print("path:", d, bpy.data.filepath)
if not d in sys.path:
    sys.path.append(d)
    sys.path.append(os.path.join(d, "..", "Utils"))
    sys.path.append(os.path.join(d, "Utils"))
from vtkutils import *
from generalutils import *
from blenderutils import *

# Remove all arguments passed to Blender, only take those after the double dash '--' into account:
argv = sys.argv
if "--" in argv:
    argv = argv[argv.index("--") + 1:]
else:
    argv = []

parser = argparse.ArgumentParser(description="Generate a random mesh using Blender functionality. Tested with Blender 2.82.")
parser.add_argument("--random_seed", type=int, default=None, help="Number to seed the random generator with. If [num] is greater than 1, the ID of the mesh will be used as seed." )
parser.add_argument("--num", type=int, default=1, help="Number of meshes to generate.")
parser.add_argument("--start_num", type=int, default=0, help="Optionally start from this simulation number.")
parser.add_argument("--outdir", type=path, default=".", help="Folder in which to save the random mesh.")
parser.add_argument("--bounds", type=float, default=0.3, help="Edge of the cube defining the volume where the object must lie.")
parser.add_argument("--max_cuts", type=int, default=0, help="Add random cuts to the original preoperative mesh. In this case, two meshes will be written out: The original mesh without cuts and the manipulated mesh with cuts.")
args = parser.parse_args(argv)


for i in range(args.start_num,args.start_num+args.num):

    try:
        outdir = path( os.path.join( args.outdir, "{:06d}".format(i) ) )
        
        if args.num > 1:
            random.seed( i )
        else:
            random.seed(args.random_seed)

        # Go to object mode if possible:
        if bpy.context.object != None:
            if bpy.context.object.mode != 'OBJECT':
                setMode( "OBJECT" )

        # Clear previous meshes:
        for o in bpy.data.objects:
            o.select_set(False)
        for o in bpy.data.objects:
            if "RandomMesh" in o.name:
                o.select_set(True)
        bpy.ops.object.delete()
        
        scene = bpy.context.scene
        
        # Create an empty mesh and the object.
        mesh = bpy.data.meshes.new('RandomMesh')
        obj = bpy.data.objects.new("RandomMesh", mesh)

        # Add the object into the scene.
        bpy.context.collection.objects.link( obj )
        bpy.context.view_layer.objects.active = obj
        obj.select_set(True)

        # Construct the bmesh sphere and assign it to the blender mesh.
        bm = bmesh.new()
        bmesh.ops.create_icosphere(bm, subdivisions=2 , diameter=random.uniform(0.05,0.15))
        bm.to_mesh(mesh)

        #bpy.ops.object.shade_smooth()
        

        #bpy.ops.object.select_all(action='DESELECT')
        #object.select = True
        #bpy.context.scene.objects.active = object
        #bpy.ops.object.mode_set(mode='EDIT')
        #bpy.ops.mesh.select_all(action = 'DESELECT')

        #bpy.ops.object.mode_set(mode = 'OBJECT')
        #object.data.polygons[5].select = True
        #bpy.ops.object.mode_set(mode = 'EDIT')

        # Extrude the mesh a random number of times:
        extrusions = random.randint(1,4)
        bm.faces.ensure_lookup_table()
        
        setMode( "EDIT" )
        for i in range( 0, extrusions ):
            
            for v in bm.verts:
                v.select_set(False)
            bm.select_flush(False)
            bm.faces.ensure_lookup_table()
            id = random.randint(0, len(bm.faces)-1)
            face = bm.faces[id]
            face.select_set(True)
            
            select_more(bm, ntimes=random.randint(1,2))
            
            # Randomly scale the selected face:
            #randomScale( bm, i, extrusions, face.verts )
            
            #bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 0.05), "constraint_axis":(False, False, True), "constraint_orientation":'NORMAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "gpencil_strokes":False, "texture_space":False, "remove_on_cancel":False, "release_confirm":False})
            #bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value":(0.05,0,0)})
            selection = [f for f in bm.faces if f.select]
            extruded = bmesh.ops.extrude_face_region(bm, geom=selection)
            vec = -selection[0].normal*0.05
            bmesh.ops.delete(bm, geom=selection, context="FACES_ONLY") # Delete previous faces
            verts = [v for v in extruded['geom'] if isinstance(v, bmesh.types.BMVert)]
            faces = [f for f in extruded['geom'] if isinstance(f, bmesh.types.BMFace)]
            #vec = mathutils.Vector((0.05, 0, 0))
            bmesh.ops.translate(bm, vec=vec, verts=verts)
            

        setMode( "OBJECT" )
        bm.to_mesh(obj.data)
        bm.free()
        
        
        # Randomly rotate the object:
        setMode( "OBJECT" )
        eul = mathutils.Euler((random.uniform(0,2*math.pi),random.uniform(0,2*math.pi),random.uniform(0,2*math.pi)), 'XYZ')
        obj.rotation_mode = "QUATERNION"
        obj.rotation_quaternion = eul.to_quaternion()
        bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)
        obj.rotation_euler = eul
        
        # Duplicate object:
        me_copy = obj.data.copy()
        duplicated = bpy.data.objects.new("RandomMeshCopy", me_copy)
        vec = mathutils.Vector((0,0,0))
        while vec.length < 0.05:
            x = random.uniform(-0.15,0.15 )
            y = random.uniform(-0.15,0.15 )
            z = random.uniform(-0.15,0.15 )
            vec = mathutils.Vector((x,y,z))
        duplicated.location = vec
        x=random.uniform(-math.pi,math.pi)
        y=random.uniform(-math.pi,math.pi)
        z=random.uniform(-math.pi,math.pi)
        duplicated.rotation_euler = (x,y,z)

        # Add to scene
        bpy.context.collection.objects.link( duplicated )
        #scene.update()
        bpy.context.view_layer.update()
        
        # Subtract the duplicate from this object:
        boolModifier = obj.modifiers.new(type="BOOLEAN", name="bool")
        boolModifier.object = duplicated
        boolModifier.operation = "DIFFERENCE"
        bpy.ops.object.modifier_apply(apply_as="DATA",modifier="bool")
        
        # Scale object down if necessary:
        for dim in obj.dimensions:
            if dim > args.bounds:
                print("Object is greater than maximum bounds ({:f}, current size {:f}). Scaling down.".format(args.bounds, dim))
                scale = 0.9*args.bounds/dim
                obj.scale = (scale,scale,scale)
                #bpy.context.scene.objects.active.scale = (scale,scale,scale)
                #bpy.context.view_layer.objects.active = obj
                #raise ValueError("Object is greater than maximum bounds ({:f})".format(args.bounds))
                
        # Move the object (and the curve) to fit into a predefined bounding box:
        local_bbox_center = 0.125 * sum((mathutils.Vector(b) for b in obj.bound_box), mathutils.Vector())
        origOffset = -local_bbox_center     # Offset to move object into world origin
        obj.location = origOffset

        # Offset to randomly place object inside bounding box:
        maxOffset = (mathutils.Vector((args.bounds, args.bounds, args.bounds)) - obj.dimensions)*0.5
        randOffset = mathutils.Vector(( random.uniform(-maxOffset.x,maxOffset.x),
                            random.uniform(-maxOffset.y,maxOffset.y),
                            random.uniform(-maxOffset.z,maxOffset.z) ))
        obj.location += randOffset

        #########################################################
        ## Cleanup:

        cleanMesh( obj )
        removeNonManifolds( obj )

        setMode( "OBJECT" )
        
        # Remove all but the wanted object:
        for o in bpy.data.objects:
            o.select_set(True)
            if "RandomMesh" in o.name and not "RandomMeshCopy" in o.name:
                o.select_set(False)
            
        bpy.ops.object.delete()
        
        #########################################################
        ## Export mesh:

        for o in bpy.data.objects:
            o.select_set(True)
        filepath = os.path.join(outdir,str('surface.stl'))
        bpy.ops.export_mesh.stl(filepath=filepath, check_existing=False, filter_glob="*.stl", ascii=True, use_mesh_modifiers=True, axis_forward='Y', axis_up='Z', global_scale=1.0)
        print("Generated random mesh. Exported to " + filepath);


        #########################################################
        ## Manipulated mesh:

        manipulateMesh = False

        if args.max_cuts > 0:
            manipulateMesh = True

        if manipulateMesh:
            mesh = obj.data.copy()
            objManip = obj.copy()
            objManip.data = obj.data.copy()
            objManip.name = "RandomMeshManipulated"
            bpy.context.collection.objects.link( objManip )

            #numCuts = random.randint( 0, args.max_cuts )
            numCuts = args.max_cuts
            print("Adding {} cuts".format( numCuts ))
            for c in range(numCuts):

                # Go to object mode if possible:
                setMode( "OBJECT" )
                
                # Create an empty mesh and the object:
                cutoutMesh = bpy.data.meshes.new("Cutout")
                cutout = bpy.data.objects.new("Cutout", cutoutMesh)

                # Add the object into the scene:
                #scene.objects.link(cutout)
                #scene.objects.active = cutout
                bpy.context.collection.objects.link( cutout )
                bpy.context.view_layer.objects.active = cutout
                cutout.select_set(True)

                # Construct the bmesh cube and assign it to the blender mesh:
                bm = bmesh.new()
                cutWidth = random.uniform( 0.001, 0.01 )
                cutHeight = random.uniform(0.02,0.1)
                print("CUT:", cutWidth, cutHeight)
                # Cut depth:
                #scaleMat = mathutils.Matrix.Scale( 1/cutWidth*cutHeight, 4, (0.0, 0.0, 1.0))
                #bmesh.ops.create_cube(bm, size=cutWidth, matrix=scaleMat )
                bmesh.ops.create_grid(bm, x_segments=1, y_segments=1, size=1 )
                bmesh.ops.scale( bm, vec=(cutWidth, cutHeight, 0), verts=bm.verts )
                #bm.to_mesh(cutoutMesh)

                # Extrude the mesh a random number of times:
                extrusions = 5
                extrudeLength = random.uniform( 0.01, 0.05 )
                bm.faces.ensure_lookup_table()
                
                setMode( "EDIT" )

                for v in bm.verts:
                    v.select_set(False)

                bm.select_flush(False)
                bm.faces.ensure_lookup_table()
                faceID = 0
                face = bm.faces[faceID]
                face.select_set(True)

                # Get the edges of the original face:
                origEdges = []
                for e in bm.edges:
                    origEdges.append(e)
                
                bm.verts.ensure_lookup_table()
                edge0 = [bm.verts[0].co]
                edge1 = [bm.verts[1].co]
                edge2 = [bm.verts[2].co]
                edge3 = [bm.verts[3].co]

                for i in range( 0, extrusions ):
                    extruded = bmesh.ops.extrude_face_region(bm, geom=[face])
                    vec = -face.normal*extrudeLength + mathutils.Vector( (
                            random.uniform(-0.01, 0.01),
                            random.uniform(-0.01, 0.01),
                            random.uniform(-0.01, 0.01)) )
                    bmesh.ops.delete(bm, geom=[face], context="FACES_ONLY") # Delete previous faces
                    verts = [v for v in extruded['geom'] if isinstance(v, bmesh.types.BMVert)]
                    #faces = [f for f in extruded['geom'] if isinstance(f, bmesh.types.BMFace)]
                    #vec = mathutils.Vector((0.05, 0, 0))
                    bmesh.ops.translate(bm, vec=vec, verts=verts)
                    
                    # Add the new vertices to the corresponding (long) edges
                    edge0.append(verts[0].co)
                    edge1.append(verts[1].co)
                    edge2.append(verts[2].co)
                    edge3.append(verts[3].co)
                   
                    # Choose the new face to be the next one which is extruded:
                    faces = [f for f in extruded['geom'] if isinstance(f, bmesh.types.BMFace)]
                    face = faces[0]

                setMode( "OBJECT" )

                # Fill the original face:
                bmesh.ops.edgeloop_fill(bm, edges=origEdges)
                
                for v in bm.verts:
                    v.select_set(False)
                for e in bm.edges:
                    e.select_set(False)

                #for v in edge1:
                #    v.select = True

                bm.to_mesh(cutout.data)
                bm.free()

                object_bbox = [objManip.matrix_world @ mathutils.Vector(b)
                        for b in objManip.bound_box]

                validSideOverlap = False
                tries = 0
                while not validSideOverlap and tries < 500:
                    # Randomly rotate the cutout object:
                    eul = mathutils.Euler((random.uniform(0,2*math.pi),random.uniform(0,2*math.pi),random.uniform(0,2*math.pi)), 'XYZ')
                    cutout.rotation_mode = "QUATERNION"
                    cutout.rotation_quaternion = eul.to_quaternion()
                    #bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)
                    #cutout.rotation_euler = eul

                    xs = [v.x for v in object_bbox]
                    ys = [v.y for v in object_bbox]
                    zs = [v.z for v in object_bbox]
                    minX = min(xs)
                    maxX = max(xs)
                    minY = min(ys)
                    maxY = max(ys)
                    minZ = min(zs)
                    maxZ = max(zs)

                    # Randomly move the cutout object within the organ object's bounds:
                    shift = mathutils.Vector((
                        random.uniform( min(xs), max(xs) ),
                        random.uniform( min(ys), max(ys) ),
                        random.uniform( min(zs), max(zs) )))
                    cutout.location = shift
                    bpy.context.view_layer.update()
                   
                    validSideOverlap = checkSideOverlap( objManip, cutout, edge0, edge1, edge2, edge3 )
                    tries = tries + 1
                print("Number of tries to position to cutting mesh:", tries)
                #exit()

                if validSideOverlap:
                    # Smooth:
                    cutout.modifiers.new(type="SUBSURF", name="subdivision")
                    bpy.ops.object.modifier_apply(apply_as="DATA",modifier="subdivision")

                    # Cut, then delete:
                    boolean = objManip.modifiers.new(type="BOOLEAN", name="boolean")
                    boolean.operation = "DIFFERENCE"
                    boolean.object = cutout
                    bpy.context.view_layer.objects.active = objManip
                    bpy.ops.object.modifier_apply(apply_as="DATA",modifier="boolean")

                for o in bpy.data.objects:
                    o.select_set(False)
                    if "Cutout" in o.name:
                        o.select_set(True)
                bpy.ops.object.delete()
                #for o in bpy.data.objects:
                #    print("non-cutout:", o.name)
                


            ## Clean resulting mesh:
            #print(mesh, objManip, objManip.data)
            cleanMesh( objManip, remesh_octree_depth=6, subdiv=False )
            removeNonManifolds( objManip )
            objManip = bpy.data.objects["RandomMeshManipulated"]    # May have been replaced by cleanMesh call

            #########################################################
            ## Export manipulated mesh:

            #print("NUM objects", len(bpy.data.objects))

            for o in bpy.data.objects:
                #print("OBJECT:", o.name, o.select)
                o.select_set(True)
            objManip.select_set(False)
            bpy.ops.object.delete()
            
            #for o in bpy.data.objects:
            #    print("OBJECT REMAINING:", o.name, o.select)

            bpy.context.view_layer.objects.active = objManip
            objManip.select_set(True)

            filepath = os.path.join(outdir,str('surface_manip.stl'))
            bpy.ops.export_mesh.stl(filepath=filepath, check_existing=False, filter_glob="*.stl", ascii=True, use_mesh_modifiers=True, axis_forward='Y', axis_up='Z', global_scale=1.0 )
            print("Generated random mesh. Exported to " + filepath);


           
    except:
        print( "Could not generate model. Seed:", i )
        print( "Unexpected error:", sys.exc_info()[0] )
        print( traceback.format_exc() )

#bpy.ops.wm.quit_blender()

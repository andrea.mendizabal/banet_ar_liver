# Random Surface Mesh Generation

## Usage:

Scripts in this folder must be started from within blender. 
To run the script **without** blender GUI:
```
	blender --background -noaudio --python generate_random_meshes.py -- --num 10 [Further options]
```

while to run the script **with** blender GUI:
```
	blender --python generate_random_meshes.py -- --num 1 [Further options]
```
The option `num` specifies the number of samples to generate. Other input options are detailed in each script.
The script `generate_random_flat_meshes.py` starts from the base script `generate_random_meshes.py` but cuts the generated meshes within two parallel planes, whose normal is in the y direction.


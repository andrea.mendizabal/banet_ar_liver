---
parallel_processes: 1

# Control the boundary conditions which are randomly set up for each simulation:
boundary_conditions:

    # Parameters controlling the creation of a fixed region
    fixed:
        active: True

        # Weights for surface extraction criterium for boundary points.
        # surface_amount = 1 seems to mean that all the points are fixed (should be between 0 and 1)
        # surface_amount_min _max = _0.5 _1 fixes only half of the nodes
        # surface_amount_min _max = _0.001 _1 fixes few nodes
        surface_amount_min: 0.01
        surface_amount_max: 0.5

        weight_distance_min: 0 # how much we want to account for the distance from c
        weight_distance_max: 1 #10

        # weight normal = 0 seems to mean that points inside a sphere will be fixed
        # weight normal_max = 10 seems to mean that only surface points will be fixed
        weight_normal_min: 0 # high values here mean that points belonging to the same surface patch as c will be kept.
        weight_normal_max: 0

        weight_noise_min: 0 # how much we want noise to influence roi selection
        weight_noise_max: 0.5

    # Parameters controlling the creation of the surrounding tissue pushing from below:
    tissue:
        active: False
        uniform: True #Toggle whether the same stiffness value has to be associated to all the springs

        # Stiffness of each spring, influenced by perlin noise.
        stiffness_min: 0.000001 # 0.000001
        stiffness_max: 1 #40

        topology_offset_max: 0  # 0 means no offset for springs end points wrt rest positions
        topology_noise_max: 0   # no perlin noise for springs' end point distance

    forces:
        active: True #Toggle whether we want to apply random nodal forces
        incremental: True
        pull_only: False
        min_magnitude: 0.001 #0.001 # Minimum magnitude of the random nodal force
        max_magnitude: 0.01 # Maximum magnitude of the random nodal force
        roi_min: 0.01 # Minimum radius of the ROI where the nodal force is applied
        roi_max: 0.08 # Maximum radius of the ROI where the nodal force is applied
        nb_non_consecutive_def: 1 # Number of non consecutive deformations that will be generated

simulation:
    # Name of the simulation scene.
    scene_name: "sofa_scene_liver_fixed.py"
    dt: 1.0 #time step of the simulation
    gravity: [0, 0, 0] # gravity
    analysis: "static"
    use_hexas: True

    # Apply force incrementally
    num_simulation_step: 5 #9 # if it is lower than frames_to_export, it is set to frames_to_export

model:
    fem:
        #material: Corotated
        #range for omental (=visceral) adipose tissue: [2.9k-32k]
        #range for subcutaneous adipose tissue: [1.6k-11.7k]
        E_min: 5000
        E_max: 5000
        nu_min: 0.4
        nu_max: 0.4


intraoperative_surface:
    surface_amount_min: 0.1
    surface_amount_max: 1.0

    # Weights for surface extraction criterium.
    # We then calculate the weighted sum using the weights given here for every point:
    # S = weight_distance*D + weight_normal*N + weight_noise*R
    # Finally, a threshold is sought so that "surface_amount" points are kept while the
    # others are discarded. Points where S > threshold are discarded.
    weight_distance_min: 5
    weight_distance_max: 10
    weight_normal_min: 1
    weight_normal_max: 2
    weight_noise_min: 1
    weight_noise_max: 2

    rigid_offset:
        active: False

    subdivide_surface:
        active: False
        subdiv_factor: 3

    sparsify_surface:
        active: False
        scale_min: 0
        scale_max: 1.5
        shift_min: -0.1
        shift_max: 0

    noise_amount: 0.0 # setting to 0 removes noise from extracted partial surface

    max_surface_pieces: 1

cleaning:
    min_displacement: 0.002 # if I want min 5mm per frame, min_displ should be 5mm*frames_to_export = 1.5cm
    max_displacement: 0.2 # length of the beam

voxelization:
    voxelization_script: "Voxelization/voxelize_sample_liver_fixed.py"
    voxelization_filename: "voxelized.vts" #name of the voxelization file

io:
    #Number of frames to export
    frames_to_export: 3

    save_debug_data: False
...

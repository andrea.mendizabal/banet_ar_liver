"""
Create and run a scene which roughly simulates a laparoscopic setting.
"""

# Python imports
import numpy as np
import sys
import os
from vtk import *
import glob

# Sofa imports
import Sofa.Simulation
import SofaRuntime
import Sofa.Gui

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), "../components"))

import utils
from sofaobject import SofaObject
from sofaobject import Material
from solver import Solver
from boundaries import FixedBoundaries, RestShapeSpringBoundaries
from roi import ROI
from forces import NodalForce


class Organ(Sofa.Core.Controller):
    def __init__(self, rootNode, opt):
        Sofa.Core.Controller.__init__(self)
        self.opt = opt

        self.createGraph(rootNode)
        # return None

    ## ------------------------------------------------------------------- ##
    ##                          SCENE CREATION                             ##
    ## ------------------------------------------------------------------- ##
    def createGraph(self, rootNode):
        self.rootNode = rootNode

        # CREATE SCENE
        rootNode.findData('dt').value = self.opt['simulation']['dt']
        rootNode.findData('gravity').value = self.opt['simulation']['gravity']
        rootNode.addObject('VisualStyle', displayFlags='showBehavior')
        rootNode.findData('animate').value = False  # True

        # Mesh
        self.undeformed_surface_mesh = self.opt['filenames']['surface_mesh']
        undeformed_tetra_mesh = None
        self.use_hexas = self.opt['simulation']['use_hexas']
        if not self.use_hexas:
            undeformed_tetra_mesh = self.opt['filenames']['tetra_mesh']

        # Create output directory
        self.output_folder = self.opt['filenames']['outdir']
        if not os.path.exists(self.output_folder):
            utils.mkdir(self.output_folder)

        # Setup number of frames to export
        frames_to_export = self.opt['io']['frames_to_export']
        self.frames_to_export = frames_to_export

        # Number of simulation steps to run per deformation/force. Force is applied incrementally on these steps.
        self.num_steps_per_def = self.opt['simulation']['num_simulation_step']
        if self.num_steps_per_def < frames_to_export:
            print(
                "The specified number of simulation steps is lower than the number of frames to export. Setting the two values to ",
                frames_to_export)
            self.num_steps_per_def = frames_to_export

        self.nb_non_consecutive_def = self.opt["boundary_conditions"]["forces"]["nb_non_consecutive_def"]
        self.total_num_steps = self.num_steps_per_def * self.nb_non_consecutive_def


        # Material
        material = Material(
            young_modulus=self.opt['model']['fem']['E'],
            poisson_ratio=self.opt['model']['fem']['nu'],
            constitutive_model=Material.COROTATED,
            mass_density=self.opt['model']['rho'],
        )

        # Grid
        organ = SofaObject(
            parent_node=rootNode,
            surface_mesh=self.undeformed_surface_mesh,
            volume_mesh=undeformed_tetra_mesh,
            sparse_grid=True,
            cell_size=0.063,
            material=material
        )
        self.organ = organ

        analysis = self.opt['simulation']['analysis']
        self.analysis = analysis
        if analysis == "static":
            self.solver = Solver(
                parent_node=organ.node,
                analysis=Solver.STATIC,
                newton_iterations=20,
                solver=Solver.CG,
                solver_name='Solver',
                print_log=True
            )
        else:
            self.solver = Solver(
                parent_node=organ.node,
                analysis=Solver.DYNAMIC,
                solver=Solver.CG,
                solver_name='Solver',
                num_iter=500
            )
            organ.node.addObject('MeshMatrixMass', massDensity=material.mass_density, name='myMatrixMass')

        # Exit criteria for dynamic simulation
        self.max_time = self.opt['simulation']['max_time']
        self.mean_acceleration_thresh = self.opt['simulation']['mean_acceleration_thresh']
        self.mean_velocity_thresh = self.opt['simulation']['mean_velocity_thresh']
        self.min_simulation_steps = self.opt['simulation']['min_simulation_steps']

        # Setup array of boundaries for exporting
        if self.use_hexas:
            nNodes = len(organ.hexa_mesh_topology.position.value)
        else:
            nNodes = len(organ.tetra_mesh_topology.position.value)
        self.boundariesToExport = np.zeros((nNodes,))

        # Setup boundary conditions
        fixedActive = self.opt['boundary_conditions']['fixed']['active']
        springsActive = self.opt['boundary_conditions']['tissue']['active']
        valueForFixed = self.opt['boundary_conditions']['fixed']['spring_value']
        maxStiffness = self.opt['boundary_conditions']['tissue']['stiffness_max']

        tissueFiles = self.opt['filenames']['surrounding_tissue']
        fixedBCfiles, springBCfiles = self.sort_tissue_files(tissueFiles, valueForFixedBC=valueForFixed)

        if fixedActive:
            print("Creating fixed boundary conditions..")
            self.create_fixed_boundaries(organ, fixedBCfiles)

        if springsActive:
            print("Creating spring boundary conditions..")
            self.create_spring_boundaries(organ, springBCfiles)

        # Setup force variable
        forcesFile = self.opt['filenames']['forces']
        self.forcesFiles = glob.glob(str(forcesFile[:-4]) + '*')


        # After boundariesToExport has been created, normalize and convert to list
        self.boundariesToExport = normalizeSpringsForExport(self.boundariesToExport, valueForFixedBC=valueForFixed,
                                                            maxStiffness=maxStiffness)
        self.boundariesToExport = self.boundariesToExport.tolist()

        self.deformedStates = []
        self.time = 0
        self.step = 0
        self.prevVel = None
        self.vel_trend = []

    ## ------------------------------------------------------------------- ##
    ##               SOFA CONTROLLER FUNCTIONS                             ##
    ## ------------------------------------------------------------------- ##
    def onSimulationInitDoneEvent(self, event):

        surface_pts = np.asarray(self.organ.surface_mesh_loader.position.value)
        volume_pts = np.asarray(self.organ.behavior_state.position.value)

        if self.opt['boundary_conditions']['fixed']['active']:
            # Convert to volume indices to correctly define fixed constraint
            surface_pts_fixed = surface_pts[self.surface_fixedIDs]
            __, volume_fixedIDs = utils.get_distance_np(volume_pts, surface_pts_fixed)
            volume_fixedIDs = (volume_fixedIDs.astype(int)).tolist()
            self.fixed_boundaries.node.indices = volume_fixedIDs

        # Initialize force node
        if self.use_hexas:
            self.forceNode = NodalForce(force_node=self.organ.forceNode)
        #
        # print("self.forceNode initialized ", self.forceNode)
        # else:
        #     self.forceNode = NodalForce(
        #         parent_node=self.organ.node,
        #         filename=self.undeformed_surface_mesh,
        #     )

    def onAnimateBeginEvent(self, value):

        # Setup new forces when needed
        if self.step == 0 or self.step % self.num_steps_per_def == 0:
            print("Creating forces ...")
            self.create_forces(self.forcesFiles)
        # elif self.step % self.num_steps_per_def != 0 and self.step < self.num_steps_per_def * self.nb_non_consecutive_def:
        else:
            print("Updating forces ...")
            self.forceNode.update_forces(self.delta_force)


    def onAnimateEndEvent(self, value):
        self.time += value['dt']
        self.step += 1
        sim_end = False
        if self.analysis == "static":
            # Check if solver diverged
            if self.solver.StaticODESolver.converged.value == 0:
                print('SIMULATION UNSTABLE: exiting without exporting...')
                os._exit(0)

            # # Check if the simulation is unstable
            # pred_pos = np.array(self.organ.behavior_state.position.value)
            # displ = pred_pos - np.array(self.organ.behavior_state.rest_position.value)
            # if not utils.check_valid_displacement(displ, low_thresh=-0.01):
            #     print
            #     'SIMULATION UNSTABLE: exiting without exporting...'
            #     os._exit(0)

            if self.step >= self.total_num_steps:
                sim_end = True

            # elif self.step % self.nb_non_consecutive_def == 1:
            #     self.exportResult(num=self.frames_to_export)

        elif self.analysis == "dynamic":
            dx = self.organ.behavior_state.findData('velocity').value
            vel = [np.asarray(d) for d in dx]

            vSum = 0
            for v in vel:
                vSum += np.linalg.norm(v)

            vAvg = vSum / len(vel)
            print("Average velocity:", vAvg)
            self.vel_trend.append(vAvg)

            if self.prevVel:
                accelSum = 0
                for i, v in enumerate(vel):
                    velDiff = np.linalg.norm(v - self.prevVel[i])
                    accelSum = accelSum + velDiff
                accelAvg = accelSum / len(vel)
                print("Average acceleration:", accelAvg)
                if vAvg < self.mean_velocity_thresh and accelAvg < self.mean_acceleration_thresh and self.step > self.min_simulation_steps:
                    sim_end = True

            self.prevVel = vel

            if self.time > self.max_time:
                # If the average simulation velocity is much higher than the final, it means that simulation is unstable
                if np.mean(self.vel_trend) > 2 * vAvg:
                    print('Simulation is unstable, exiting without exporting results')
                    os._exit(0)

                sim_end = True

        ###########################################
        # Append current deformed state to list
        deformedVtkState = result2vtk(self.organ.surface_state.position.value,
                                      self.boundariesToExport,
                                      self.organ.triangle_mesh_topology)
        self.deformedStates.append(deformedVtkState)

        if sim_end:
            self.rootNode.findData('animate').value = False
            self.exportResult(num=self.frames_to_export * self.nb_non_consecutive_def)
            os._exit(0)

        if self.step == 0 or self.step % self.num_steps_per_def == 0:
            self.reset()

    def reset(self):

        print("Simulation is being reset ... ")
        # Reset mechanical objects
        self.organ.behavior_state.position = self.organ.behavior_state.rest_position
        self.organ.surface_state.position = self.organ.surface_state.rest_position
        Sofa.Simulation.reset(self.rootNode)


    # self.organ.grid.grid_state.position = self.organ.grid.grid_state.rest_position
    # self.surface_state.position = self.surface_state.rest_position

    def storeResetState(self):
        print
        'storeResetState called (python side)'
        sys.stdout.flush()
        return 0

    def cleanup(self):
        print
        'cleanup called'

        # if self.opt['io']['save_data']:
        print
        'Saving variables before exiting'
        # Save something here when sofa is closed

        sys.stdout.flush()
        return 0

    def onKeyPressed(self, k):
        print
        'onKeyPressed ' + k
        if k == 'L':
            self.storeResetState()
            self.reset()

        sys.stdout.flush()

    ## ------------------------------------------------------------------- ##
    ##                         OTHER FUNCTIONS                             ##
    ## ------------------------------------------------------------------- ##
    def sort_tissue_files(self, tissueDescriptionFiles, valueForFixedBC=1000.0):
        if isinstance(tissueDescriptionFiles, str):
            tissueDescriptionFiles = [tissueDescriptionFiles]

        fixedBCfiles = []
        springBCfiles = []
        for lFile in tissueDescriptionFiles:
            if os.path.exists(lFile):

                data = np.load(lFile)
                if data["stiffness"][0] == valueForFixedBC:
                    fixedBCfiles.append(lFile)
                else:
                    springBCfiles.append(lFile)

        return fixedBCfiles, springBCfiles

    def create_spring_boundaries(self, organ, springDescriptionFiles):
        if isinstance(springDescriptionFiles, str):
            springDescriptionFiles = [springDescriptionFiles]

        nNodes = len(organ.surface_mesh_loader.position.value)
        stiffnessPerNode = np.zeros((nNodes,))
        for lFile in springDescriptionFiles:
            if os.path.exists(lFile):
                # name = os.path.basename(lFile)
                # print('Name:', name)
                data = np.load(lFile)
                stiffnessPerNode = appendSpringsForExport(data, stiffnessPerNode)
                self.boundariesToExport = appendSpringsForExport(data, self.boundariesToExport)

        # At the moment, we assume springs are attached to an embedded surface and forces are mapped back to the main mechanical object
        print("Springs are attached to a surface!")
        self.surface_springs = stiffnessPerNode
        if self.use_hexas:
            # If using hexas, such surface is created in organ.surface_node
            self.spring_boundaries = RestShapeSpringBoundaries(organ.surface_node, stiffnessPerNode.tolist(),
                                                               node_name='SpringBoundaries', view=True)
        else:
            # If you use tetras you can
            # 1) create a child node with an embedded surface
            # 2) apply springs to volume nodes BUT this will require mapping spring indices from surface to volume in onSimulationInitDoneEvent as done for fixed points
            raise NotImplementedError("Spring attachments for tetra simulation not implemented yet")

    def create_fixed_boundaries(self, organ, springDescriptionFiles):
        if isinstance(springDescriptionFiles, str):
            springDescriptionFiles = [springDescriptionFiles]

        fixedIDs = []
        for lFile in springDescriptionFiles:
            # Exported array has surface indices because we are exporting the surface
            if os.path.exists(lFile):
                data = np.load(lFile)
                fixedIDs.extend(data["startIDs"])
                self.boundariesToExport = appendSpringsForExport(data, self.boundariesToExport)

        # Save surface fixedIDs for later use
        self.surface_fixedIDs = fixedIDs

        # Initialize fixed points to zero and update them once simulation is initialized
        self.fixed_boundaries = FixedBoundaries(organ.node, indices=[0], node_name='FixedBoundaries')

    def create_forces(self, forceDescriptionFiles):
        if isinstance(forceDescriptionFiles, str):
            forceDescriptionFiles = [forceDescriptionFiles]

        if len(forceDescriptionFiles) != self.nb_non_consecutive_def:
            print('INCONSISTENT VALUE: the number of non consecutive deformations does not match the number of file forces...')
            os._exit(0)

        file_index = self.step // self.num_steps_per_def

        lFile = forceDescriptionFiles[file_index]

        if os.path.exists(lFile):
            data = np.load(lFile)
            forcesIDs = data["forcesIDs"].tolist()
            forces = data["forces"].tolist()
            print('Force magnitude: ', np.linalg.norm(forces))

            self.delta_force = np.asarray(forces) / self.num_steps_per_def
            forces = self.delta_force.tolist()

            self.forceNode.set_forces(forces, indices=forcesIDs)

    def exportResult(self, num=1):
        delta_save = len(self.deformedStates) // num
        idxs_to_save = [(i + 1) * delta_save - 1 for i in range(num)]

        if self.step % num:
            idxs_to_save[-1] = len(self.deformedStates) - 1

        for j, idx in enumerate(idxs_to_save):

            if len(idxs_to_save) == 1:
                deformed_name = "deformed.vtu"
            else:
                deformed_name = "deformed" + str(j) + ".vtu"

            filename = os.path.join(self.output_folder, deformed_name)
            uGrid = self.deformedStates[idx]
            writer = vtkXMLUnstructuredGridWriter()
            writer.SetFileName(filename)
            writer.SetInputData(uGrid)
            writer.Update()

            print("Exported to", filename)


def appendSpringsForExport(data, exportArray):
    # Get fields of interest
    startIDs = data["startIDs"]
    stiffness = data["stiffness"]
    # Associate topology node ind with its corresponding spring stiffness
    for i, ind in enumerate(startIDs):
        exportArray[ind] = stiffness[i]
    return exportArray


def normalizeSpringsForExport(exportArray, valueForFixedBC=1000, maxStiffness=1.0, minStiffness=0.0):
    """
    Normalizes values of the boundary conditions between 0 and 1.
    Fixed points, which are associated with valueForFixedBC in exportArray, are assigned to value 1.
    All the other values in the array are rescaled between 0 and 1, based on the extreme values that they could
    had at data generation time [minStiffness-maxStiffness]
    """
    indicesFixed = np.nonzero(exportArray == valueForFixedBC)

    normalized_array = (exportArray - minStiffness) / (maxStiffness - minStiffness)
    normalized_array[indicesFixed] = 1.0
    return normalized_array


def result2vtk(curPositions, curBoundaries, triangle_topology):
    # Convert the current positions to VTK:
    points = vtkPoints()
    for i, p in enumerate(curPositions):
        points.InsertNextPoint(curPositions[i])

    # Convert triangles to VTK
    cells = vtkCellArray()
    cellTypes = []

    for i, t in enumerate(triangle_topology.triangles.value):
        cells.InsertNextCell(3, t)
        cellTypes.append(VTK_TRIANGLE)

    # Create vtk unstructured grid from the data:
    ugrid = vtkUnstructuredGrid()
    ugrid.SetPoints(points)
    ugrid.SetCells(cellTypes, cells)

    # Needed for evaluation error computation - actually not, because it's relative to all points
    # Compute and add the displacement field:
    # displacement = vtkDoubleArray()
    # displacement.SetNumberOfComponents(3)
    # displacement.SetNumberOfTuples( len(curPositions) )
    # displacement.SetName("displacement")
    # restPositions = triangle_topology.position.value
    # for i, p in enumerate(curPositions):
    #     pRest = restPositions[i]
    #     displ = (p[0]-pRest[0], p[1]-pRest[1], p[2]-pRest[2])
    #     displacement.SetTuple3( i, displ[0], displ[1], displ[2] )
    # ugrid.GetPointData().AddArray( displacement )

    # Add stiffness values
    stiffness = vtkDoubleArray()
    stiffness.SetNumberOfComponents(1)
    stiffness.SetName("stiffness")
    for b in curBoundaries:
        stiffness.InsertNextTuple1(b)

    ugrid.GetPointData().AddArray(stiffness)

    return ugrid


## ------------------------------------------------------------------- ##
##                         SCENE CREATION                              ##
## ------------------------------------------------------------------- ##
def createScene(root):
    # Required plugins
    SofaRuntime.PluginRepository.addFirstPath(os.environ['CARIBOU_INSTALL'])
    required_plugins = ['SofaComponentAll', 'SofaLoader', 'SofaBaseTopology', 'SofaGeneralEngine',
                        'SofaEngine', 'SofaOpenglVisual', 'SofaBoundaryCondition', 'SofaPython3',
                        'SofaGeneralSimpleFem', 'SofaImplicitOdeSolver', 'SofaCaribou']
    root.addObject('RequiredPlugin', pluginName=required_plugins)

    # Animation loop
    root.addObject('DefaultVisualManagerLoop')
    root.addObject('DefaultAnimationLoop')

    # Load YAML configuration file
    options = utils.load_yaml()

    # Create simulation
    root.addObject(Organ(root, options))

    return root


if __name__ == '__main__':

    # Create scene graph
    root = Sofa.Core.Node()
    root.dt = 0.01
    root.gravity = [0.0, 0.0, 0.0]
    root = createScene(root)

    # Launch Sofa GUI
    Sofa.Simulation.init(root)

    try:
        if sys.argv[2] == "--gui":
            Sofa.Gui.GUIManager.Init("myscene", "qglviewer")
            Sofa.Gui.GUIManager.createGUI(root, __file__)
            Sofa.Gui.GUIManager.SetDimension(1080, 1080)
            # Initialization of the scene will be done here
            Sofa.Gui.GUIManager.MainLoop(root)
            Sofa.Gui.GUIManager.closeGUI()
    except:
        for iter in range(100):
            Sofa.Simulation.animate(root, root.dt.value)
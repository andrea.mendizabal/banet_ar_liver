class Solver:
    STATIC = 'Static'
    DYNAMIC = 'Dynamic'

    CG = 'CG'
    PCG = 'PCG'
    PARDISO = 'Pardiso'
    SOFASPARSE = 'SofaSparse'
    
    def __init__(self, parent_node, analysis=DYNAMIC, solver='CG', solver_name='Solver', **kwargs):
        assert analysis in [self.STATIC, self.DYNAMIC]
        self.analysis = analysis
        assert solver in [self.CG, self.PCG, self.PARDISO, self.SOFASPARSE]
        solver_type = solver

        # Analysis
        if self.analysis == self.DYNAMIC:
            kd = kwargs.get('kd', None)
            ks = kwargs.get('ks', None)
            self.odesolver = parent_node.addObject('EulerImplicitSolver')
            if kd is not None:
                self.odesolver.rayleighMass = kd
            if ks is not None:
                self.odesolver.rayleighStiffness = ks

        elif self.analysis == self.STATIC:
            self.niter          = kwargs.get('newton_iterations',20)
            self.correction_thr = kwargs.get('correction_thr',1e-7)
            self.tolerance_thr  = kwargs.get('tolerance_thr',1e-7)
            self.print_log      = kwargs.get('print_log',False)
            self.StaticODESolver = parent_node.addObject('StaticODESolver', name='ODESolverFromCaribou',
                                                         newton_iterations=self.niter, printLog=self.print_log,
                                                         correction_tolerance_threshold=self.correction_thr,
                                                         residual_tolerance_threshold=self.tolerance_thr)
            # parent_node.addObject('StaticSolver', newton_iterations=self.niter,
            #                       absolute_correction_tolerance_threshold = self.correction_thr,
            #                       absolute_residual_tolerance_threshold = self.tolerance_thr, printLog = self.print_log)


            # Linear solver
        if solver_type == self.CG:
            num_iter = kwargs.get('num_iter', 2000)
            self.solver = parent_node.addObject('ConjugateGradientSolver', name='CGSolverFromCaribou',
                                                maximum_number_of_iterations=num_iter, preconditioning_method='Diagonal',
                                                residual_tolerance_threshold=1e-15, printLog=False)
            # self.solver = parent_node.addObject('CGLinearSolver', name=solver_name, iterations=num_iter,
            #                                     threshold="1e-15")


        elif solver_type == self.PCG:
            print('PCG not yet ready')
            self.solver = parent_node.addObject('CGLinearSolver',name=solver_name,iterations='100')

        elif solver_type == self.PARDISO:
            try:
                parent_node.addObject('RequiredPlugin',name='SofaPardisoSolver',pluginName='SofaPardisoSolver')
                self.solver = parent_node.addObject('SparsePARDISOSolver',name=solver_name)
            except Exception as e:
                print("Exception. To use Pardiso solver you need SofaPardisoSolver plugin, which is private.")
                raise e

        elif solver_type == self.SOFASPARSE: 
            parent_node.addObject('RequiredPlugin',name='SofaSparseSolver',pluginName='SofaSparseSolver')
            self.solver = parent_node.addObject('SparseLDLSolver',name=solver_name)

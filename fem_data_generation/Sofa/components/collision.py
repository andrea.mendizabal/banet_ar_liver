" TO DO "
class CollisionPipeline:
    def __init__(self, parent_node, contact_d=0.003, alarm_d=0.001, **kwargs):
        parent_node.addObject('FreeMotionAnimationLoop')
        parent_node.addObject('GenericConstraintSolver', maxIterations='1000', tolerance='1e-6', printLog='0', allVerified='0')
        parent_node.addObject('CollisionPipeline',depth='6',verbose='0',draw='0')
        parent_node.addObject('BruteForceDetection')
        parent_node.addObject('LocalMinDistance',name='localmindistance',alarmDistance=alarm_d, contactDistance=contact_d, angleCone='90.0', filterIntersection='0')
        parent_node.addObject('DefaultContactManager', name="Response", response="FrictionContact", responseParams='mu=0')

    def create_constraint_correction(self, parent_node, solver_name):
        parent_node.addObject('LinearSolverConstraintCorrection', solverName=solver_name)    

class CollisionObject:
    def __init__(self, parent_node, surface_loader_link, name='Collision', **kwargs):
        parent_node = breastNode.addChild(name)            
        parent_node.addObject('Mesh', src=surface_loader_link, name='surface')
        parent_node.addObject('MechanicalObject', src=surface_loader_link)        
        parent_node.addObject('BarycentricMapping')
        parent_node.addObject('Triangle', color='1 0 1 1')
        parent_node.addObject('Line', color='1 0 1 1')
        parent_node.addObject('Point', color='1 0 1 1')
import os, math, sys
import numpy as np

class ROI:
    SPHERE = 'Sphere'
    AABB = 'AABB'
    ORIENTEDBOX = 'OrientedBox'
    
    def __init__(self, parent_node,shape='Sphere',node_name=None,num_roi=1,view='1',**kwargs):
        assert shape in [self.SPHERE, self.AABB, self.ORIENTEDBOX]
        if node_name is None:
            node_name=shape+'ROI'

        if shape == 'Sphere':
            centers = kwargs.get('centers')
            radii = kwargs.get('radii')
            if not centers:
                centers = [0,0,0]
            if not radii:
                radii = 0.1

            roi = parent_node.addObject(
                'SphereROI',
                name=node_name,
                centers=centers,
                radii=radii,
                listening='1',
                drawPoints=view,
            )
            print('drawSphere has a bug, it draws half of the real sphere')

        if shape == 'AABB':
            box = kwargs.get('fixed_box')
            if not box:
                box = [0,0,0]

            roi = parent_node.addObject(
                'BoxROI',
                name=node_name,
                box=box,
                listening='1',
                drawPoints=view
            )

        self.node = roi

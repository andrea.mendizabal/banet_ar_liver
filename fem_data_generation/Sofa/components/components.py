import numpy as np
from  utils import lame

def load_mesh(parent_node,filename,node_name="loader",translation=[0.0,0.0,0.0],eulerRotation=[0.0,0.0,0.0]):
        """
        Use the file extension to create the adequate loader and add it to the scene.
        """
        if len(filename) == 0:
            raise Exception("Unable to instanciate the loader because the filename is empty")

        loader = None
        if filename.endswith(".msh"):
            return parent_node.addObject('MeshGmshLoader', name=node_name, filename=filename, rotation=eulerRotation, translation=translation)
        elif filename.endswith(".stl"):
            return parent_node.addObject('MeshSTLLoader', name=node_name, filename=filename, rotation=eulerRotation, translation=translation)
        elif filename.endswith(".obj"):
            return parent_node.addObject('MeshObjLoader', name=node_name, filename=filename, rotation=eulerRotation, translation=translation)
        elif filename.endswith(".vtu") or filename.endswith(".vtk"):
            return parent_node.addObject('MeshVTKLoader', name=node_name, filename=filename, rotation=eulerRotation, translation=translation)

        raise Exception("Unable to instanciate the loader because the file extension is not supported")

def create_tetrahedral_topology(parent_node,volume_mesh='loader'):
    container = parent_node.addObject(
        'TetrahedronSetTopologyContainer',
        name='Topology',
        src='@'+volume_mesh)
    parent_node.addObject(
        'TetrahedronSetTopologyModifier',
        name='TopoModi')
    parent_node.addObject(
        'TetrahedronSetGeometryAlgorithms',
        name='GeoAlgo',
        template='Vec3d')

    return container

def create_hexahedral_topology(parent_node,volume_mesh='loader'):
    """ Create Hexahedral topology from volume mesh and extracts triangles from quads"""
    hexa_container = parent_node.addObject(
        'HexahedronSetTopologyContainer',
        name='HexaTopologyContainer',
        src='@'+volume_mesh)
    parent_node.addObject(
        'HexahedronSetTopologyModifier',
        name='TopoModi')
    parent_node.addObject(
        'HexahedronSetGeometryAlgorithms',
        name='GeoAlgo',
        template='Vec3d')

    return hexa_container

def create_triangle_topology(parent_node, mesh_link='@./'):
    triangle_container = parent_node.addObject(
        'TriangleSetTopologyContainer',
        name='TriangleTopologyContainer',
        src=mesh_link)
    parent_node.addObject(
        'TriangleSetTopologyModifier',
        name='TopoModi')
    parent_node.addObject(
        'TriangleSetGeometryAlgorithms',
        name='GeoAlgo',
        template='Vec3d') 

    return triangle_container

def create_tetrahedral_FEM(parent_node,material,node_name='tetraFEM'):
    if material.constitutive_model in ('Linear', 'Corotated'):
        method = 'large'
        if material.constitutive_model=='Linear':
            method = 'small'
        ff = parent_node.addObject(
            'TetrahedronFEMForceField',
            name=node_name,
            method=method,
            youngModulus=material.young_modulus,
            poissonRatio=material.poisson_ratio,
        )
    else:
        parameters = lame(material)
        ff = parent_node.addObject(
            'TetrahedronHyperelasticityFEMForceField',
            ParameterSet=parameters, 
            materialName=material.constitutive_model,
            name=node_name
        )

    return ff
import os, math, sys
import numpy as np
import components as comp

class NodalForce:
    
    # def __init__(self, parent_node, node_name='NodalForce', filename=None, **kwargs):
    def __init__(self, force_node=None, **kwargs):

        # Create intermediate surface for correct force application
        # if a surface filename is provided, a child node is created and forces are applied to the MechanicalObject
        # associated to the provided file. Otherwise, forces are applied to the MechanicalObject in parent_node.
        # if not(filename is None):
        #     child_node = parent_node.addChild(node_name)
        #     comp.load_mesh(child_node, filename=filename, node_name='surface_loader')
        #     child_node.addObject('MechanicalObject', src='@surface_loader')
        #     child_node.addObject('TriangleSetTopologyContainer', name="triangleTopo", src="@surface_loader")
        #     child_node.addObject('BarycentricMapping')
        #     parent_node = child_node
        #
        # print("parent_node", parent_node)
        # force = parent_node.addObject(
        #                     'ConstantForceField',
        #                     name=node_name,
        #                     listening='1',
        #                     )
        self.node = force_node
    
    def set_forces(self, force, indices=0, roi_node=None):

        if roi_node:
            num_indices = len(roi_node.indices.value)
            indices = roi_node.getLinkPath()+'.indices'
        else:
            num_indices = len(indices)

        nodal_forces = np.zeros((num_indices, 3), dtype=np.float)
        nodal_forces[:] = force
        self.node.forces.value = nodal_forces.tolist()
        self.node.indices.value = indices

    def update_forces(self, increment_force):
        if isinstance(increment_force, list):
            increment_force = np.asarray(increment_force)
        old_forces = np.asarray(self.node.forces.value)
        new_forces = (old_forces + increment_force).tolist()
        self.node.forces.value = new_forces

    

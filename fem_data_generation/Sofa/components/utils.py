import sys, os, errno
import numpy as np
from numpy import linalg as LA
from tempfile import mkstemp
from shutil import move
import yaml
from scipy import spatial

def euclidean_distance(array1, array2):
	""" 
	Computes euclidean distance between input arrays.

	Arguments
	-----------
	array1 : array_like
		Nx3 array with coordinates of N points.
	array2 : array_like
		Nx3 array with coordinates of N points.

	Returns
	----------
	float
		Euclidean distance.
	"""	
	return LA.norm(array1-array2)
	
def check_valid_displacement(displacement, low_thresh=0.0, high_thresh=np.nan):
	""" 
	Computes if the displacement is valid.

	Arguments
	-----------
	displacement : array_like
		Nx3 array with x,y,z displacements of N points.
	low_thresh : float
		value below which the displacement is discarded (defalut 0.0).
	high_thresh : float
		value above which the displacement is discarded (defalut NaN).
	
	Returns
	-----------
	False, if:
		* there is at least one displacement with NaN value
		* there is at least one displacement whose norm is >= high_thresh
		* there is at least one displacement whose norm is <= low_thresh 
	otherwise, returns True.
	"""
	displ_norm = LA.norm(displacement, axis=1)
	max_displ_norm = np.amax(displ_norm)

	if np.isnan(high_thresh): 
		if np.isnan(max_displ_norm):
			print('Simulation is unstable')
			return False
	elif max_displ_norm >= high_thresh:
		print('Max displacement is too big')
		return False

	if max_displ_norm <= low_thresh:
		print('Max displacement is too small')
		return False

	return True

def get_mean_displacement(array1,array2):
	""" 
	Gets the average displacement between two arrays.

	Arguments
	-----------
	array1 : array_like
		Nx3 array with coordinates of N points.
	array2 : array_like
		Nx3 array with coordinates of N points.

	Returns
	-----------
	float
		Average displacement.
	"""
	assert len(array2)==len(array1)
	displacement = array2 - array1
	displ_norm = LA.norm(displacement, axis=1)
	return np.mean(displ_norm)

def get_bbox(position):
	""" 
	Gets the bounding box of the object defined by the given vertices.

	Arguments
	-----------
	position : list
		List with the coordinates of N points (position field of Sofa MechanicalObject).

	Returns
	----------
	xmin, xmax, ymin, ymax, zmin, zmax : floats
		min and max coordinates of the object bounding box.
	"""
	points_array = np.asarray(position.value)
	m = np.min(points_array, axis=0)
	xmin, ymin, zmin = m[0], m[1], m[2]

	m = np.max(points_array, axis=0)
	xmax, ymax, zmax = m[0], m[1], m[2]

	return xmin, xmax, ymin, ymax, zmin, zmax

def get_distance_np( array1, array2 ):
	"""
	For each point in array2, returns the Euclidean distance with its closest point in array1 and
	the index of such closest point. 
	
	Arguments
	----------
	array1 (ndarray):
		N1 x M array with the points.
	array2 (ndarray):
		N2 x M array with the points.
	
	Returns
	----------
	dist:
		N2 x 1 Euclidean distance of each point of array2 with the closest point in array1.
		dist contains the same result as: 	np.nanmin( distance.cdist( array1, array2 ), axis=1 )
	indexes:
		N2 x 1 Indices of closest point in array1

	"""
	mytree = spatial.cKDTree(array1)
	dist, indexes = mytree.query(array2)
	return dist, indexes

def parse_file_paths( data, configFilePath ):
    """ 
    Makes all paths in options which start with "%BASEDIR" relative to yaml file.
    """
    # Cool function adapted from blhsing from:
    # https://stackoverflow.com/questions/55713877/recursively-iterate-through-a-nested-dict-with-list-and-replace-matched-values
    if isinstance(data, (dict, list)):
        for k, v in (data.items() if isinstance(data, dict) else enumerate(data)):
            if type(v) == str:
                data[k] = v.replace( "%BASEDIR", configFilePath)
            parse_file_paths(v, configFilePath)

def load_yaml():
	""" 
	Loads the YAML configuration file passed as command line argument when running Sofa.
	
	Returns
	-----------
	dict 
		containing the loaded data.
	"""
	try :
			sys.argv[0]
	except :
			commandLineArguments = []
	else :
			commandLineArguments = sys.argv

	if len(commandLineArguments) > 1:
			configFileName = commandLineArguments[1]
	else:
			print('ERROR: Must supply a yaml config file as an argument!')
	configFilePath = os.path.dirname( configFileName )
	with open(configFileName, 'r') as stream:
			try:
					options = yaml.load(stream)
					parse_file_paths( options, configFilePath )
					return options     

			except yaml.YAMLError as exc:
					print(exc)
					return

def replace_line(file_path, pattern, subst):
	"""
	Replaces the line containing 'pattern' in file 'file_path' with the new line 'subst'.

	Arguments
	-----------
	file_path : str
		name of the file where a line has to be changed.
	pattern : str
		line that has to be substituted in the opened file.
	subst : str
		new line that has to replace 'pattern' in the opened file.

	"""
	#Create temp file
	fh, abs_path = mkstemp()
	with fdopen(fh,'w') as new_file:
			with open(file_path) as old_file:
					for line in old_file:
							new_file.write(line.replace(pattern, subst))
	#Remove original file
	remove(file_path)
	#Move new file
	move(abs_path, file_path)

def read_param_from_file(filename,param_name):
	"""
	Reads the value of a parameter in the input file filename. 
	
	The function assumes that a single value has to be read, which is written in the file with the following format:
	paramName = paramValue
	For example: readParamFromFile('simulation.txt','poissonRatio') 
	will read from the file 'simulation.txt' placed in the same folder as the calling py scene the parameter 'poissonRatio' and
	return its value.

	Arguments
	-----------
	filename : str
		name of the file where to read a value.
	param_name : str
		name of the parameter to read.

	Returns
	-----------
	float
		of the read value. 
	"""
	with open(filename,'r') as f:
		for line in f:
			if line.split()[0] == param_name:
				value = line.split()[2]
				return value

def mkdir(path):
	try:
		os.makedirs(path)
	except OSError as exc:  # Python >2.5
		if exc.errno == errno.EEXIST and os.path.isdir(path):
			pass
		else:
			raise

def lame(material):
	young_modulus = material.young_modulus
	poisson_ratio = material.poisson_ratio
	if material.constitutive_model == 'StVenantKirchhoff':
		mu = young_modulus / (2. * (1. + poisson_ratio))
		l = young_modulus * poisson_ratio / ((1. + poisson_ratio) * (1. - 2.*poisson_ratio))
		parameters = [mu,l]
	elif material.constitutive_model == 'NeoHookean':
		mu = young_modulus / (2. * (1. + poisson_ratio))
		K = young_modulus /(3 * (1. - 2.*poisson_ratio))
		parameters = [mu,K]
	return parameters


def from_lame(mu, l):
	poisson_ratio = 1. / (2 * ((mu / l) + 1))
	young_modulus = 2*mu*(1 + poisson_ratio)
	return poisson_ratio, young_modulus



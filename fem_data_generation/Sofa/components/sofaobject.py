import os, math, sys
import numpy as np
import time

#from mesh import Mesh, SurfacePart
import components as comp
from utils import get_bbox

class Material:
    LINEAR = 'Linear'
    COROTATED = 'Corotated'
    STVENANTKIRCHHOFF = 'StVenantKirchhoff'
    NEOHOOKEAN = 'NeoHookean'

    def __init__(self, constitutive_model=LINEAR, poisson_ratio=0.3, young_modulus=4000, mass_density=2.5):
        assert constitutive_model in [self.LINEAR, self.COROTATED, self.STVENANTKIRCHHOFF, self.NEOHOOKEAN]
        self.constitutive_model = constitutive_model
        self.poisson_ratio = poisson_ratio
        self.young_modulus = young_modulus
        self.mass_density = mass_density


class SofaObject:
    """
    Object defined by either a tetrahedral mesh or by a grid.

    Attributes:
        behavior_state (Sofa MechanicalObject): 
            Mechanical object containing the solution of the DOFs (i.e., the object associated with a FEM model)
            It corresponds to the tetra object if present, otherwise the grid object.
        surface_state (Sofa MechanicalObject):   
            Mechanical object containing the positions of the surface mesh.
        grid_state (Sofa MechanicalObject):   
            Mechanical object containing the position of the grid object.
        bbox (dict):
            Min and max coordinates of the bounding box of the main mechanical object.

    """
    def __init__(self, **kwargs):
        """ 
        Keyword arguments:
            parent_node (Sofa Node):
                parent node to which the object is attached.
            material (Material): 
                Material used for the elastic force field.
            verbose (bool): 
                Whether or not to display log messages (default False).
            surface_mesh (str): 
                Path to the surface mesh of the simulated object (default None).
            tetra_mesh (str):
                Path to the tetrahedral mesh of the simulated object (default None).
            sparse_grid (bool): 
                Whether or not to create a sparse grid (default False).
            regular_grid (bool): 
                Whether or not to create a regular grid (default False).
            cell_size (float): 
                Size of the grid's cells defined as a % of the object's minimum size (default 0.1).
            name (str):  
                Name of the node where the object is created (default 'SofaObject').
        """
        self.parent_node = kwargs.get('parent_node')
        self.verbose = kwargs.get('verbose', False)
        self.surface_mesh = kwargs.get('surface_mesh', None)
        self.volume_mesh = kwargs.get('tetra_mesh', None)
        self.create_sparse_grid = kwargs.get('sparse_grid', False)
        self.create_regular_grid = kwargs.get('regular_grid', False)
        self.cell_size = kwargs.get('cell_size', 0.1)
        self.material = kwargs.get('material', Material())
        self.node_name = kwargs.get('name', 'SofaObject')

        # Check on the type of simulation
        if self.volume_mesh: 
            print('Simulation will use TetraFEM.')
        else:
            if self.create_sparse_grid or self.create_regular_grid:
                print('Simulation will use HexaFEM created from grid.')
                if self.surface_mesh is None:
                    raise AttributeError('Simulation requires a valid surface mesh or volume mesh to embed into grid.')

        # Check on the material
        assert isinstance(self.material, Material)

        # SOFA Objects
        # ===== !!!! REFERENCES !!!!!!! ===== #
        self.behavior_state = None  # Mechanical object containing the solution of the DOFs (= tetra object if present, otherwise the grid object)
        self.surface_state = None  # Mechanical object containing the interpolated positions of the surface mesh
        self.bbox = None

        # creates the graph
        self.create_sofa_graph()


    def create_sofa_graph(self):
        # ===== MECHANICAL NODE ===== #
        mechanical_node = self.parent_node.addChild(self.node_name)
        self.node = mechanical_node

        # Surface mesh
        if self.surface_mesh:
            surface_mesh_name = 'surface_mesh'
            surface_mesh_loader = comp.load_mesh(mechanical_node,filename=self.surface_mesh,node_name=surface_mesh_name)
            self.surface_mesh_loader = surface_mesh_loader
            xmin, xmax, ymin, ymax, zmin, zmax = get_bbox(surface_mesh_loader.position)
            self.bbox = {
                'min': [xmin, ymin, zmin],
                'max': [xmax, ymax, zmax]
            }

        # Tetrahedral mesh
        if self.volume_mesh:
            volume_mesh_name = 'volume_mesh'
            comp.load_mesh(mechanical_node,filename=self.volume_mesh,node_name=volume_mesh_name)
            self.tetra_mesh_topology = comp.create_tetrahedral_topology(mechanical_node,volume_mesh=volume_mesh_name)
            self.behavior_state = mechanical_node.addObject('MechanicalObject', src='@Topology', name='behavior_state', showObject=True, showObjectScale='2')
            self.ff = comp.create_tetrahedral_FEM(mechanical_node,self.material)

            # If grid is required in addition to tetra mesh, make it a child of volume mesh
            if self.create_sparse_grid or self.create_regular_grid:
                grid_parent_node = mechanical_node.addChild('Grid')
            else:
                grid_parent_node = None

            if self.bbox is None:
                xmin, xmax, ymin, ymax, zmin, zmax = get_bbox(self.tetra_mesh_topology.position)
                self.bbox = {
                    'min': [xmin, ymin, zmin],
                    'max': [xmax, ymax, zmax]
                }
        
        # If grid is the main mechanical object
        else:
            grid_parent_node = mechanical_node


        # Bounding box
        self.sx = math.fabs(self.bbox['max'][0] - self.bbox['min'][0])  # Size in the x-axis
        self.sy = math.fabs(self.bbox['max'][1] - self.bbox['min'][1])  # Size in the y-axis
        self.sz = math.fabs(self.bbox['max'][2] - self.bbox['min'][2])  # Size in the z-axis
        s = np.array([self.sx, self.sy, self.sz])
        center = np.array(self.bbox['min']) + s/2.
        self.bbox = {
            'min': (center - s/2.).tolist(),
            'max': (center + s/2.).tolist()
        }
        
        # Initialize root bbox for visualization (I think this only works if parent_node = root)
        self.parent_node.bbox = [[self.bbox['min'][0], self.bbox['min'][1], self.bbox['min'][2]],
                                 [self.bbox['max'][0], self.bbox['max'][1], self.bbox['max'][2]]]
        print("BBOX: ", self.parent_node.bbox.value)

        # Grid
        if grid_parent_node:

            self.cell_size = self.cell_size * min(self.sx, self.sy, self.sz)  # Cell size of the grid is x% of the object's size
            print("Cell size = {}x{}x{}".format(self.cell_size, self.cell_size, self.cell_size))
            self.nx = int(self.sx / self.cell_size)  # Number of cells in the x-axis
            self.ny = int(self.sy / self.cell_size)  # Number of cells in the y-axis
            self.nz = int(self.sz / self.cell_size)  # Number of cells in the z-axis
            print("Nx = {}, Ny = {}, Nz = {}".format(self.nx, self.ny, self.nz))
            self.number_of_nodes = (self.nx+1)*(self.ny+1)*(self.nz+1)
            print("Number of nodes in regular grid = {}".format(self.number_of_nodes))

            grid_dim = [self.nx + 1, self.ny + 1, self.nz + 1]
            if self.surface_mesh:
                mesh_link = surface_mesh_loader.getLinkPath()
            else:
                mesh_link = self.tetra_mesh_topology.getLinkPath()

            if self.create_sparse_grid:
                self.grid = SofaSparseGrid(grid_parent_node, grid_dim, self.bbox, mesh_link=mesh_link)
            elif self.create_regular_grid: 
                self.grid = SofaRegularGrid(grid_parent_node, grid_dim, self.bbox)

            # If a grid was created and represents the main mechanical object        
            if grid_parent_node == mechanical_node:    
                self.behavior_state = self.grid.grid_state
                self.hexa_mesh_topology = comp.create_hexahedral_topology(mechanical_node,
                                                                          volume_mesh=self.grid.topology.name.value)
                # Constitutive law of the beam
                # mechanical_node.addObject('SaintVenantKirchhoffMaterial', young_modulus=self.material.young_modulus,
                #                           poisson_ratio=self.material.poisson_ratio, name="StVK")
                mechanical_node.addObject('NeoHookeanMaterial', young_modulus=self.material.young_modulus,
                                          poisson_ratio=self.material.poisson_ratio, name="StVK")
                mechanical_node.addObject('HyperelasticForcefield', material="@StVK", template="Hexahedron")

                # Map the triangular surface to the created grid
                self.surface_node = mechanical_node.addChild("Surface")
                self.triangle_mesh_topology = comp.create_triangle_topology(self.surface_node, mesh_link=mesh_link)
                
                self.surface_state = self.surface_node.addObject('MechanicalObject', src='@TriangleTopologyContainer', name='surface_state', showObject=True, showObjectScale='2', showColor=[1,0,0,1])
                self.forceNode = self.surface_node.addObject('ConstantForceField', name='NodalForce',
                                                             indices='0', forces=[0.0, 0.0, 0.0], listening='1')
                self.surface_node.addObject('BarycentricMapping', input='@../grid_state', output='@./surface_state')
                # self.surface_node = surface_node
                # triangle_topo_node.addObject(
                #                             'Quad2TriangleTopologicalMapping',
                #                             name='Quad2TriangleTopoMapping',
                #                             input="@../HexaTopologyContainer",
                #                             output="@TriangleTopologyContainer")

                # # Initialize force node
                # forceNode = self.surface_node.addChild('forceNode')
                # self.forceNode = forceNode.addObject('ConstantForceField', name='NodalForce', listening='1')

            else:
                grid_parent_node.addObject('BarycentricMapping')
###################################################################################################
        # # Embedded surface for correct deformed state extraction
        # # (SparseGridTopology component introduces surface elements inside the object and without this extra
        # # embedded surface, the following surface extraction process in run.py fails)
        #
        # embedded_surface = mechanical_node.addChild('embedded_surface')
        #
        # # ## CREATE TETRA MESH
        # # volume_mesh_name = 'volume_mesh'
        # # comp.load_mesh(embedded_surface, filename='/media/andrea/data/post_doc_verona/banet/volume.vtk', node_name=volume_mesh_name)
        # # self.embedded_surface_topology = comp.create_tetrahedral_topology(embedded_surface, volume_mesh=volume_mesh_name)
        # # embedded_surface.addObject('MechanicalObject', src='@Topology', name='behavior_state_bis', showObject=False)
        # # # embedded_surface.addObject('MechanicalObject', src='@Topology', name='behavior_state',
        # # #                                                 showObject=True, showObjectScale='2')
        # # # comp.create_tetrahedral_FEM(embedded_surface, self.material)
        #
        # ## CREATE SURFACE FROM STL FILE
        # comp.load_mesh(embedded_surface, filename='/media/andrea/data/post_doc_verona/banet/surface.stl', node_name='embedded_surface_loader')
        # self.embedded_surface_mo = embedded_surface.addObject('MechanicalObject', src='@embedded_surface_loader')
        # self.embedded_surface_topology = embedded_surface.addObject('TriangleSetTopologyContainer', name="triangleTopo", src="@embedded_surface_loader")
        # embedded_surface.addObject('BarycentricMapping')
        #
        # # visual_node = embedded_surface.addChild('visual_node')
        # # self.visual_state_embedded_surface = visual_node.addObject('OglModel', src=embedded_surface_mesh.getLinkPath())
        # # visual_node.addObject('BarycentricMapping')

###################################################################################################

    def visualize_surface( self ):
        if self.surface_mesh:
            visual_node = self.node.addChild('VisualSurface')
            visual_node.addObject('OglModel', name="VisualSurfaceOGL", src="@../surface_mesh")
            visual_node.addObject('BarycentricMapping', input="@../behavior_state", output="@VisualSurfaceOGL")

class SofaSparseGrid:
    def __init__(self, parent_node, grid_dimension, bbox, **kwargs):
        mesh_link = kwargs.get('mesh_link')
        self.topology = parent_node.addObject(
                    'SparseGridTopology',
                    name='sparsegrid',
                    # min=bbox['min'],
                    # max=bbox['max'],
                    n=grid_dimension,
                    src=mesh_link
                )
        self.grid_state = parent_node.addObject('MechanicalObject', src='@sparsegrid', name='grid_state', showObject=True, showObjectScale='2')    

class SofaRegularGrid:
    def __init__(self, parent_node, grid_dimension, bbox):
        self.topology = parent_node.addObject(
                    'RegularGridTopology',
                    name='regulargrid',
                    min=bbox['min'],
                    max=bbox['max'],
                    n=grid_dimension,
                )
        self.grid_state = parent_node.addObject('MechanicalObject', src='@regulargrid', name='grid_state', showObject=True, showObjectScale='2')
            
    def set_color(self, color=[1, 1, 1, 1]):
        self.grid_state.showColor = color


"""
Simulation of a cube with fixed boundary conditions on the top face.
Every 3 time steps, a new random force is applied to a new subset of cube surface vertices.
"""

import numpy as np
import sys
import os
import time
from datetime import date
import Sofa
from random import uniform, randint

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),"../components"))

import utils
from sofaobject import SofaObject
from sofaobject import Material
from solver import Solver
from boundaries import FixedBoundaries
from roi import ROI
from forces import NodalForce


load_vts_grid = 0

#################
# SOFA functions
#################
def createScene(rootNode):
	rootNode.createObject('RequiredPlugin',name='SofaPython',pluginName='SofaPython')
	
	# Load YAML configuration file
	options = utils.load_yaml()

	# Create simulation
	Cube(rootNode, options)

class Cube(Sofa.PythonScriptController):

	def __init__(self, rootNode, opt):
		super(Cube,self).__init__()

		self.opt = opt

		# Read config parameters
		self.force_max_magnitude = self.opt['simulation']['force_max_magnitude']
		self.force_min_magnitude = self.opt['simulation']['force_min_magnitude']
		self.max_radius_force_roi = self.opt['simulation']['max_radius_force_roi']
		self.max_radius_visible_roi = self.opt['simulation']['max_radius_visible_roi']
		self.max_allowed_displacement = self.opt['simulation']['max_allowed_displacement'] 

		self.createGraph(rootNode)
		return None


	def createGraph(self,rootNode):

		self.rootNode = rootNode

		# CREATE SCENE  
		rootNode.findData('dt').value = self.opt['simulation']['dt']
		rootNode.findData('gravity').value = self.opt['simulation']['gravity']
		rootNode.createObject('VisualStyle', displayFlags='showBehavior showWireframe')
	
		# Mesh
		undeformed_surface_mesh = self.opt['filenames']['surface_mesh']
		undeformed_tetra_mesh = self.opt['filenames']['tetra_mesh']

		# Create output directory
		if self.opt['io']['save_data']:
			self.output_folder = self.opt['filenames']['output_file']+date.today().strftime("%d%m")
			utils.mkdir(self.output_folder)

		# Material
		material = Material(
			young_modulus=self.opt['model']['fem']['E'],
			poisson_ratio=self.opt['model']['fem']['nu'],
			constitutive_model=Material.COROTATED,
			mass_density=self.opt['model']['rho'],
		)

		# Grid
		cube = SofaObject(
				parent_node=rootNode,
				surface_mesh=undeformed_surface_mesh,
				tetra_mesh=undeformed_tetra_mesh,
				cell_size=0.07,
				material=material,
				regular_grid=1,
			)
		self.cube = cube

		solver = Solver(
					parent_node=cube.node,
					analysis=Solver.DYNAMIC,
					solver=Solver.CG					
				)
		
		# Mass required in case of dynamic analysis or in presence of gravity
		if solver.analysis == Solver.DYNAMIC:
			cube.node.createObject('MeshMatrixMass',massDensity=material.mass_density,name='myMatrixMass')

		# Fixed boundaries and forces
		surface_node = cube.node.createChild('SurfaceMechanical')
		surface_topology = surface_node.createObject('MeshTopology',src=self.cube.surface_mesh_loader.getLinkPath())
		self.surface_state = surface_node.createObject('MechanicalObject',src='@../surface_mesh')

		# I want the bc_roi to select only points belonging to the surface. But:
		# 1) if I attach the roi to the tetra mesh and link bc to its indices, also internal points are fixed
		# 2) if I attach the roi to the surface mesh and link bc to its indices, the bc are NOT mapped to the mech obj.
		# Thus, I use option 2) and manually map indices to those of the tetra mesh in bwdInit
		self.bc_roi = ROI(
					surface_node,
					shape='AABB',
					fixed_box=[-0.06,0.05,-0.06,0.06,0.08,0.06],
					node_name='FixedROI',
					view='1'
				)

		self.boundaries = FixedBoundaries(
				parent_node=cube.node,
				roi_node=self.bc_roi.node
			)
		
		self.forces_roi = ROI(
					surface_node,
					shape='Sphere',
					centers=[0.05,0,0.05],
					radii=0.03,
					node_name='ForcesROI'
				)

		self.force = NodalForce(
					parent_node=surface_node	
				)

		self.visible_roi = ROI(
			surface_node,
			shape='Sphere',
			centers=[0.05,0,0.05],
			radii=0.03,
			node_name='VisibleROI'
		)
		# Map surface to tetra mesh
		surface_node.createObject('BarycentricMapping')

		self.max_num_points = len(self.surface_state.position)
		print 'Number of surface points: ', self.max_num_points
		self.max_aabb = np.amax([cube.sx,cube.sy,cube.sz])

	def bwdInitGraph(self,rootNode):
		self.num_iter = 0
		self.save = 0
		self.n_amplitude = 0

		magnitude = [0,0,self.force_max_magnitude/2.0]
		self.force.set_forces(magnitude,roi_node=self.forces_roi.node)


	def onEndAnimationStep(self,dt):
		self.save += 1

		# Save and reset after 3 animation steps
		if self.save == 3:
			self.save = 0

			if self.opt['io']['save_data']:
				# All surface points after deformation
				all_points_def = np.asarray(self.surface_state.position)
				
				# Visible points
				visible_idx = [item for sublist in self.visible_roi.node.indices for item in sublist]
				visible_points_rest = np.asarray(self.surface_state.rest_position)
				visible_points_def = np.asarray(self.surface_state.position)
				visible_points_rest = visible_points_rest[visible_idx]
				visible_points_def = visible_points_def[visible_idx]
				
				# Boundary points
				bc_points = self.bc_roi.node.pointsInROI
				
				mean_displacement = utils.get_mean_displacement(visible_points_def,visible_points_rest)
				if mean_displacement <= self.max_allowed_displacement:
					# Save sample
					output_file = self.output_folder+'/displ'+str(self.num_iter-1) #+self.outfileloss 
					np.savez_compressed(output_file,pts_rest=visible_points_rest,pts_def=visible_points_def,all_pts=all_points_def)
				else:
					# Forget about current sample
					self.num_iter-=1
				
			# Reset and start new simulation
			self.reset()

	def reset(self):
		print 'Sim number ',self.num_iter

		# Reset mechanical object
		self.cube.behavior_state.position = self.cube.behavior_state.rest_position
		self.cube.grid.grid_state.position = self.cube.grid.grid_state.rest_position
		self.surface_state.position = self.surface_state.rest_position

		# check required because reset() is called once automatically at the beginning
		if self.num_iter:

			# Re-init FORCES
			d = utils.random_unit_vector()
			new_force = utils.get_random_forces_around_direction(
					n=1, direction=d, min_force=self.force_min_magnitude, max_force=self.force_max_magnitude)[0]

			# Try max 10 times to find a set of force indices which does not overlap with the one of BC indices
			for i in range(10):
				new_centers = self.surface_state.position[randint(0,self.max_num_points-1)]
				new_radii = np.random.uniform(0.01,self.max_radius_force_roi) #self.max_aabb)#
				self.forces_roi.node.centers = new_centers
				self.forces_roi.node.radii = new_radii

				bc_idx_set = set([elem[0] for elem in self.boundaries.node.indices])
				force_idx_set = set([elem[0] for elem in self.forces_roi.node.indices])
				if not len(bc_idx_set.intersection(force_idx_set)) > 0:
					break

			self.force.set_forces(new_force,roi_node=self.forces_roi.node)
			self.force.node.reinit()
			#self.cube.ff.reinit() #does not update forces

			# Re-init VISIBLE SURFACE
			# same center as force, but get a radius between that of the force roi and the max allowed
			self.visible_roi.node.centers = new_centers	
			new_visible_radii = np.random.uniform(new_radii-0.001,self.max_radius_visible_roi)
			self.visible_roi.node.radii = new_visible_radii

		self.num_iter += 1

	def storeResetState(self):
		print 'storeResetState called (python side)'
		sys.stdout.flush()
		return 0

	def cleanup(self):
		print 'cleanup called'
		
		if self.opt['io']['save_data']:
			print 'Saving variables before exiting'
			# Save something here when sofa is closed
				
		sys.stdout.flush()
		return 0

	def onKeyPressed(self,k):
		print 'onKeyPressed '+k
		if k is 'L':
			self.storeResetState()
			self.reset()

		sys.stdout.flush()
		return 0
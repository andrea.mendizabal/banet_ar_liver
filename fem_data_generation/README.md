# SpringSimulation

This repository contains code to automatically set up and run biomechanical simulations using the Sofa Framework. The focus lies on generating realistic conditions (tissue types, boundary conditions) but random, unrealistic, synthetic structures.

The resulting data is meant to be used as training data for neural networks, allowing them to learn biomechanical behavior.

## Usage

### Generating and simulating random samples

To generate 1000 samples in the Output/RandomData/ directory, run:
```
python3 run.py Output/RandomData/ 1000
```

### Generating and simulating random samples from a given mesh

If you want to use the same, predefined organ mesh for each simulation, run:
```
python3 run.py Output/LiverData/ 1000 --mesh liver.stl
```
where liver.stl is the surface of your organ. Make sure the organ is scaled to meters (not millimeters)!

### Parameter setup

The default parameters can be found in Config/base\_input\_parameters.yml.
To set up your own simulation parameters, copy the file, edit the values and then pass the
path to the file to the run.py script:

```
python3 run.py Output/MyData/ 1000 --config Config/my_input_parameters.yml
```

Please refer to the comments in Config/base\_input\_parameters.yml to find out what each value does.

### Misc

To find out about further command line options, run:
```
python3 run.py --help
```
When you only want to re-run a part of the pipeline (but skip all steps leading up to this step, please see the "Pipeline Jumping" arguments.

## Repository overview

This repository is organized in folders. Each folder contains the scripts relative to the corresponding phase of the pipeline.
The run.py script found in the main folder goes through all the steps of the pipeline, by calling the related functions. 
However, each folder can also be used on its own.

### Pipeline folders:

1. SurfaceGeneration: Script(s) to generate random surface meshes in Blender.
2. Meshing: Script(s) to generate tetrahedral volume meshes from triangle surface meshes.
3. BoundarySetup: Scripts(s) which analyse the surface and volume meshes and generate boundary conditions. These boundary conditions are meant to simmulate surrounding tissue (other organs, fat) and connecting ligament, both of which are simulated by springs.
4. Sofa: Scripts(s) to set up and run the Sofa Scene and the FEM Solver.
5. SurfaceExtraction: Scripts(s) to extract (parts of) the surface of the intraoperative state.
6. Voxelization: Scripts(s) to interpolate the results into a regular grid and calculate distance fields on the grid.
7. Statistics: Scripts(s) to calculate statistics and plot an overview (mean diplacemnt per file, visible surface amount per file...) of the generated data.

Misc folders:

- Config: Configurations which serve as basis for all other simulations. Some values are copied and others may be overwritten during the process of generating a simulation
- Data: Example meshes
- Utils: Functions which are common to multiple scripts.

## Dependencies

To use the entire pipeline, several dependencies must be installed and added to your $PATH:
- [SOFA Framework](https://www.sofa-framework.org/) Version 19.12.
	- SOFA must be built with python support. Install python-dev (apt install python-dev) and enable PLUGIN_SOFAPYTHON when configuring SOFA with cmake.
	- Note: SOFA must be installed with the Pardiso plugin. Download the [pardiso lib](pardiso-project.org) (tested with Version 7.2). Place the license key into your home directory as "pardiso.lic". Enable PLUGIN_SOFAPARDISOSOLVER when configuring SOFA with cmake. Then set the cmake variable PADISO_LIB to point to the library, for example /home/username/Software/Pardiso/libpardiso720-GNU831-X86-64.so
- [Blender 3D](https://www.blender.org) Version 2.82
- [VTK](https://vtk.org) Version 8.2.0
- [Gmsh](https://gmsh.info) Version 4.5.6
- Python, Version 3.5. Note that additionally, python 2.7 is required by SOFA's python plugin.

The project has been tested on Ubuntu 18.04 and 20.04.

### Python modules

- numpy
- pyYAML
- time
- vtk
- matplotlib
- flatten-dict

Blender might also complain about some missing third party python modules (such as yaml and vtk). To fix this issue, in a shell:
```
cd /path/to/blender/python/bin
./python -m ensurepip
./python -m pip install pyYAML vtk
```

Also, some of these may need to be installed for the system python2 as well (such as yaml and vtk). Try:

```
python2 -m pip install pyYAML vtk
```

## Output Visualization

To visualize the results, we recommend using [Paraview](https://www.paraview.org/).
For convenience, there are several state files included in Utils/Visualize (requires at least Paraview version 5.4). After running the run.py script, open Paraview and choose "File: Load State". Load one of the .psvm files in the Utils/Visualize folder. In the dialogue that appears, choose "Search files under specific directory", then choose the folder of the sample which you want to visualize (for example /path/to/your/Output/Random/00010).

Note:
- To visualize the ligaments/springs, make sure that save\_debug\_data is set to true in the config yaml file when you pass it to run.py.
- If the number of ligaments in a folder is _below_ 10, you may get warnings that the other files can't be found.
- If the number of ligaments in a folder is _above_ 10, only the first 10 ligaments will be visualized.




import sys
import random
import numpy as np
from vtk import *
import os

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),"../Utils"))
from generalutils import *
from vtkutils import *

def selectSurfacePath( surface, startNodeID=None, direction=None, pathLength=0.1 ):
    """
    Select connected nodes along a surface. When selecting the next node, this function
    always looks at previously selected nodes and tries to select the new node along the
    same direction. This makes the path face roughly into a single direction.

    Parameters
    ----------
    surface (vtkPointSet):
        The mesh topology
    startNodeID (int):
        The ID of the node for which neighbors are to be found.
        If None, a random node will be selected. Only nodes where the normal faces upwards
        will be considered in this case.
    direction (tuple of 3 floats):
        Direction in which the path should (roughly) go, starting at the node startNodeID.
        If None, a random direction will be selected.
    pathLength (float):
        The algorithm will stop selecting more nodes as soon as the path is longer than
        pathLength. This means that it's possible that pathLength is exceeded.

    Returns
    ----------
    list of int:
        IDs of the vertices that form the path

    """

    # Generate normals if necessary:
    surface = generatePointNormals( surface )
    normals = surface.GetPointData().GetArray("Normals")
    # If no start node ID is given, select a random one:
    while startNodeID is None:
        startNodeID = int(random.random()*surface.GetNumberOfPoints())
        # Only select start point where the normal faces upwards (positive Y)
        if normals.GetTuple3( startNodeID )[1] < 0.3:
            startNodeID = None

    
    if direction is None: 
        # Select a random direction:
        direction = np.asarray( [random.random()-0.5, random.random()-0.5, random.random()-0.5] )
        norm = np.linalg.norm( direction )
        if norm == 0:
            direction = np.asarray( [1,0,0] )
        else:
            direction = direction/norm

    pathNodes = [startNodeID]
    curPoint = np.asarray( surface.GetPoint( startNodeID ) )
    curNodeID = startNodeID
    curPathLength = 0
    while curPathLength < pathLength:
        # Select next node:
        connectedVerts = getConnectedVertices( surface, curNodeID )
        connectedVerts = [v for v in connectedVerts if not v in pathNodes]
        if len(connectedVerts) == 0:
            break
        
        # Project current direction forward, select point closest to this projection:
        projection = curPoint + direction
        dist = np.inf
        nextNodeID = -1
        for i in range( len(connectedVerts) ):
            p = np.asarray( surface.GetPoint( connectedVerts[i] ) )
            curDist = np.linalg.norm( p - projection )
            if curDist < dist:
                dist = curDist
                nextNodeID = connectedVerts[i]

        assert nextNodeID != -1, "Could not find suitable next point for path."

        #nextNodeID = connectedVerts[ int(random.random()*len(connectedVerts)) ]
        nextPoint = np.asarray( surface.GetPoint( nextNodeID ) )
        direction = 0.9*direction + 0.1*(nextPoint - curPoint)
        curPathLength += np.linalg.norm( nextPoint - curPoint )
        pathNodes.append( curNodeID )

        curNodeID = nextNodeID
        curPoint = nextPoint
        #print(connectedVerts)

    return pathNodes

def extrudeLigament( surface, surfacePoints, center, radius, constStiffness=30 ):
    """
    Given a surface and a path or region on that surface (which describes the nodes where a
    a ligament connects to), tries to find end-points for these ligaments by projecting the
    points outwards, away from the surface.
    This is meant to mimick the fact that ligament is connected to the surface (i.e. the
    organ) as well as the abdomnial wall, where the latter is being inflated (thus the
    end points of the ligament are projected away from the organ).
    Note: This function is subject to change.

    Arguments:
    ----------
    surface (vtkPointSet, for example vtkPolyData):
        Describes the organ surface
    surfacePoints (list of points):
        List of ooints on surface describing which start points of the ligament.
        Usually, these will correspond to vertices on the surface (but they don't have to).
    constStiffness (float):
        Stiffness used for all springs in this ligament

    Returns:
    ----------
    dict
        A description of the ligament. The dict holds the following entries:
            - start: The same as the surfacePoints given to the function
            - end: The extruded surface points, i.e. the end points of the ligament springs
            - lengths: The resting lengths for each spring. Usually shorter than the 
                distance between "start" and "end" points.
            - stiffness: The stiffness for each spring
    """

    extruded = []
    lengths = []
    stiffness = []

    for i in range( len(surfacePoints) ):
        basePoint = surfacePoints[i]
        diff = basePoint - center
        dist = np.linalg.norm( diff )
        baseDirection = diff/dist

        extrudedPoint = center + baseDirection*radius

        length = np.linalg.norm( basePoint - extrudedPoint )*0.5

        extruded.append( extrudedPoint )
        lengths.append( length )
        stiffness.append( constStiffness )

    ligament = {"start":surfacePoints, "end":extruded,
        "length":lengths, "stiffness":stiffness }
    return ligament

def randomSurfaceConnections( surface, numLigaments, lengthMin=0.1, lengthMax=0.1 ):
    """
    Find random paths on the surface and return them.
    """
    paths = []
    for i in range( numLigaments ):
        rndLength = random.uniform( lengthMin, lengthMax )
        pathNodes = selectSurfacePath( surface, pathLength=rndLength )
        path = []
        for j in range(len(pathNodes)):
            point = surface.GetPoint( pathNodes[j] )
            path.append( np.asarray(point) )
        paths.append( path )
    return paths

def predefinedSurfaceConnections( vtkPoints ):
    """
    Converts points given in vtk format to 

    Arguments:
    ----------
    vtkPoints (vtkPointSet, for example a vtkPolyData)

    Returns:
    ----------
    points: (list of np.array):
        list of 3D points, each in the form of an np.array.
    """
    points = []
    for j in range(vtkPoints.GetNumberOfPoints()):
        points.append( np.asarray(vtkPoints.GetPoint( j )) )
    return [points]

def pruneLigament( volume, surface, ligament ):
    """
    Given a ligament and a volume, connect the ligament to the volume.

    First, for each start point in the ligament, the closest node on the volume is found.
    This node will act as the start point.
    If multiple start points have the same closest node in the volume, only one of them
    is kept, the other springs are discarded, thus pruning the ligament.
    If an end point lies within the volume, this spring is removed.
    Of the connections which are kept, the stiffness, starting lengths and end points are
    copied to the final spring descriptions, which are returned by the function.
    Note that this can influence the overall stiffness of the ligament, as the stiffness
    values are not modified, but the number of springs is reduced.

    Arguments:
    ------------
    Volume (vtkPointSet):
        An organ volume to which the ligament should be connected.
    ligament (dict):
        A dictionary with the following lists (all of which have the same length):
            - "start": Start points of the ligament (list of tuples of 3 floats)
            - "end": End points of the ligament (list of tuples of 3 floats)
            - "length": List of resting-length values for each connection (list of float)
            - "stiffness": List of stiffness values for each connection (list of float)

    Returns:
    ------------
    dict
        A dictionary describing the springs connected to the volume.
        Just as the input ligament, the dictionary has "end", "length" and "stiffness"
        lists (which are a subset of the corresponding lists in the input ligament).
        However, the "start" point list has been replaced by a list of ints called
        "startIDs" which holds the IDs of the nodes in the volume which the springs are
        connected to.
        Generally, the lists in this dict will be shorter (pruned) than the lists in the
        input ligament dict.
            - "startIDs": Node IDs of the volume which the springs are connected to
                (list of int)
            - "end": End points of the ligament (list of tuples of 3 floats)
            - "length": List of resting-length values for each connection (list of float)
            - "stiffness": List of stiffness values for each connection (list of float)
    """

    locator = vtkPointLocator( )
    locator.SetDataSet( volume )
    locator.SetNumberOfPointsPerBucket(1)
    locator.BuildLocator()

    startPoints = ligament["start"]
    endPoints = ligament["end"]
    lengths = ligament["length"]
    stiffness = ligament["stiffness"]

    sparseStartNodes = []
    sparseStartPoints = []
    sparseEndPoints = []
    sparseLengths = []
    sparseStiffness = []

    sparseEndPointsPD = vtkPolyData()
    sparseEndPointsVTK = vtkPoints()
    sparseEndPointsPD.SetPoints( sparseEndPointsVTK )

    # Go through the ligament points in random order. Since the ligament is usually denser
    # than the resulting pruned ligament, this avoids always selecting the first point
    # which would connect to a volume node and thus - hopefully - avoids introducing a
    # directional bias.
    rrange = list(range(len(startPoints)))
    random.shuffle(rrange)
    for j in rrange:
        s = startPoints[j]
        closestMeshPointID = locator.FindClosestPoint( s )
        if not closestMeshPointID in sparseStartNodes:
            sparseStartNodes.append( closestMeshPointID )
            sparseStartPoints.append( volume.GetPoint(closestMeshPointID) )
            sparseEndPoints.append( endPoints[j] )
            sparseLengths.append( lengths[j] )
            sparseStiffness.append( stiffness[j] )

            sparseEndPointsVTK.InsertNextPoint( endPoints[j] )

    # Go through the end points again and check if they are inside the springs. If so,
    # remove the corresponding indices.
    enclosedPointSelector = vtkSelectEnclosedPoints()
    enclosedPointSelector.CheckSurfaceOn()
    enclosedPointSelector.SetInputData( sparseEndPointsPD )
    enclosedPointSelector.SetSurfaceData( surface )
    enclosedPointSelector.SetTolerance( 1e-9 )
    enclosedPointSelector.Update()
    enclosedPoints = enclosedPointSelector.GetOutput()
    toRemove = []
    for j in range(0, sparseEndPointsPD.GetNumberOfPoints() ):
        if enclosedPointSelector.IsInside(j):
            toRemove.append( j )
    sparseStartNodes = [v for i,v in enumerate(sparseStartNodes) if not i in toRemove]
    sparseStartPoints = [v for i,v in enumerate(sparseStartPoints) if not i in toRemove]
    sparseEndPoints = [v for i,v in enumerate(sparseEndPoints) if not i in toRemove]
    sparseLengths = [v for i,v in enumerate(sparseLengths) if not i in toRemove]
    sparseStiffness = [v for i,v in enumerate(sparseStiffness) if not i in toRemove]
            

    sparseLigament = {"startIDs":sparseStartNodes,
        "length":sparseLengths, "stiffness":sparseStiffness,
        "start":sparseStartPoints, "end":sparseEndPoints}

    return sparseLigament

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser( description="Given a surface mesh and a corresponding volume mesh of an organ, set up ligaments connecting to the organ. In default mode, the paths along the surface mesh (i.e. the nodes which a ligament connects to) are created automatically. If a mesh is given via --surface_path [OBJ_FILE], this path is used is instead." )
    parser.add_argument("surface_mesh", type=str, help="")
    parser.add_argument("volume_mesh", type=str, help="")
    parser.add_argument("--outdir", type=str, default="output", help="")
    parser.add_argument("--random_seed", type=int, default=1, help="")

    group = parser.add_mutually_exclusive_group( required=True )

    group.add_argument("--num_ligaments", type=int, help="Number of random ligaments to create. Will generate random paths along the surface and use these to connect the ligaments to.")

    group.add_argument("--surface_nodes", type=filepath, help="Use predefined nodes to connect the ligaments to. The nodes to use are given in the form of an OBJ file which holds the (surface) points to connect the ligament to. Note: This file must not contain any faces (only vertices)!")

    args = parser.parse_args()

    random.seed( args.random_seed )

    surface = loadMesh( args.surface_mesh )

    volume = loadMesh( args.volume_mesh )

    if args.num_ligaments:
        print("Selecting random paths along surface")
        paths = randomSurfaceConnections( surface, args.num_ligaments )
    else:
        print("Loading surface nodes from file", args.surface_nodes)
        paths = predefinedSurfaceConnections( loadMesh( args.surface_nodes ) )
  
    print( "Creating ligament end points" )
    ligaments = []
    for p in paths:
        l = extrudeLigament( surface, p )
        ligaments.append(l)

    print( "Removing springs to ensure only one spring is connected per node" )
    ligamentsPruned = []
    for l in ligaments:
        pruned = pruneLigament( volume, surface, l )
        ligamentsPruned.append( pruned )

    if not os.path.exists( args.outdir ):
        os.makedirs( args.outdir )

    # For debugging purposes, write out the surface ligaments:
    for i in range(len(ligaments)):
        filename = "ligament{}.vtp".format(i)
        writeSpringsVTK( ligaments[i], os.path.join( args.outdir, filename ) )
        filename = "ligamentSparse{}.vtp".format(i)
        writeSpringsVTK( ligamentsPruned[i], os.path.join( args.outdir, filename ) )

    # Write out the final results:
    for i in range(len(ligamentsPruned)):
        filename = "ligament{}.npz".format(i)
        wrtieSprings( ligamentsPruned[i], os.path.join( args.outdir, filename ) )



import random
import numpy as np
from vtk import *
from collections import deque
import os
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),"../Utils"))
from generalutils import *
from vtkutils import *

def gridRayCast( volume, numSteps=32 ):
    """
    Casts rays from below the volume. Returns list of mesh indices which were hit by a ray."

    Arguments:
    ----------
    volume (vtkPointSet, for example a vtkUnstructuredGrid):
        Organ volume. Rays will be shot from the -Y direction, assuming this is "below"
        the organ.
    numSteps (int):
        Number of steps along each side of the grid. Overall, numSteps^2 rays will be
        cast in a regular grid with side length numSteps. The grid is bounded by the 
        volume's bounding box.

    Returns:
    ---------- 
    list of int:
        The IDs of the volume nodes at which rays have hit. These can be used to set up
        springs.
    """

    volume = unstructuredGridToPolyData( volume )

    # Locator for ray trace:
    obbTree = vtkOBBTree()
    obbTree.SetDataSet( volume )
    obbTree.BuildLocator()

    # Locator for nearby points:
    locator = vtkPointLocator( )
    locator.SetDataSet( volume )
    locator.SetNumberOfPointsPerBucket(1)
    locator.BuildLocator()

    bounds = volume.GetBounds()

    yMin = bounds[2]
    yMax = bounds[3]
    points = vtkPoints()
    cellIds = vtkIdList()

    springStartIDs = []
    # Iterate over the points in a regular grid:
    for step_x in range(numSteps+1):
        for step_z in range(numSteps+1):
            # Calculate grid coordinates. The grid should be bounded by the bounding-box,
            # i.e. the grid lies on the -Y plane of the bounding box.
            x = (bounds[1]-bounds[0])*step_x/numSteps + bounds[0]
            z = (bounds[5]-bounds[4])*step_z/numSteps + bounds[4]

            # Shoot ray "Upwards" towards the organ
            code = obbTree.IntersectWithLine((x, yMin, z), (x, yMax, z), points, cellIds)

            # If the ray hit...
            if points.GetNumberOfPoints() > 0:
                hitPoint = points.GetPoint(0)
                # Find the node closest to the hit:
                closestMeshPointID = locator.FindClosestPoint( hitPoint )
                # If the node has not been hit before, append it to the list of hit nodes
                if not closestMeshPointID in springStartIDs:
                    springStartIDs.append( closestMeshPointID )

    # Return the list of nodes which have been hit by a ray:
    return springStartIDs

def generateSprings( volume, springStartIDs, topologyOffset=0.01, maxTopologyNoise=0.02,
        minStiffness=10, maxStiffness=20,  maxSpringLength=0 ):
    """
    Create a spring on each node given by springStartIDs. For each spring, the end point
    will have a random offset in the Y-direction in an attempt to simulate organs lying
    underneath the organ and excerting pressure from below it.

    Arguments:
    ----------
    volume (vtkPointSet, for example a vtkUnstructuredGrid):
        Volume which the springs should connect to
    springStartIDs (list of int):
        Node IDs of "volume" at which the sprins should start
    topologyOffset (float):
        The distance (in meters) which the end point of a spring has from
        the node it is connected to. This distance is summed to perlin noise, if
        maxTopologyNoise is not zero.
    maxTopologyNoise (float):
        Weight factor to apply to perlin noise evaluated at the considered ID, 
        when defining the offset for springs' end points. 
    minStiffness (float):
        Min stiffness value springs can have.
    maxStiffness (float):
        Max stiffness value springs can have.
    maxSpringLength (float):
        Weight factor to apply to perlin noise evaluated at the considered ID, 
        when defining springs' rest length. 
        
    Returns:
    ----------
    dict
        A description of the springs. The dict holds the following entries:
            - "startIDs" (list of int): 
                Node IDs of the volume which the springs are connected to. 
                Same as the input springStartIDs. 
            - "end" (list of tuples of 3 floats): 
                End points of each spring (associated to each startID). 
                End points coordinates are computed as start points coordinates + offset, 
                where offset(startID) = p_noise(startID) * maxTopologyNoise + topologyOffset. 
            - "length" (list of floats): 
                Rest length values of each spring (associated to each startID).
                Rest length values are computed as p_noise(startID) * maxSpringLength.
            - "stiffness" (list of floats): 
                Stiffness values of each spring (associated to each startID).
                Stiffness is computed as p_noise(startID) * (maxStiffness-minStiffness) + minStiffness
        
        p_noise represents perlin noise generated with freq=25 and phase=150.
    """

    noise = vtkPerlinNoise()
    noise.SetFrequency( 25, 25, 25 )
    noise.SetPhase( random.random()*150, random.random()*150, random.random()*150 )

    springStartPoints = []
    springEndPoints = []
    stiffness = []
    lengths = []

    for ID in springStartIDs:
        pt = volume.GetPoint( ID )
        
        # Sample the 3D noise function at the given point:
        rnd = noise.EvaluateFunction( pt )
        # Convert random value into a random distance in meters:
        rndLength = rnd*maxTopologyNoise + topologyOffset
        
        # Create the spring end point at a rndLength offset from the start node:
        springStartPoints.append( (pt[0], pt[1], pt[2]) )
        springEndPoints.append( (pt[0], pt[1]+rndLength, pt[2]) )

        # These values probably need work:
        #stiffness.append( random.uniform( minStiffness, maxStiffness ) )
        stiffness.append( abs(rnd)*(maxStiffness-minStiffness) + minStiffness )
        #lengths.append( rndLength*0.25 )
        lengths.append( abs(rnd)*maxSpringLength )

    # Debug output of noise:
    noiseArray = vtkDoubleArray()
    noiseArray.SetNumberOfComponents(1)
    noiseArray.SetNumberOfTuples(volume.GetNumberOfPoints())
    noiseArray.SetName( "perlin noise" )
    for i in range(volume.GetNumberOfPoints()):
        pt = volume.GetPoint( i )
        noiseArray.SetTuple1( i, noise.EvaluateFunction( pt ) )
    volume.GetPointData().AddArray( noiseArray )

    return {"startIDs":springStartIDs, "start":springStartPoints, "end":springEndPoints,
            "stiffness":stiffness, "length":lengths}

def removeDuplicatedIDs( curSpringIDs, prevSpringIDs ):
    """
    Removes indices of curSpringIDs that are already present in prevSpringIDs.

    Arguments:
    ----------
    curSpringIDs (list of ints):
        List of IDs where to remove indices.
    prevSpringIDs (list of ints):
        Reference list of IDs.
        
    Returns:
    ----------
    list of ints
        curSpringIDs where the IDs already present in prevSpringIDs have been removed.
    """
    # Input are two lists and removes from cur ind already rpesent in prev
    diffSpringIDs = curSpringIDs

    if prevSpringIDs:
        curSpringIDs = set(curSpringIDs)
        prevSpringIDs = set(prevSpringIDs)
        if len(curSpringIDs.intersection(prevSpringIDs)) > 0:
            diffSpringIDs = list(curSpringIDs.difference(prevSpringIDs))
    return diffSpringIDs

def springs2HeightMap( imgSize ):
    """
    TODO
    """

    # TODO!
    heightMap = np.zeros( [imgSize, imgSize] )
    for i, nodeID in enumerate(springs["startIDs"]):
        # Write the offset value also into the grid:
        relX = (pt[0]-bounds[0])/(bounds[1]-bounds[0])
        relZ = (pt[2]-bounds[4])/(bounds[5]-bounds[4])
        x = int(relX*imgSize)  # Round to nearest pixel coordinate
        z = int(relZ*imgSize)  # Round to nearest pixel coordinate
        if x < numSteps and z < numSteps:
            heightMap[x,z] = rnd/maxTopologyOffset

    return heightMap

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser( description="Given a surface mesh and a corresponding volume mesh of an organ, set up (random) springs simulating surrounding tissue." )
    #parser.add_argument("surfaceMesh", type=str, help="")
    parser.add_argument("volumeMesh", type=str, help="")
    parser.add_argument("--outdir", type=str, default="output", help="")
    parser.add_argument("--random_seed", type=int, default=1, help="")

    args = parser.parse_args()

    random.seed( args.random_seed )

    volume = loadMesh( args.volumeMesh )

    springStartIDs = gridRayCast( volume )
    springs = generateSprings( volume, springStartIDs )


    if not os.path.exists( args.outdir ):
        os.makedirs( args.outdir )

    # Write out the final results:
    writeSprings( springs, os.path.join( args.outdir, "surroundingTissue.npz" ) )

    #import imageio
    #imageio.imwrite( os.path.join(args.outdir, "height.png"), 128+heightMap*127 )

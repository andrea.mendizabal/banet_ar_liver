import random
import numpy as np
from vtk import *
import os
import sys
from scipy.spatial.transform import Rotation

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),"../Utils"))
from generalutils import *
from vtkutils import *

def getForceIndices( surface, minRoi, maxRoi, subset=None ):
	""" 
	Gets surface indices within a random spherical region of interest. 
	Point indices are extracted on the surface.

	Arguments:
	----------
	surface (vtkDataSet)
		surface where the points are extracted
	volume (vtkDataSet)
		volume whose indices are returned
	minRoi (float)
		minimum radius of the random spherical ROI created
	maxRoi (float)
		maximum radius of the random spherical ROI created
	subset (list of ids):
        surface IDs to consider in ROI selection. If not specified, all the 
        points in the surface will be considered. A maximum of 10 trials is performed to
		extract ids from this subset. If it is not possible, the subset is returned anyways
		but points will not strictly belong to it.

	Returns:
	----------
	list of ints
		list with the indices of the extracted points. Indices are referred to surface points.
	float
		radius of the extracted ROI
	"""

	r = round(random.uniform(minRoi, maxRoi), 4)

	numTrials = 0
	while(numTrials < 10):
		numTrials += 1
		centerID = random.randint(0, surface.GetNumberOfPoints())
		surfaceForcesIDs = getIndicesInROI( surface, centerID, r)

		# Convert surface indices into volume indices
		#forcesIDs = getClosestPoints(surface, volume, subset=surfaceForcesIDs, discardDuplicate=True)

		if len( surfaceForcesIDs ):
			# if there are points in forceIDs that do not belong to subset, try again. 
			if subset and (len(set(surfaceForcesIDs).difference(set(subset)))):
				continue
			else:
				break

	if numTrials >= 10:
		print("WARNING: {} out of {} forces are applied on points out of the specified subset".format(len(set(surfaceForcesIDs).difference(set(subset))), len(surfaceForcesIDs)))
	return surfaceForcesIDs, r

def generateForces( forcesIDs, minForce=0.0, maxForce=1.0, direction=None ):
	"""
	Generates a random force and returns a dict with force values and force indices.

	Arguments:
	----------
	forcesIDs (list of int):
		Node IDs where forces have to be applied, to be written to the output dictionary.
	minForce (float):
		Minimum allowed magnitude of the random force.
	maxForce (float):
		Maximum allowed magnitude of the random force.
	direction (ndarray):
		Desired force direction. If None, a random direction is picked. Default: None.
		
	Returns:
	----------
	dict
		A description of the forces. The dict holds the following entries:
			- "forcesIDs" (list of int): 
				Node IDs where forces have to be applied. They correspond to the input forcesIDs. 
			- "force" (list of 3 floats):
				Force vector. 
	"""

	if direction is None:
		d = random_unit_vector()
	else:
		d = direction

	forces = get_random_forces_around_direction(n=1, direction=d, min_force=minForce, max_force=maxForce)[0]
	return {"forcesIDs":forcesIDs, "forces":forces}

def perturbDirection( direction, minAlpha=0.0, maxAlpha=360.0 ):
	"""
	Perturbates a direction by applying a combination of rotations in x, y, z 
	of three random angles between minAlpha and maxAlpha.

	Arguments:
	----------
	direction (ndarray):
		starting force direction that has to be perturbed.
	minAlpha (float):
		Minimum angle to perturb the direction.
	maxAlpha (float):
		Maximum angle to perturb the direction.
		
	Returns:
	----------
	direction
		Rotated direction 
	"""

	alpha = random.uniform( minAlpha, maxAlpha )
	beta = random.uniform( minAlpha, maxAlpha )
	gamma = random.uniform( minAlpha, maxAlpha )

	rx = Rotation.from_euler('x',alpha,degrees=True)
	ry = Rotation.from_euler('y',beta,degrees=True)
	rz = Rotation.from_euler('z',gamma,degrees=True)
	r = rz*ry*rx
	direction = r.apply(direction)
	return direction

def get_random_forces_around_direction(n, direction, min_force=0., max_force=1.):
	""" 
	Gets n random forces around a given direction.

	Arguments
	-----------
	n : int
		Number of forces to generate.
	direction : list
		Force direction.
	min_force : float
		Minimum force magnitude (default 0.0).
	max_force : float
		Maximum force magnitude (default 1.0).

	Returns
	-----------
	list
		of the forces with random magnitude and given direction.
	"""
	forces = np.zeros((n, 3))
	direction = np.asarray(direction)

	for i in range(n):
		magnitude = min_force + (max_force - min_force) * random.random()
		forces[i] = direction*magnitude
	return forces

def random_unit_vector():
	"""
	Generates a random 3D unit vector (direction) with a uniform spherical distribution.
	Borrowed from http://stackoverflow.com/questions/5408276/python-uniform-spherical-distribution
	"""
	phi = random.uniform(0,np.pi*2)
	costheta = random.uniform(-1,1)

	theta = np.arccos( costheta )
	x = np.sin( theta) * np.cos( phi )
	y = np.sin( theta) * np.sin( phi )
	z = np.cos( theta )

	unit_vector = (x,y,z)
	return unit_vector

def writeForces( forces, filename ):
	"""
	Writes forces to a .npz file.

	Arguments:
	------------
	forces (dict)
		Dictionary describing the forces. Must have two entries, "forcesIDs", "forces". 
		The forcesIDs is a list of ints, the forces is a list of floats. 
	filename (str)
	
	"""
	print("Writing", filename)
	forcesIDs = forces["forcesIDs"]
	forces = forces["forces"]

	np.savez( filename, forcesIDs=forcesIDs, forces=forces )

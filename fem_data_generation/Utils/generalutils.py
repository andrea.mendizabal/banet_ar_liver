import os
import math
import random
import re
import subprocess
import yaml

# Check if string is a valid path. If not, try to create it. If not, fail.
def path(string):
    if os.path.isdir(string):
        return string
    else:
        try:
            os.makedirs(string)
            return string
        except:
            raise NotADirectoryError(string)

# Check if string is a valid file. If not, fail.
def filepath(string):
    if os.path.isfile(string):
        return string
    else:
        raise FileNotFoundError(string)

def directoryForIndex( index, outdir ):
    d = os.path.join( outdir, "{:06d}".format(index))
    if not os.path.exists( d ):
        os.makedirs( d )
    return d

def getSurfaceMeshName( directory ):
    meshManipFileName = os.path.join( directory, "surface_manip.stl" )
    if os.path.exists( meshManipFileName ):
        return "surface_manip.stl"
    return "surface.stl"


def findFiles( directory, pattern, exact_pattern=False ):
    """
    Find all files matching a pattern in directory. 
    By default, exact_pattern is False, thus the function searches for all
    the files in directory whose name contains pattern. If instead exact_pattern is True,
    the function looks for filenames matching a specific pattern expressed with re syntax.
    """
    if exact_pattern:
        fileList = []
        for f in os.listdir( directory ):
            fullpath = os.path.join( directory, f )
            if os.path.isfile( fullpath ):
                if re.match( pattern, f ):
                    fileList.append( f )
    else:
        fileList = [f for f in os.listdir( directory ) if pattern in f ]
    return fileList


def removeFiles( directory, pattern, exact_pattern=False ):
    """
    Remove all files in directory matching pattern
    """
    
    files = findFiles( directory, pattern, exact_pattern )
    for f in files:
        os.remove( os.path.join( directory, f ) )

def str2list( inputString ):
    """
    Converts the input string into a list of ints.
    """
    splitString = inputString.strip('[]').split(',')
    outputList  = [int(s.replace("'", "").lstrip()) for s in splitString]
    return outputList
    
def logRunInfo( outputFolder, args ):
    result = subprocess.run(['git', 'rev-parse', 'HEAD'], stdout=subprocess.PIPE)
    sha = result.stdout.decode('utf-8')
    result = subprocess.run(['hostname'], stdout=subprocess.PIPE)
    hostname = result.stdout.decode('utf-8')

    if not os.path.exists( outputFolder ):
        os.makedirs( outputFolder )

    print("Saving results to: {}".format(outputFolder))

    runinfoFile = os.path.join( outputFolder, "RunInfo" )
    print("Saving run info to {}".format(runinfoFile) )
    with open( runinfoFile, "w" ) as f:
        f.write("Git commit:\t" + str(sha))
        f.write("Hostname:\t" + str(hostname))
        f.write("\nArguments:\n")
        for key,value in vars(args).items():
            f.write("\t" + key + "\t" + str(value) + "\n")

#########################################################################
## Config loading and handling:
class ConfigMeta(type):
    def __getitem__( cls, key ):
        return cls.data[key]
    def __contains__( cls, key ):
        return key in cls.data

class Config(metaclass=ConfigMeta):

    data = []
    
    @staticmethod
    def load( filename ):
        with open(filename, 'r') as stream:
            Config.data = yaml.load(stream, Loader=yaml.SafeLoader)
    

    

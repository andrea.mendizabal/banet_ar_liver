# Script to clean the given dataset using new cleaning parameters specified in the config file.
# The cleaned samples are not removed but renamed with the tag '_discard'. 
# Use the --revert input flag if you want to do through the discarded samples and recover those meeting some new conditions.

import argparse
import sys
import os
import shutil
import yaml
import copy
import time
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),"Utils"))
from generalutils import *
from vtkutils import *

parser = argparse.ArgumentParser(description="Generate a dataset of simulated organ deformations.")
parser.add_argument("outdir", type=path, help="Output folder")
parser.add_argument("num", type=int, help="Number of samples to create")
parser.add_argument("--start_num", type=int, default=0, help="ID of first sample to be created (default: %(default)d).")
parser.add_argument("--mesh", type=filepath, help="Surface mesh to use for all samples. If omitted, a random mesh will be generated for every sample.")
parser.add_argument("--config", type=filepath, default="../Config/input_parameters_liver_fixed.yml", help="Path to config yaml (default: %(default)s).")
parser.add_argument("--revert", action="store_true", help="If we want to revert changes and come back to original vox files")

args = parser.parse_args()

## Load the base config file. This will be used to set up the simulation as well as
## for post-processing:
Config.load( args.config )

# Copy the config to the output folder:
shutil.copy( args.config, args.outdir )

samples_range = range(args.start_num,args.start_num+args.num)

# visible surface range
surfaceAmountMin    = Config["intraoperative_surface"]["surface_amount_min"]
surfaceAmountMax    = Config["intraoperative_surface"]["surface_amount_max"]

# displacement range
minAllowedDispl     = Config["cleaning"]["min_displacement"]
maxAllowedDispl     = Config["cleaning"]["max_displacement"]

# # surface area range
# minAllowedArea      = Config["cleaning"]["min_surface_area"]
# maxAllowedArea      = Config["cleaning"]["max_surface_area"]

# filenames to change
original_filenames = ['partialDeformedSurface.vtp', 'voxelized.vts', 'stats.yml']
discarded_filenames = ['partialDeformedSurface_discard.vtp', 'voxelized_discard.vts', 'stats_discard.yml']

if not args.revert:
    old_names = original_filenames
    new_names = discarded_filenames
    stats_filename = 'stats.yml'
else:
    old_names = discarded_filenames
    new_names = original_filenames
    stats_filename = 'stats_discard.yml'

for i in samples_range:
    curDir = directoryForIndex( i, args.outdir )

    try:
        statsFile = os.path.join( curDir, stats_filename ) 
        if os.path.exists( statsFile ):  
            with open(statsFile, 'r') as stream:
                stats = yaml.load(stream, Loader=yaml.SafeLoader)

            clean_sample = args.revert
            maxDispl = stats["MaxDisplacement"]
            visSurf  = stats["VisibleSurfaceAmount"]
            surfArea = stats["SurfaceArea"]

            if maxDispl < minAllowedDispl or maxDispl > maxAllowedDispl:
                clean_sample = not( clean_sample )
            elif visSurf < surfaceAmountMin or visSurf > surfaceAmountMax:
                clean_sample = not( clean_sample )
            # elif surfArea < minAllowedArea or surfArea > maxAllowedArea:
            #     clean_sample = not( clean_sample )

            if clean_sample:
                print("Renaming files for sample {}".format(i))

                for k in range( len(old_names) ):
                    try:
                        old_name = os.path.join( curDir, old_names[k] )
                        new_name = os.path.join( curDir, new_names[k] )
                        os.rename( old_name, new_name )
                    except IOError as e:
                        print("CAUGHT ERROR: '{}' - Skipping case {}".format(e,i))
                    
    except KeyboardInterrupt as e:
        exit()

print("!!! REMEMBER to recompute dataset statistics. !!!")
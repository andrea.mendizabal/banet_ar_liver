from vtk import *
import numpy as np
import math
import sys
import os
import random

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),"Utils"))

def loadMesh( filename ):
	"""
	Loads a mesh using VTK. Supported file types: stl, ply, obj, vtk, vtu, vtp, pcd.

	Arguments:
	---------
	filename (str)
	
	Returns:
	--------
	vtkDataSet
		which is a vtkUnstructuredGrid or vtkPolyData, depending on the file type of the mesh.
	"""

	# Load the input mesh:
	fileType = filename[-4:].lower()
	if fileType == ".stl":
		reader = vtkSTLReader()
		reader.SetFileName( filename )
		reader.Update()
		mesh = reader.GetOutput()
	elif fileType == ".obj":
		reader = vtkOBJReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".ply":
		reader = vtkPLYReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".vtk":
		reader = vtkUnstructuredGridReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".vtu":
		reader = vtkXMLUnstructuredGridReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".vtp":
		reader = vtkXMLPolyDataReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".pcd":
		import pcl
		pc = pcl.load( filename )
		pts = vtkPoints()
		verts = vtkCellArray()
		for i in range( pc.size ):
			pts.InsertNextPoint( pc[i][0], pc[i][1], pc[i][2] )
			verts.InsertNextCell( 1, (i,) )
		mesh = vtkPolyData()
		mesh.SetPoints( pts )
		mesh.SetVerts( verts )
 
	else:
		raise IOError("Mesh should be .vtk, .vtu, .vtp, .obj, .stl, .ply or .pcd file!")

	if mesh.GetNumberOfPoints() == 0:
		raise IOError("Could not load a valid mesh from {}".format(filename))
	return mesh


def writeMesh( mesh, filename ):
	"""
	Saves a VTK mesh to file. 
	Supported file types: stl, ply, obj, vtk, vtu, vtp, pcd.

	Arguments:
	---------
	mesh (vtkDataSet):
		mesh to save
	filename (str): 
		name of the file where to save the input mesh. MUST contain the desired extension.
	
	"""

	if mesh.GetNumberOfPoints() == 0:
		raise IOError("Input mesh has no points!")

	# Get file format
	fileType = filename[-4:].lower()
	if fileType == ".stl":
		writer = vtkSTLWriter()
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update()
	elif fileType == ".obj":
		writer = vtkOBJWriter()
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update() 
	elif fileType == ".ply":
		writer = vtkPLYWriter()
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update() 
	elif fileType == ".vtk":
		writer = vtkUnstructuredGridWriter()
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update() 
	elif fileType == ".vtu":
		writer = vtkXMLUnstructuredGridWriter() 
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update() 
	elif fileType == ".vts":
		writer = vtkXMLStructuredGridWriter() 
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update() 
	elif fileType == ".vtp":
		writer = vtkXMLPolyDataWriter()
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update() 
	else:
		raise IOError("Supported extensions are .vtk, .vtu, .vts, .vtp, .obj, .stl, .ply!")


def loadStructuredGrid( filename ):
	"""
	Loads a structured grid using VTK. Supported file types: vtk, vts.

	Arguments:
	---------
	filename (str)
	
	Returns:
	--------
	vtkDataSet
		of type vtkStructuredGrid.
	"""
	fileType = filename[-4:].lower()
	if fileType == ".vtk":
		reader = vtkStructuredGridReader()
		reader.SetFileName( filename )
		reader.Update() 
		grid = reader.GetOutput()
	elif fileType == ".vts":
		reader = vtkXMLStructuredGridReader()
		reader.SetFileName( filename )
		reader.Update() 
		grid = reader.GetOutput()
	else:
		raise IOError(filename + " should be .vtk or .vts")
	return grid


def writeSpringsVTK( spring, filename ):
	"""
	Writes a ligament or spring to a .vtp file, mainly for debugging purposes.
	Note that this only handles the start and end points of the springs and ignores any
	stiffness or other information.

	Arguments:
	------------
	spring (dict)
		Dictionary describing the spring. Must have two entries, "start" and "end".
		Both entries are a list of points (where each point is a tuple of 3 float values).
	filename (str)
	"""
	print("Writing debug output", filename)
	startPoints = spring["start"]
	endPoints = spring["end"]

	points = vtkPoints()
	verts = vtkCellArray()
	for i in range(len(startPoints)):
		points.InsertNextPoint( startPoints[i] )
		verts.InsertNextCell( 1, [points.GetNumberOfPoints()-1] )

	for i in range(len(endPoints)):
		points.InsertNextPoint( endPoints[i] )
		verts.InsertNextCell( 1, [points.GetNumberOfPoints()-1] )

	offset = len(startPoints)
	lines = vtkCellArray()
	for i in range(len(startPoints)):
		lines.InsertNextCell( 2, [i, i+offset] )

	polyData = vtkPolyData()
	polyData.SetPoints( points )
	polyData.SetVerts( verts )
	polyData.SetLines( lines )

	writer = vtkXMLPolyDataWriter()
	writer.SetInputData( polyData )
	writer.SetFileName( filename )
	writer.Update()

def writeSprings( spring, filename ):
	"""
	Writes a ligament or spring to a .npz file.

	Arguments:
	------------
	spring (dict)
		Dictionary describing the spring. Must have four entries, "startIDs", "endPoints",
		"legnth" and "stiffness". The startIDs is a list of ints, the endPoints is a list of
		points (i.e. tuples of 3 floats), the length and stiffness are lists of floats. All
		four lists should have the same length N (where N is the number of springs).
	filename (str)
	"""
	print("Writing", filename)
	startIDs = spring["startIDs"]
	endPoints = spring["end"]
	length = spring["length"]
	stiffness = spring["stiffness"]

	np.savez( filename, startIDs=startIDs, endPoints=endPoints,
			length=length, stiffness=stiffness )

def findArraysByName( data, string ):
	""" 	
	Search for all arrays in data which contain "string" in their name and return them. 
	"""
	foundArrays = []
	for i in range(0, data.GetNumberOfArrays() ):
		if string in data.GetArray(i).GetName():
			foundArrays.append( data.GetArray(i) )
	return foundArrays


def extractSurface( inputMesh ):
	surfaceFilter = vtkDataSetSurfaceFilter()
	surfaceFilter.SetInputData( inputMesh )
	surfaceFilter.Update()
	surface = surfaceFilter.GetOutput()

	return surface

def getRandomSurfaceArea( mesh, radius, triangleIDs, minRequiredCells=0, centerTriangleID=None ):
	
	# Final area should roughly have this size:
	targetAreaSize = math.pi*radius**2

	# Choose a random triangle ID from the list of triangle IDs to act as the center triangle:
	if centerTriangleID == None:
		centerTriangleID = triangleIDs[random.randint(0,len(triangleIDs)-1)]
	
	centerCell = mesh.GetCell( centerTriangleID )

	region = []
	area = 0

	borderCells = [centerTriangleID]
	checkedCells = []

	while area < targetAreaSize:
		if len(borderCells) == 0:
			break

		newBorderCells = []
		for borderCellID in borderCells:
			checkedCells.append(borderCellID)

			idList = vtkIdList()
			newNeighbours = []

			edge1 = mesh.GetCell( borderCellID ).GetEdge( 0 ).GetPointIds()
			mesh.GetCellNeighbors( borderCellID, edge1, idList )
			newNeighbours += [idList.GetId(i) for i in range(0,idList.GetNumberOfIds())]
			edge2 = mesh.GetCell( borderCellID ).GetEdge( 1 ).GetPointIds()
			mesh.GetCellNeighbors( borderCellID, edge1, idList )
			newNeighbours += [idList.GetId(i) for i in range(0,idList.GetNumberOfIds())]
			edge3 = mesh.GetCell( borderCellID ).GetEdge( 2 ).GetPointIds()
			mesh.GetCellNeighbors( borderCellID, edge1, idList )
			newNeighbours += [idList.GetId(i) for i in range(0,idList.GetNumberOfIds())]
			#for n in newNeighbours:
			#    print(n, mesh.GetCell(n).GetCellType())

			for newID in newNeighbours:
				if newID not in checkedCells and newID not in borderCells and not newID in newBorderCells:
					if mesh.GetCell( newID ).GetCellType() == VTK_TRIANGLE:
						newBorderCells.append( newID )

			area += mesh.GetCell( borderCellID ).ComputeArea()
			region.append( borderCellID )
		  
		# After traversing the list, delete old borderCell list and replace it by the new one:
		borderCells = newBorderCells

	if len(region) >= minRequiredCells:
		return region

	return getRandomSurfaceArea( mesh, radius + 0.01, triangleIDs, minRequiredCells )

def removeUnwantedArrays( data, arraysToKeep ):

	unwantedArrayFound = True
	while unwantedArrayFound:
		unwantedArrayFound = False
		for i in range(0, data.GetNumberOfArrays()):
			if not data.GetArray(i).GetName() in arraysToKeep:
				unwantedArrayFound = True
				data.RemoveArray(i)
				break

class ErrorObserver:

   def __init__(self):
	   self.__ErrorOccurred = False
	   self.__ErrorMessage = None
	   self.CallDataType = 'string0'

   def __call__(self, obj, event, message):
	   self.__ErrorOccurred = True
	   self.__ErrorMessage = message

   def ErrorOccurred(self):
	   occ = self.__ErrorOccurred
	   self.__ErrorOccurred = False
	   return occ

   def ErrorMessage(self):
	   return self.__ErrorMessage

def makeSingleFloatArray( name, val ):
	arr = vtkFloatArray()
	arr.SetNumberOfTuples(1)
	arr.SetNumberOfComponents(1)
	arr.SetTuple1(0,val)
	arr.SetName(name)
	return arr

def surfaceArea( mesh ):
	area = 0
	for i in range(mesh.GetNumberOfCells()):
		if mesh.GetCell(i).GetCellType() == VTK_TRIANGLE:
			p0 = mesh.GetCell(i).GetPoints().GetPoint(0)
			p1 = mesh.GetCell(i).GetPoints().GetPoint(1)
			p2 = mesh.GetCell(i).GetPoints().GetPoint(2)
			a = mesh.GetCell(i).TriangleArea(p0,p1,p2)
			area += a

	areaArr = makeSingleFloatArray( "surfaceArea", area )
	mesh.GetFieldData().AddArray(areaArr)

	return area

def getMaximumDisplacement( mesh_initial, mesh_deformed ):
	"""     
	Given an initial and deformed mesh, computes the maximum displacement.
	
	Arguments:
	---------
	mesh_initial (vtkDataSet):
		Topology of the starting mesh
	mesh_deformed (vtkDataSet):
		Topology of the deformed mesh
	
	Returns:
	--------
	float
		Norm of the maximum displacement.
	"""
	if not mesh_initial.GetNumberOfPoints() == mesh_deformed.GetNumberOfPoints():
		raise IOError( "mesh_initial and mesh_deformed must have the same number of points!" )
	
	displacement = vtkFloatArray()
	displacement.SetNumberOfComponents( 3 )
	displacement.SetNumberOfTuples( mesh_initial.GetNumberOfPoints() )

	for i in range( mesh_initial.GetNumberOfPoints() ):
		p1 = mesh_initial.GetPoint( i )
		p2 = mesh_deformed.GetPoint( i )
		displ = (p2[0]-p1[0], p2[1]-p1[1], p2[2]-p1[2])
		displacement.SetTuple3( i, displ[0], displ[1], displ[2] )

	# Get the maximum value (norm) in the array
	maximum = displacement.GetMaxNorm()
	return maximum

def getMaximumOfArray( arr, fieldData ):
	# First get the maximum value (norm) in the array from the source data
	maximum = arr.GetMaxNorm()
	# Make an array holding only this one maximum value:
	fieldArr = makeSingleFloatArray( arr.GetName() + "_max", maximum )
	# Append the value to the field data
	fieldData.AddArray( fieldArr )
	return maximum

# Sum up two arrays element wise and return an array with the result.
# arr1 and arr2 must have the same number of tuples and components!
def sumOfArrays( arr1, arr2, resultArrayName ):

	tuples = arr1.GetNumberOfTuples()
	components = arr1.GetNumberOfComponents()
	resArr = vtkDoubleArray()
	resArr.SetNumberOfComponents( components )
	resArr.SetNumberOfTuples( tuples )
	resArr.SetName( resultArrayName )
	for i in range(tuples):
		for j in range(components):
			s = arr1.GetComponent(i,j) + arr2.GetComponent(i,j) 
			resArr.SetComponent(i,j,s)

	return resArr


def unstructuredGridToPolyData( ug ):
	"""
	Converts an input unstructured grid into a polydata object. 
	Be careful since PolyData objects cannot contain 3D elements, thus all 
	tetrahedra will be lost with this operation.

	Parameters
	----------
	ug (vtkUnstructuredGrid):
		The input unstructured grid

	Returns
	----------
	vtkPolyData
	
	"""
	geometryFilter = vtkGeometryFilter()
	geometryFilter.SetInputData( ug )
	geometryFilter.Update()
	return geometryFilter.GetOutput()

def polyDataToUnstructuredGrid( pd ):
	"""
	Converts an input polydata into an unstructured grid.

	Parameters
	----------
	pd (vtkPolyData):
		The input polydata

	Returns
	----------
	vtkUnstructuredGrid

	"""
	appendFilter = vtkAppendFilter()
	appendFilter.SetInputData( pd )
	appendFilter.Update()
	return appendFilter.GetOutput()

def addVertsToPolyData( pd ):
	
	verts = vtkCellArray()
	for i in range(pd.GetNumberOfPoints()):
		verts.InsertNextCell( 1, (i,) )

	pd.SetVerts(verts)

def generatePointNormals( surface ):
	# Check if the point normals already exist
	if surface.GetPointData().HasArray("Normals"):
		return surface

	# If no normals were found, generate them:
	normalGen = vtkPolyDataNormals()
	normalGen.SetInputData( surface )
	normalGen.ComputePointNormalsOn()
	normalGen.ComputeCellNormalsOff()
	normalGen.SplittingOff()        # Don't allow generator to add points at sharp edges
	normalGen.Update()
	return normalGen.GetOutput()

def getConnectedVertices( mesh, nodeID ):
	"""
	Find the neighbor vertices of the node with ID nodeID.

	Parameters
	----------
	mesh (vtkDataSet):
		The mesh topology
	nodeID (int):
		The ID of the node for which neighbors are to be found

	Returns
	----------
	list of int:
		IDs of the vertices that are neighbors of nodeID.

	"""
	connectedVertices = []

	#get all cells that vertex 'id' is a part of
	cellIDList = vtkIdList()
	mesh.GetPointCells( nodeID, cellIDList )
	
	for i in range(cellIDList.GetNumberOfIds()):
		c = mesh.GetCell( cellIDList.GetId(i) )
		pointIDList = vtkIdList()
		mesh.GetCellPoints( cellIDList.GetId(i), pointIDList )
		for j in range(pointIDList.GetNumberOfIds()):
			neighborID = pointIDList.GetId(j)
			if neighborID != nodeID:
				if not neighborID in connectedVertices:
					connectedVertices.append( neighborID )
	return connectedVertices


def geodesicDistance( surface, centerNodeID ):
 
	# pre-compute cell neightbors:
	neighbors = {}
	for i in range( surface.GetNumberOfPoints() ):
		neighbors[i] = getConnectedVertices( surface, i )
	
	distance = vtkDoubleArray() 
	distance.SetNumberOfTuples( surface.GetNumberOfPoints() )
	distance.SetNumberOfComponents( 1 )
	distance.Fill( 1e10 )   # initialize with large numbers
	distance.SetName( "geodesic_distance" )

	front = [centerNodeID]
	distance.SetTuple1( centerNodeID, 0 )

	while len(front) > 0:

		curID = front.pop(0)
		curPt = surface.GetPoint( curID )
		curDist = distance.GetTuple1( curID )
		curNeighbors = neighbors[curID]

		# Go through all neighboring points. Check if the distance in those points
		# is still up to data or whether there is a shorter path to them:
		for nID in curNeighbors:

			# Find distance between this neighbour and the current point:
			nPt = surface.GetPoint( nID )
			dist = math.sqrt( vtkMath.Distance2BetweenPoints( nPt, curPt ) )

			newDist = dist + curDist
			if newDist < distance.GetTuple1( nID ):
				distance.SetTuple1( nID, newDist )
				if not nID in front:
					front.append( nID )     # This neighbor node needs to be checked again!
		
	return distance

def getClosestPoints( mesh1, mesh2, subset=None, discardDuplicate=False ):
	"""
	For each point in mesh1, returns the index of the closest point in mesh2. 
	
	Parameters
	----------
	mesh1 (vtkDataSet):
		Topology of the first mesh
	mesh2 (vtkDataSet):
		Topology of the second mesh
	subset (list of ints):
		If specified, a subset of mesh1 nodes are considered. Subset represents
		the list of node indices to consider.
		Default: None
	discardDuplicate (bool):
		If true, the returned indices cannot be present more than once.
		Default: False

	Returns
	----------
	list of int:
		IDs of mesh2 vertices closest to each mesh1 vertex.

	"""
	locator = vtkPointLocator( )
	locator.SetDataSet( mesh2 )
	locator.SetNumberOfPointsPerBucket(1)
	locator.BuildLocator()

	mesh2IDs = []
	if subset is None:
		subset = range(mesh1.GetNumberOfPoints())
		
	for idx in subset:
		mesh1Point = mesh1.GetPoint(idx)
		mesh2PointID = locator.FindClosestPoint( mesh1Point )
		# If we want to discard duplicate indices and we have already found it, skip append
		if discardDuplicate and (mesh2PointID in mesh2IDs):
			pass
		else:
			mesh2IDs.append( mesh2PointID )
	return mesh2IDs

def getDistance( mesh1, mesh2 ):
	"""
	For each point in mesh1, computes the Euclidean distance with its closest point on mesh2.
	Computed distance works on CELLS and NOT on POINTS. This means that, for each point
	in mesh1, the closest point on mesh2 considers the entire mesh, including its SURFACE.
	In this way, the computed distance should not be affected too much by points decimation.
	Returns a list of distances for each point in mesh1.
	
	Parameters
	----------
	mesh1 (vtkDataSet):
		Topology of the first mesh
	mesh2 (vtkDataSet):
		Topology of the second mesh

	Returns
	----------
	list of floats:
		Euclidean distance of each point of mesh1 with the closest point in mesh2.

	"""
	distance = []
	
	# Build a locator on mesh2
	cellLocator = vtkCellLocator()
	cellLocator.SetDataSet( mesh2 )
	cellLocator.BuildLocator()

	# For each point in mesh1
	for i in range(0, mesh1.GetNumberOfPoints() ):
		# Take a point in mesh1
		testPoint = [0]*3
		mesh1.GetPoint( i, testPoint )
		# Find the point in mesh2 closest to it
		cID, subID, dist2 = mutable(0), mutable(0), mutable(0.0)
		closestPoint = [0]*3
		cellLocator.FindClosestPoint( testPoint, closestPoint, cID, subID, dist2 )
		dist = math.sqrt(dist2)

		distance.append(dist)

	return distance

def getIndicesInROI( mesh, centerID, r, subset=None):
	"""
	Get the indices of mesh points belonging to a spherical ROI of radius r and
	centered in node centerID.

	Arguments:
	----------
	mesh (vtkDataSet, for example a vtkUnstructuredGrid):
		Mesh where the ROI is defined. centerID and subset (if defined) must come from this volume.
	centerID (int):
		ID of the node selected as reference point for distance computation.
	r (float):
		The radius (in meters) defining the ROI size. 
	subset (list of ids):
		List of IDs of mesh to consider in ROI selection. If not specified, all the 
		points in the mesh will be considered.
		
	Returns:
	----------
	list of ints
		IDs of the points in mesh falling at a distance < r from node centerID.
	"""
	
	sampledIDs = vtkIdList()  

	# mesh points
	pointsToSample = vtkPolyData()
	if subset is None:
		pointsToSample = unstructuredGridToPolyData( mesh )
	else:
		points = vtkPoints()
		for i in subset:
			points.InsertNextPoint(mesh.GetPoint(i))
		pointsToSample.SetPoints(points)

	centerPoint = mesh.GetPoint(centerID)
	
	pointLocator = vtkPointLocator( )
	pointLocator.SetDataSet( pointsToSample )
	pointLocator.SetNumberOfPointsPerBucket(1)
	pointLocator.BuildLocator()
	pointLocator.FindPointsWithinRadius(r, centerPoint, sampledIDs)

	sampledIDlist = []
	for i in range(sampledIDs.GetNumberOfIds()):
		sampledIDlist.append(sampledIDs.GetId(i))

	return sampledIDlist
	
def smoothPolyData( mesh, n_iter=20, relaxation_factor=0.01,
			   edge_angle=15, feature_angle=45,
			   boundary_smoothing=True, feature_smoothing=False):
	"""
	Smooths a PolyData object by adjust point coordinates using Laplacian smoothing.
	The effect is to "relax" the mesh, making the cells better shaped and
	the vertices more evenly distributed.
	Adapted from https://github.com/pyvista/pyvista/blob/master/pyvista/core/filters.py.

	Parameters
	----------
	mesh: vtkPolyData
		PolyData object to be smoothed.
	n_iter : int
		Number of iterations for Laplacian smoothing.
	relaxation_factor : float, optional
		Relaxation factor controls the amount of displacement in a single
		iteration. Generally a lower relaxation factor and higher number of
		iterations is numerically more stable.
	edge_angle : float, optional
		Edge angle to control smoothing along edges (either interior or boundary).
	feature_angle : float, optional
		Feature angle for sharp edge identification.
	boundary_smoothing : bool, optional
		Boolean flag to control smoothing of boundary edges.
	feature_smoothing : bool, optional
		Boolean flag to control smoothing of feature edges.
	
	Returns
	------
	vtkPolyData:
		Smoothed mesh
	"""

	alg = vtkSmoothPolyDataFilter()
	alg.SetInputData( mesh )
	alg.SetNumberOfIterations(n_iter)
	#alg.SetConvergence(convergence)
	alg.SetFeatureEdgeSmoothing(feature_smoothing)
	alg.SetFeatureAngle(feature_angle)
	alg.SetEdgeAngle(edge_angle)
	alg.SetBoundarySmoothing(boundary_smoothing)
	alg.SetRelaxationFactor(relaxation_factor)
	alg.Update()

	return alg.GetOutput()
	
def appendSurfaces( surface1, surface2 ):
	"""
	docstring
	"""
	append = vtkAppendFilter()
	append.AddInputData( surface1 )
	append.AddInputData( surface2 )
	append.SetMergePoints( True )
	append.Update()
	surface1 = append.GetOutput()
	surface1 = unstructuredGridToPolyData( surface1 )
	return surface1

def createRandomRigidTransform( angDegreeMax, offsetMax ):
	"""
	docstring
	"""
	ang = random.uniform(-1,1)*angDegreeMax
	axis = np.array( [random.uniform(-1,1),
		random.uniform(-1,1),
		random.uniform(-1,1)] )

	axisNorm = np.linalg.norm( axis )
	if axisNorm == 0:
		# Fallback, just in case
		axis = [1,0,0]
	else:
		# Normalize
		axis = axis/axisNorm

	dx = random.uniform(-1,1)*offsetMax
	dy = random.uniform(-1,1)*offsetMax
	dz = random.uniform(-1,1)*offsetMax

	t = vtkTransform()
	t.RotateWXYZ( ang, axis )
	t.Translate( dx, dy, dz )
	return t
	
def applyTransform( mesh, vtkTransform ):
	tf = vtkTransformFilter()
	tf.SetInputData( mesh )
	tf.SetTransform( vtkTransform )
	tf.Update()
	mesh = tf.GetOutput()
	return mesh

def vtkTriangle2PointList( t ):
	return (
			t.GetPoints().GetPoint(0),
			t.GetPoints().GetPoint(1),
			t.GetPoints().GetPoint(2)
			)

def removeDoubleFaces( surfaceMesh ):
	outputMesh = vtkPolyData()
	outputPoints = vtkPoints()
	outputPoints.DeepCopy( surfaceMesh.GetPoints() )
	outputMesh.SetPoints( outputPoints )
	outputCells = vtkCellArray()
	outputMesh.SetPolys( outputCells )

	copied = {}
	skipped = 0

	for i in range( surfaceMesh.GetNumberOfCells() ):
		c = surfaceMesh.GetCell( i )
		if c.GetCellType() == VTK_TRIANGLE:
			pIds = c.GetPointIds()
			idList = [pIds.GetId(i) for i in range(pIds.GetNumberOfIds())]
			idList = sorted(idList)
			idListStr = ",".join(str(x) for x in idList)
			#print(idList, idListStr)
			if not idListStr in copied:
				copied[idListStr] = True
				outputCells.InsertNextCell( 3, idList )
			else:
				skipped += 1

	return outputMesh
		   
def triangleLineIntersection( v0, v1, v2, p0, p1, tol=1e-6 ):
#int rayIntersectsTriangle(float *p, float *d,
			#float *v0, float *v1, float *v2) {

	#float e1[3],e2[3],h[3],s[3],q[3];
	#float a,f,u,v;
	#vector(e1,v1,v0);
	#vector(e2,v2,v0);
	e1 = v1-v0
	e2 = v2-v0

	# Direction:
	d = p1 - p0
	segLen = np.linalg.norm(d)
	if segLen == 0:
		return False, 0
	d = d/segLen

	#crossProduct(h,d,e2);
	#a = innerProduct(e1,h);
	h = np.cross( d, e2 )
	a = np.dot( e1, h )

	if a > -tol and a < tol:
		return False, 0

	f = 1/a
	#vector(s,p,v0);
	s = p0 - v0
	#u = f * (innerProduct(s,h));
	u = f * np.dot( s, h )

	if u < 0.0 or u > 1.0:
		return False, 0

	#crossProduct(q,s,e1);
	#v = f * innerProduct(d,q);
	q = np.cross( s, e1 )
	v = f * np.dot( d, q )

	if v < 0.0 or u + v > 1.0:
		return False, 0

	# at this stage we can compute t to find out where
	# the intersection point is on the line
	#t = f * innerProduct(e2,q);
	t = f * np.dot(e2,q)
	#print(t)

	if t > tol and t < segLen-tol: # segment intersection
		return True, t
	else:   # Line intersection, but not within the segment:
		return False, t

def printTriangle( t ):
	print(t.GetPoints().GetPoint(0))
	print(t.GetPoints().GetPoint(1))
	print(t.GetPoints().GetPoint(2))

def triangleTriangleIntersection( tri0, tri1 ):
	# Bounding box check:
	#b0 = tri0.GetBounds()
	#b1 = tri1.GetBounds()
	#if (b0[1] < b1[0] or b0[3] < b1[2] or b0[5] < b1[4] or
	#        b1[1] < b0[0] or b1[3] < b0[2] or b1[5] < b0[4]):
	#    #print("bb check failed")
	#    return False
	#print("bb check passed")

	#print(tri1, tri2)
	#print("tri1")
	#printTriangle( tri1 )
	#print("tri2")
	#printTriangle( tri2 )

	v0 = np.asarray( tri0.GetPoints().GetPoint(0) )
	v1 = np.asarray( tri0.GetPoints().GetPoint(1) )
	v2 = np.asarray( tri0.GetPoints().GetPoint(2) )
	p0 = np.asarray( tri1.GetPoints().GetPoint(0) )
	p1 = np.asarray( tri1.GetPoints().GetPoint(1) )
	p2 = np.asarray( tri1.GetPoints().GetPoint(2) )
	
	intersects, t = triangleLineIntersection( v0, v1, v2, p0, p1 )
	if intersects:
		return True
	intersects, t = triangleLineIntersection( v0, v1, v2, p1, p2 )
	if intersects:
		return True
	intersects, t = triangleLineIntersection( v0, v1, v2, p2, p0 )
	if intersects:
		return True
	return False

#def triangleTriangleIntersectionVTK( tri0, tri1 ):
#    #print(tri1, tri2)
#    #print("tri1")
#    #printTriangle( tri1 )
#    #print("tri2")
#    #printTriangle( tri2 )
#    x = [0,0,0]
#    pcoords = [0,0,0]
#    t = vtk.reference(0)
#    subId = vtk.reference(0)
#
#    #v0 = np.asarray( tri1.GetPoints().GetPoint(0) )
#    #v1 = np.asarray( tri1.GetPoints().GetPoint(1) )
#    #v2 = np.asarray( tri1.GetPoints().GetPoint(2) )
#    for i in range(3):
#        p0 = np.asarray(tri1.GetEdge(i).GetPoints().GetPoint(0))
#        p1 = np.asarray(tri1.GetEdge(i).GetPoints().GetPoint(1))
#        intersects = tri0.IntersectWithLine( p0, p1, 1e-4, t, x, pcoords, subId )
#        #print( v0, v1, v2, p0, p1 )
#        #intersects, t = triangleLineIntersection( v0, v1, v2, p0, p1 )
#        #print(intersects, t)
#        if intersects:
#            return True
#    return False


def triangleBounds( t ):
	pt0 = t.GetPoints().GetPoint(0)
	pt1 = t.GetPoints().GetPoint(1)
	pt2 = t.GetPoints().GetPoint(2)
	x = [pt0[0], pt1[0], pt2[0]]
	y = [pt0[1], pt1[1], pt2[1]]
	z = [pt0[2], pt1[2], pt2[2]]
	return min(x), max(x), min(y), max(y), min(z), max(z)


def getCellNeighbors( mesh, cellId ):

	cell = mesh.GetCell( cellId )
	
	neighbors = []

	idList = vtkIdList()

	for i in range( cell.GetNumberOfEdges() ):
		edge = cell.GetEdge( i ).GetPointIds()
		mesh.GetCellNeighbors( cellId, edge, idList )
		neighbors += [idList.GetId(i) for i in range(idList.GetNumberOfIds())]

	return neighbors

# def checkSelfIntersection( surfaceMesh ):
# 	print("Checking self-intersections")

# 	#surfaceMesh = removeDoubleFaces( surfaceMesh )

# 	cellLocator = vtkCellLocator()
# 	cellLocator.SetDataSet( surfaceMesh )
# 	#cellLocator.SetNumberOfCellsPerBucket(1)
# 	cellLocator.SetMaxLevel( 10 )
# 	cellLocator.BuildLocator()

# 	intersections = 0
# 	checks = 0
# 	numIds = []
# 	lastPercent = None

# 	intersectionArr = vtkDoubleArray()
# 	intersectionArr.SetName("intersects")
# 	intersectionArr.SetNumberOfComponents(1)
# 	intersectionArr.SetNumberOfTuples( surfaceMesh.GetNumberOfCells() )
# 	intersectionArr.Fill(0)

# 	tArr = vtkDoubleArray()
# 	tArr.SetName("t")
# 	tArr.SetNumberOfComponents(1)
# 	tArr.SetNumberOfTuples( surfaceMesh.GetNumberOfCells() )

# 	pcoordsArr = vtkDoubleArray()
# 	pcoordsArr.SetName("pcoords")
# 	pcoordsArr.SetNumberOfComponents(3)
# 	pcoordsArr.SetNumberOfTuples( surfaceMesh.GetNumberOfCells() )


# 	# Iterate over every triangle and check if it intersects with another triangle:
# 	for id0 in range(surfaceMesh.GetNumberOfCells()):
# 		percent = int((id0/surfaceMesh.GetNumberOfCells()*10))*10
# 		if percent != lastPercent:
# 			print(percent, "%")
# 			lastPercent = percent
# 		#print(id0, "/", surfaceMesh.GetNumberOfCells())
# 		c0 = vtkGenericCell()
# 		surfaceMesh.GetCell(id0, c0)
# 		if c0.GetCellType() == VTK_TRIANGLE:
# 			#c0 = vtkTriangle()
# 			#c0.DeepCopy( c )    # Keep, because future GetCell calls will overwrite!
# 			#tri1 = vtkTriangle2PointList( c )
# 			b = c0.GetBounds()
# 			#bounds = triangleBounds( c0 )
# 			#print("bounds", b, bounds)
# 			idList = vtkIdList()
# 			cellLocator.FindCellsWithinBounds( b, idList )
# 			numIds.append( idList.GetNumberOfIds() )
				
# 			neighborCellIds = getCellNeighbors( surfaceMesh, id0 )

# 			for j in range(idList.GetNumberOfIds()):
# 				id1 = idList.GetId(j)
# 				if id1 in neighborCellIds:
# 					continue
# 				c1 = vtkGenericCell()
# 				surfaceMesh.GetCell(id1, c1)
# 				if c1.GetCellType() == VTK_TRIANGLE and not id0 == id1:
# 					#print("Comparing", c0,c1)
# 					intersects = triangleTriangleIntersection( c0, c1 )
# 					#intersects = c0.IntersectWithCell( c1, tol=1e-5 )
# 					if intersects:
# 						intersections += 1
# 						#print("Intersection:", id0, id1)
# 						intersectionArr.SetTuple1( id0, intersectionArr.GetTuple1( id0 )+1 )
# 						tArr.SetTuple1( id0, 0 )
# 						continue
# 						#pcoordsArr.SetTuple3( id0, *pcoords )
# 					else:
# 						#print("No intersection:", id0, id1)
# 						#intersectionArr.SetTuple1( id0, 0 )
# 						tArr.SetTuple1( id0, -1 )
# 						#pcoordsArr.SetTuple3( id0, 0,0,0 )
# 					checks += 1
					
# 					#tri2 = vtkTriangle2PointList( c2 )
# 					##print(tri1, tri2)
# 					#coplanar = vtk.reference(0)
# 					#res1 = [0,0,0]
# 					#res2 = [0,0,0]
# 					#surfaceId = [0,0]
# 					#tol = 0
# 					#
# 					#p1 = c.GetPoints().GetPoint(0)
# 					#p2 = c.GetPoints().GetPoint(1)
# 					#p3 = c.GetPoints().GetPoint(2)
# 					#q1 = c2.GetPoints().GetPoint(0)
# 					#q2 = c2.GetPoints().GetPoint(1)
# 					#q3 = c2.GetPoints().GetPoint(2)
# 					#intersect = vtkIntersectionPolyDataFilter.TriangleTriangleIntersection(
# 					#    p1,p2,p3, q1,q2,q3, coplanar, res1, res2, surfaceId, tol )
# 					#if intersect > 0:
# 					#    print(intersect)

# 					##intersect = triangle3DTriangle3DIntersection( tri1, tri2 )
# 					#checks += 1
# 					##print("Checking intersection:", id0, id1, intersect )
# 					#if intersect:
# 					#    intersections += 1
# 	print("Found intersections:", intersections, "Checks made:", checks)
# 	print("Avg num ids:", sum(numIds)/len(numIds), len(numIds))

# 	surfaceMesh.GetCellData().AddArray( intersectionArr )
# 	surfaceMesh.GetCellData().AddArray( tArr )
# 	surfaceMesh.GetCellData().AddArray( pcoordsArr )

# 	return intersections > 0, surfaceMesh
					

if __name__ == "__main__":

	filename = sys.argv[1]
	print("Loading", filename)

	surfaceMesh = loadMesh( filename )
	
	intersection, intersectionMesh = checkSelfIntersection( surfaceMesh )

	filenameOut = os.path.join( os.path.dirname( filename ), "intersectionCheck.vtp" )
	print("Writing", filenameOut)

	writer = vtkXMLPolyDataWriter()
	writer.SetInputData( intersectionMesh )
	writer.SetFileName( filenameOut )
	writer.Update()








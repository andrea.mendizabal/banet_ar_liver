import torch
import math
import itertools
import functools
import numpy as np
from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk


### Device Management ###

__PYTORCH_GPU = torch.device("cuda")
__PYTORCH_CPU = torch.device("cpu")

def on_gpu() -> torch.device:
    return __PYTORCH_GPU if torch.cuda.is_available() else on_cpu()

def on_cpu():
    return __PYTORCH_CPU

def is_gpu(device: torch.device):
    return device == __PYTORCH_GPU


### Memory Management ###

__PYTORCH_DEFAULT_TYPE = torch.float32
__PYTORCH_DEFAULT_TYPE_BITS = 32
__MAX_MEM = 0.6

def get_dtype() -> torch.dtype:
    return __PYTORCH_DEFAULT_TYPE

def get_dtype_bits() -> int:
    return __PYTORCH_DEFAULT_TYPE_BITS

def get_usable_memory(device: torch.device = on_gpu(), dtype_bits: int = get_dtype_bits()):
    if not is_gpu(device):
        raise ValueError("device not supported")

    total_memory = torch.cuda.get_device_properties(device).total_memory  # in bit
    usable_memory = total_memory - (torch.cuda.memory_reserved() - torch.cuda.memory_allocated())
    usable_memory *= __MAX_MEM
    return int(usable_memory // dtype_bits)

def estimate_grid_mesh_batch_size(grid, mesh, device=on_gpu(), dtype_bits=get_dtype_bits()):
    usable_mem = get_usable_memory(device, dtype_bits)

    mesh_size = mesh.size(0)
    grid_size = grid.size(0)

    if grid_size < usable_mem or mesh_size < usable_mem:
        if grid_size < mesh_size:
            grid_batch_size = mesh.size(0)
            usable_mem //= grid_batch_size
            return grid_batch_size, usable_mem
        else:
            mesh_batch_size = mesh.size(0)
            usable_mem //= mesh_batch_size
            return usable_mem, mesh_batch_size

    nd_cubic_size = usable_mem ** (1/grid.size(1))
    return nd_cubic_size, nd_cubic_size


### Conversion ###

def as_nd_cubic_tensor(row_vectors, nd=None):
    if nd is None:
        nd = row_vectors.size(1)

    grid_side_length = round(row_vectors.size(0) ** (1/nd))

    if grid_side_length ** nd != row_vectors.size(0):
        raise ValueError("not a cubic grid")

    return row_vectors.view([grid_side_length]*nd)

def as_row_vector_tensor(nd_grid):
    nd = len(nd_grid.size())
    return nd_grid.view(functools.reduce(lambda a, b: a * b, nd_grid.size(), 1), nd)

def vtkPointSetToNumpyArray(ps):
    """
    PointSets are: StructuredGrid, PolyData, ... https://vtk.org/doc/nightly/html/classvtkPointSet.html
    """
    return vtkDataArrayToNumpyArray(ps.GetPoints().GetData())

def vtkDataArrayToNumpyArray(da):
    """
    DataArrays are: FloatArray, BitArray, ... https://vtk.org/doc/nightly/html/classvtkDataArray.html
    """
    return vtk_to_numpy(da)

def numpyArrayToVTK(na):
    return numpy_to_vtk(na)

class Distances:

    @staticmethod
    def batched_min_cdist(x, y, batch_size, p=2.0):
        """
        Calculates the minimum L2-Norm between all row vectors
        Args:
            x: row vectors 1
            y: row vectors 2
            x_batch_size: batch size of row vectors 1
            y_batch_size: batch size of row vectors 2
            p: https://pytorch.org/docs/stable/generated/torch.cdist.html

        Returns:
        the minimum distance from each point in x to a point in y
        """

        solution = torch.empty(x.size(0), 1, device=x.device)

        x_splits = torch.split(x, batch_size[0])
        solution_splits = torch.split(solution, batch_size[0])
        y_splits = torch.split(y, batch_size[1])
        for sol_split, x_split in zip(solution_splits, x_splits):
            init = False
            for y_split in y_splits:
                min_dist = torch.cdist(x_split, y_split, p=p).min(1, keepdim=True).values
                if not init:
                    sol_split.copy_(min_dist)
                    init = True
                else:
                    sol_split.copy_(torch.minimum(sol_split, min_dist))

        return solution

import bpy
import mathutils
import random
import os
import sys
import bmesh

#############################################################################
# Settings
def setMode( mode ):
    try:
        bpy.ops.object.mode_set(mode=mode)
    except:
        pass

def getEdges( mesh, vert ):
    edges = []
    for e in mesh.edges:
        if e.vertices[0] == vert.index or e.vertices[1] == vert.index:
            edges.append(e)
    return edges

def getAverageEdgeLength(Obj):
    
    OWMatrix = Obj.matrix_world
    AvgLength = 0

    for e in Obj.data.edges :  
        v0 = e.vertices[0]
        v1 = e.vertices[1]
        v0Pos = OWMatrix @ Obj.data.vertices[v0].co
        v1Pos = OWMatrix @ Obj.data.vertices[v1].co
        edgeLength = (v0Pos - v1Pos).length
        AvgLength += edgeLength
    
    return AvgLength/len(Obj.data.edges)

def otherVert( mesh, edge, vert ):
    if vert.index == edge.vertices[0]:
        return mesh.vertices[edge.vertices[1]]
    return mesh.vertices[edge.vertices[0]]

def select_more( bm, ntimes=1 ):
    for i in range(0, ntimes):
        sel_verts = [v for v in bm.verts if v.select]
        for v in sel_verts:
            for f in v.link_faces:
                f.select_set(True)
    #sel_edges = [ed for ed in bm.edges if ed.select]
    #for ed in sel_edges:
    #   for f in ed.link_faces:
    #       f.select_set(True)

def edgeFullyOutside( obj, edge ):

    # If any point lies inside the mesh, the edge is not fully outside:
    for i in range(len(edge)):
        v = edge[i]
        if pointInsideMesh( obj, v ):
            return False

    # Iterate over all edges in the vertex list and check if they intersect the mesh:
    world2obj = obj.matrix_world.inverted()
    for i in range(1,len(edge)):
        v0 = edge[i-1]
        v1 = edge[i]
        v0 = world2obj @ v0
        v1 = world2obj @ v1
        d = v1-v0
        if bpy.app.version >= (2, 77, 0):    # API changed
            result, _, _, _ = obj.ray_cast( v0, d.normalized(), distance=d.length )
        else:
            location, _, index = obj.ray_cast( v0, v1 )
            result = not (index == -1)
        if result:
            return False
    return True

# From aothms on https://blenderartists.org/t/detecting-if-a-point-is-inside-a-mesh-2-5-api/485866/4
def pointInsideMesh(obj,point):
    axes = [ mathutils.Vector((1,0,0)) , mathutils.Vector((0,1,0)), mathutils.Vector((0,0,1)) ]
    outside = False

    mat = obj.matrix_world.inverted()
    #f = obj.ray_cast(mat * ray_origin, mat * ray_destination)
    for axis in axes:
        orig = mat @ point
        count = 0
        while True:
            if bpy.app.version >= (2, 77, 0):    # API changed
                result,location,normal,index = obj.ray_cast(orig,axis)
            else:
                end = orig+axis*1e10
                location,normal,index = obj.ray_cast(orig,end)
            if index == -1: break
            count += 1
            orig = location + axis*0.01
        if count%2 == 0:
            outside = True
            break
    return not outside

def checkSideOverlap( obj, cutout, edge0, edge1, edge2, edge3 ):

    # Move into world space:
    #world2obj = obj.matrix_world.inverted()
    e0 = [cutout.matrix_world @ mathutils.Vector(b) for b in edge0]
    e1 = [cutout.matrix_world @ mathutils.Vector(b) for b in edge1]
    e2 = [cutout.matrix_world @ mathutils.Vector(b) for b in edge2]
    e3 = [cutout.matrix_world @ mathutils.Vector(b) for b in edge3]

    edge0Outside = edgeFullyOutside( obj, e0 )
    edge1Outside = edgeFullyOutside( obj, e1 )
    edge2Outside = edgeFullyOutside( obj, e2 )
    edge3Outside = edgeFullyOutside( obj, e3 )
    print("edge0Outside", edge0Outside)
    print("edge1Outside", edge1Outside)
    print("edge2Outside", edge2Outside)
    print("edge3Outside", edge3Outside)

    edgesOutside = int(edge0Outside) + int(edge1Outside) + int(edge2Outside) + int(edge3Outside)
    print("edgesOutside:", edgesOutside)

    # Exactly 2 of the edges should be fully outside for the check to succeed:
    return (edgesOutside == 2)

    ## If all are outside, there is no overlap with the mesh:
    #if edge0Outside and edge1Outside and edge2Outside and edge3Outside:
    #    return False

    ## Two neighbouring edges must lie entirely outside:
    #if edge0Outside and edge1Outside:
    #    return True
    #if edge1Outside and edge2Outside:
    #    return True
    #if edge2Outside and edge3Outside:
    #    return True
    #if edge3Outside and edge0Outside:
    #    return True
    #return False

#############################################################################
# Transforms

def randomScale( bm, i, extrusions, verts ):
    # Randomly discard some of the vertices:
    vertCoords = []
    for vert in verts:
        if random.random() > 0.3:
            vertCoords.append( vert.co )
    # Make sure you didn't discard all verts:
    if len(vertCoords) == 0:
        vertCoords = [vert.co for vert in verts]
    
    pivot = sum(vertCoords, mathutils.Vector()) / len(vertCoords)
    
    minScale = (extrusions-i)/extrusions*0.5 + 0.3
    maxScale = minScale + 0.6
    scale = random.uniform( minScale, maxScale )# - abs((float(i)-extrusions/2)/(extrusions/2))*0.4
    #bpy.ops.transform.resize(value=(scale,scale,scale));
    #bmesh.ops.scale(bm, vec=mathutils.Vector((scale,scale,scale)), verts=verts, space=mat)
    for vert in verts:
        vert.co = pivot + (vert.co-pivot)*scale


##############################################################################
# Cleaning operations

def deleteIslands( obj ):
    
    finalName = obj.name
    
    # split into loose parts
    bpy.ops.mesh.separate(type='LOOSE')
    # object mode
    bpy.ops.object.mode_set(mode='OBJECT')

    parts = bpy.context.selected_objects
    # sort by number of verts (last has most)
    parts.sort(key=lambda o: len(o.data.vertices))
    # print
    for part in parts:
        #print(part.name, len(part.data.vertices))
        part.select_set(True)

    parts[-1].select_set(False)
    largest = parts[-1]

    bpy.ops.object.delete()
    
    largest.name = finalName
    largest.select_set(True)
    parts = bpy.context.selected_objects
    for part in parts:
        print("still there:", part.name, len(part.data.vertices))

def removeNonManifolds( obj ):
    mesh = obj.data
    # Clean up possible non-manifolds
    cleanupAttempts = 0
    isClean = False
    while not isClean and cleanupAttempts < 15:
        isClean = True
        cleanupAttempts = cleanupAttempts + 1

        setMode( "EDIT" )
        bpy.ops.mesh.select_mode(type="VERT")
        bpy.ops.mesh.select_non_manifold()

        # Workaround to ensure the v.select flag is set correctly on the vertices:
        setMode( "OBJECT" )
        setMode( "EDIT" )

        nonManifoldVerts = [v for v in mesh.vertices if v.select]
        if len(nonManifoldVerts) > 0:
            #print("Cleaning non-manifold verts: ", len(nonManifoldVerts))
            isClean = False
            bpy.ops.mesh.dissolve_verts()
            bpy.ops.mesh.vert_connect_concave()

    deleteIslands( obj )

def cleanMesh( obj, remesh_octree_depth=4, remesh_scale=0.7, subdiv=True, add_crease=True ):

    mesh = obj.data
    setMode( "OBJECT" )

    # Remeshing
    remesh = obj.modifiers.new(type="REMESH", name="remesh")
    remesh.mode="SMOOTH"
    remesh.octree_depth = remesh_octree_depth
    remesh.scale = remesh_scale
    #remesh.sharpness = 0.1
    obj.select_set(True)
    bpy.context.view_layer.objects.active = obj
    bpy.ops.object.modifier_apply(modifier="remesh")
   
    # Subdivision
    if subdiv:
        obj.modifiers.new(type="SUBSURF", name="subdivision")
        setMode( "EDIT" )

        if add_crease:
            # Randomly add creased edges:
            for k in range(0,2):
                if random.random() > 0.2:
                    edgeLen = random.randint( 3, 10 )
                    crease = random.uniform(0.7,0.9)
                    # Start with a random vertex:
                    vert = mesh.vertices[random.randint(0,len(mesh.vertices)-1)]
                    for i in range(0,edgeLen):
                        edges = getEdges( mesh, vert )
                        edge = edges[random.randint(0,len(edges)-1)]
                        edge.crease = crease
                        vert = otherVert( mesh, edge, vert )

        setMode( "OBJECT" )
        bpy.ops.object.modifier_apply(apply_as="DATA",modifier="subdivision")

# From blender's object_print3d_utils addon:
def bmeshCopyFromObject(obj, transform=True, triangulate=True, apply_modifiers=False):
    """
    Returns a transformed, triangulated copy of the mesh
    """

    assert obj.type == 'MESH'

    if apply_modifiers and obj.modifiers:
        import bpy
        depsgraph = bpy.context.evaluated_depsgraph_get()
        obj_eval = obj.evaluated_get(depsgraph)
        me = obj_eval.to_mesh()
        bm = bmesh.new()
        bm.from_mesh(me)
        obj_eval.to_mesh_clear()
        del bpy
    else:
        me = obj.data
        if obj.mode == 'EDIT':
            bm_orig = bmesh.from_edit_mesh(me)
            bm = bm_orig.copy()
        else:
            bm = bmesh.new()
            bm.from_mesh(me)

    # TODO. remove all customdata layers.
    # would save ram

    if transform:
        bm.transform(obj.matrix_world)

    if triangulate:
        bmesh.ops.triangulate(bm, faces=bm.faces)

    return bm


import random
import math
import os
import sys
import argparse
import traceback
import array

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),"../Utils"))
from generalutils import *
from vtkutils import *

# Given a directory, check if the self intersection test returned true:
def checkSelfIntersection( curDir ):

    selfIntersectionFile = os.path.join( curDir, "selfIntersection" )
    if os.path.exists( selfIntersectionFile ):
        with open(selfIntersectionFile,'r') as f:
            si = f.readline()
            print("Self intersection:", si)

            # Clean folder from extracted surface
            removeFiles( curDir, "deformedSurface.stl" )

            if "True" in si:
                # Clean previously generated samples, if they exist
                removeFiles( curDir, "partialDeformedSurface" )
                removeFiles( curDir, "voxelized" )
                removeFiles( curDir, "stats.yml" )

                raise ValueError("Self intersection")

# From blender's object_print3d_utils addon:
def bmesh_check_self_intersect_object(obj):
    """
    Check if any faces self intersect

    returns an array of edge index values.
    """

    if not obj.data.polygons:
        return array.array('i', ())

    bm = bmeshCopyFromObject(obj, transform=False, triangulate=False)
    tree = mathutils.bvhtree.BVHTree.FromBMesh(bm, epsilon=0.00001)
    overlap = tree.overlap(tree)
    faces_error = {i for i_pair in overlap for i in i_pair}

    return array.array('i', faces_error)

# If the whole script is called
if __name__ == "__main__":

    import bpy
    import bmesh
    import mathutils

    d = os.path.dirname(bpy.data.filepath)
    print("path:", d, bpy.data.filepath)
    if not d in sys.path:
        sys.path.append(d)
        #sys.path.append(os.path.join(d, "..", "Utils"))
        sys.path.append(os.path.join(d, "Utils"))
    from blenderutils import setMode, bmeshCopyFromObject

    # Remove all arguments passed to Blender, only take those after the double dash '--' into account:
    argv = sys.argv
    if "--" in argv:
        argv = argv[argv.index("--") + 1:]
    else:
        argv = []

    parser = argparse.ArgumentParser(description="Checks if deformed configuration is self-intersecting, using Blender. Tested with Blender 2.82.")
    parser.add_argument("num", type=int, help="Number of samples to test")
    parser.add_argument("--start_num", type=int, default=0, help="First sample to test")
    parser.add_argument("--outdir", type=path, default=".", help="Folder where to save the random mesh.")
    parser.add_argument("--in_filename", type=str, default="deformed.vtu", help="Deformed volume filename, to parse in each folder.")
    parser.add_argument("--out_filename", type=str, default="selfIntersection", help="Output file, containing 'True' if mesh intersects itself or 'False' otherwise.")
    args = parser.parse_args(argv)

    # Loop through the list of provided samples    
    #samples_list = str2list(args.samples_list)
    startNum = int(args.start_num)
    num = int(args.num)
    samples_list = range( startNum, startNum + num )
    for i in samples_list:
        try:
            outdir = path( os.path.join( args.outdir, "{:06d}".format(i) ) )
            
            # Remove previous files
            removeFiles( outdir, "selfIntersection" )

            deformed = loadMesh( os.path.join( outdir, args.in_filename ) )
            deformedSurface = extractSurface( deformed )

            # Create the surface of the simulation result:
            deformedSurfaceFilename = os.path.join( outdir, "deformedSurface.stl" )
            writer = vtkSTLWriter()
            writer.SetFileName( deformedSurfaceFilename )
            writer.SetInputData( deformedSurface )
            writer.Update()

            # Work in blender
            setMode( "OBJECT" )        
            
            # Clear previous meshes:
            for o in bpy.data.objects:
                o.select_set(True)
            bpy.ops.object.delete()

            print("Loading file", deformedSurfaceFilename)
            if os.path.exists( deformedSurfaceFilename ):
                bpy.ops.import_mesh.stl( filepath=deformedSurfaceFilename )
            else:
                raise FileNotFoundError( deformedSurfaceFilename )
            
            obj = bpy.context.view_layer.objects.active 
            
            decimate = obj.modifiers.new(type="DECIMATE", name="decimate")
            decimate.decimate_type = "COLLAPSE"
            decimate.ratio = 0.05
            bpy.ops.object.modifier_apply(modifier="decimate")
            
            smooth = obj.modifiers.new(type="SMOOTH", name="smooth")
            smooth.factor = 2
            smooth.iterations = 5
            bpy.ops.object.modifier_apply(modifier="smooth")
            
            intersections = bmesh_check_self_intersect_object(obj)
            print("Intersections:", len(intersections))
            outfilepath = os.path.join( outdir, args.out_filename )
            with open(outfilepath, "w") as f:            
                if len(intersections) > 0:
                    f.write("True")
                else:
                    f.write("False")
        
        except KeyboardInterrupt as e:
            exit()
        except FileNotFoundError as e:
            print( "Could not check self intersection. File not found. Skipping sample:", i )
        except IOError as e:
            print("CAUGHT ERROR: '{}' - Skipping case {}".format(e,i))
        except:
            print( "Could not check self intersection. Skipping sample:", i )
            print( "Unexpected error:", sys.exc_info()[0] )
            print( traceback.format_exc() )
            
        

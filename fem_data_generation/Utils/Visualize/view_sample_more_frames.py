# state file generated using paraview version 5.8.1

# ----------------------------------------------------------------
# SELECT THE SAMPLE TO VISUALIZE
# ----------------------------------------------------------------
NUM_SAMPLE = 0
dataset_dir = '/home/eleonora/Desktop/UNIVR/PhD/Shared/SpringSimulation/Output/25032021/'

# Create full paths
import os
surface_filename = os.path.join(dataset_dir, f"{NUM_SAMPLE:06}", "surface.stl")

def0_filename = os.path.join(dataset_dir, f"{NUM_SAMPLE:06}", "deformed0.vtu")
def1_filename = os.path.join(dataset_dir, f"{NUM_SAMPLE:06}", "deformed1.vtu")
def2_filename = os.path.join(dataset_dir, f"{NUM_SAMPLE:06}", "deformed2.vtu")

pcd0_filename = os.path.join(dataset_dir, f"{NUM_SAMPLE:06}", "partialDeformedSurface0.vtp")
pcd1_filename = os.path.join(dataset_dir, f"{NUM_SAMPLE:06}", "partialDeformedSurface1.vtp")
pcd2_filename = os.path.join(dataset_dir, f"{NUM_SAMPLE:06}", "partialDeformedSurface2.vtp")

vox_filename = os.path.join(dataset_dir, f"{NUM_SAMPLE:06}", "voxelized.vts")

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# trace generated using paraview version 5.8.1
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1524, 803]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [0.03985189739614725, -0.028455533552914858, -0.09196559339761734]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [0.1473291595862766, 0.021059620425680613, -0.3438639891647179]
renderView1.CameraFocalPoint = [0.0398518973961467, -0.028455533552914906, -0.0919655933976174]
renderView1.CameraViewUp = [0.03697818255898963, 0.9774482218802186, 0.20791245166615965]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 0.07203167644497087
renderView1.Background = [1.0, 1.0, 1.0]
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'STL Reader'
surfacestl = STLReader(FileNames=[surface_filename])

# create a new 'XML Unstructured Grid Reader'
deformed1vtu = XMLUnstructuredGridReader(FileName=[def0_filename])
deformed1vtu.PointArrayStatus = ['stiffness']

# create a new 'XML PolyData Reader'
partialDeformedSurface1vtp = XMLPolyDataReader(FileName=[pcd1_filename])
partialDeformedSurface1vtp.PointArrayStatus = ['Normals', 'geodesic_distance', 'likelyhood', 'stiffness']

# create a new 'XML PolyData Reader'
partialDeformedSurface2vtp = XMLPolyDataReader(FileName=[pcd2_filename])
partialDeformedSurface2vtp.PointArrayStatus = ['Normals', 'geodesic_distance', 'likelyhood', 'stiffness']

# create a new 'XML PolyData Reader'
partialDeformedSurface0vtp = XMLPolyDataReader(FileName=[pcd0_filename])
partialDeformedSurface0vtp.PointArrayStatus = ['Normals', 'geodesic_distance', 'likelyhood', 'stiffness']

# create a new 'XML Unstructured Grid Reader'
deformed0vtu = XMLUnstructuredGridReader(FileName=[def0_filename])
deformed0vtu.PointArrayStatus = ['stiffness']

# create a new 'XML Unstructured Grid Reader'
deformed2vtu = XMLUnstructuredGridReader(FileName=[def2_filename])
deformed2vtu.PointArrayStatus = ['stiffness']

# create a new 'XML Structured Grid Reader'
voxelizedvts = XMLStructuredGridReader(FileName=[vox_filename])
voxelizedvts.PointArrayStatus = ['stiffness', 'preoperativeSurface', 'intraoperativeSurface0', 'intraoperativeSurface1', 'intraoperativeSurface2']

# create a new 'Threshold'
threshold1 = Threshold(Input=voxelizedvts)
threshold1.Scalars = ['POINTS', 'preoperativeSurface']
threshold1.ThresholdRange = [-0.014880175702273846, 0.0047]

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from partialDeformedSurface1vtp
partialDeformedSurface1vtpDisplay = Show(partialDeformedSurface1vtp, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
partialDeformedSurface1vtpDisplay.Representation = 'Points'
partialDeformedSurface1vtpDisplay.AmbientColor = [1.0, 1.0, 0.4980392156862745]
partialDeformedSurface1vtpDisplay.ColorArrayName = [None, '']
partialDeformedSurface1vtpDisplay.DiffuseColor = [1.0, 1.0, 0.4980392156862745]
partialDeformedSurface1vtpDisplay.PointSize = 4.0
partialDeformedSurface1vtpDisplay.OSPRayScaleArray = 'Normals'
partialDeformedSurface1vtpDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
partialDeformedSurface1vtpDisplay.SelectOrientationVectors = 'None'
partialDeformedSurface1vtpDisplay.ScaleFactor = 0.010993401519954205
partialDeformedSurface1vtpDisplay.SelectScaleArray = 'None'
partialDeformedSurface1vtpDisplay.GlyphType = 'Arrow'
partialDeformedSurface1vtpDisplay.GlyphTableIndexArray = 'None'
partialDeformedSurface1vtpDisplay.GaussianRadius = 0.0005496700759977102
partialDeformedSurface1vtpDisplay.SetScaleArray = ['POINTS', 'Normals']
partialDeformedSurface1vtpDisplay.ScaleTransferFunction = 'PiecewiseFunction'
partialDeformedSurface1vtpDisplay.OpacityArray = ['POINTS', 'Normals']
partialDeformedSurface1vtpDisplay.OpacityTransferFunction = 'PiecewiseFunction'
partialDeformedSurface1vtpDisplay.DataAxesGrid = 'GridAxesRepresentation'
partialDeformedSurface1vtpDisplay.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
partialDeformedSurface1vtpDisplay.ScaleTransferFunction.Points = [-0.6086382269859314, 0.0, 0.5, 0.0, 0.5665999054908752, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
partialDeformedSurface1vtpDisplay.OpacityTransferFunction.Points = [-0.6086382269859314, 0.0, 0.5, 0.0, 0.5665999054908752, 1.0, 0.5, 0.0]

# show data from partialDeformedSurface2vtp
partialDeformedSurface2vtpDisplay = Show(partialDeformedSurface2vtp, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
partialDeformedSurface2vtpDisplay.Representation = 'Points'
partialDeformedSurface2vtpDisplay.AmbientColor = [0.0, 1.0, 0.4980392156862745]
partialDeformedSurface2vtpDisplay.ColorArrayName = [None, '']
partialDeformedSurface2vtpDisplay.DiffuseColor = [0.0, 1.0, 0.4980392156862745]
partialDeformedSurface2vtpDisplay.PointSize = 4.0
partialDeformedSurface2vtpDisplay.OSPRayScaleArray = 'Normals'
partialDeformedSurface2vtpDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
partialDeformedSurface2vtpDisplay.SelectOrientationVectors = 'None'
partialDeformedSurface2vtpDisplay.ScaleFactor = 0.013645641505718231
partialDeformedSurface2vtpDisplay.SelectScaleArray = 'None'
partialDeformedSurface2vtpDisplay.GlyphType = 'Arrow'
partialDeformedSurface2vtpDisplay.GlyphTableIndexArray = 'None'
partialDeformedSurface2vtpDisplay.GaussianRadius = 0.0006822820752859116
partialDeformedSurface2vtpDisplay.SetScaleArray = ['POINTS', 'Normals']
partialDeformedSurface2vtpDisplay.ScaleTransferFunction = 'PiecewiseFunction'
partialDeformedSurface2vtpDisplay.OpacityArray = ['POINTS', 'Normals']
partialDeformedSurface2vtpDisplay.OpacityTransferFunction = 'PiecewiseFunction'
partialDeformedSurface2vtpDisplay.DataAxesGrid = 'GridAxesRepresentation'
partialDeformedSurface2vtpDisplay.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
partialDeformedSurface2vtpDisplay.ScaleTransferFunction.Points = [-0.5836802124977112, 0.0, 0.5, 0.0, 0.537192165851593, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
partialDeformedSurface2vtpDisplay.OpacityTransferFunction.Points = [-0.5836802124977112, 0.0, 0.5, 0.0, 0.537192165851593, 1.0, 0.5, 0.0]

# show data from surfacestl
surfacestlDisplay = Show(surfacestl, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
surfacestlDisplay.Representation = 'Surface With Edges'
surfacestlDisplay.AmbientColor = [0.3333333333333333, 0.6666666666666666, 1.0]
surfacestlDisplay.ColorArrayName = ['POINTS', '']
surfacestlDisplay.DiffuseColor = [0.3333333333333333, 0.6666666666666666, 1.0]
surfacestlDisplay.Opacity = 0.7
surfacestlDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
surfacestlDisplay.SelectOrientationVectors = 'None'
surfacestlDisplay.ScaleFactor = 0.014151300117373467
surfacestlDisplay.SelectScaleArray = 'STLSolidLabeling'
surfacestlDisplay.GlyphType = 'Arrow'
surfacestlDisplay.GlyphTableIndexArray = 'STLSolidLabeling'
surfacestlDisplay.GaussianRadius = 0.0007075650058686733
surfacestlDisplay.SetScaleArray = [None, '']
surfacestlDisplay.ScaleTransferFunction = 'PiecewiseFunction'
surfacestlDisplay.OpacityArray = [None, '']
surfacestlDisplay.OpacityTransferFunction = 'PiecewiseFunction'
surfacestlDisplay.DataAxesGrid = 'GridAxesRepresentation'
surfacestlDisplay.PolarAxes = 'PolarAxesRepresentation'

# show data from partialDeformedSurface0vtp
partialDeformedSurface0vtpDisplay = Show(partialDeformedSurface0vtp, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
partialDeformedSurface0vtpDisplay.Representation = 'Points'
partialDeformedSurface0vtpDisplay.AmbientColor = [1.0, 0.6666666666666666, 1.0]
partialDeformedSurface0vtpDisplay.ColorArrayName = [None, '']
partialDeformedSurface0vtpDisplay.DiffuseColor = [1.0, 0.6666666666666666, 1.0]
partialDeformedSurface0vtpDisplay.PointSize = 4.0
partialDeformedSurface0vtpDisplay.OSPRayScaleArray = 'Normals'
partialDeformedSurface0vtpDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
partialDeformedSurface0vtpDisplay.SelectOrientationVectors = 'None'
partialDeformedSurface0vtpDisplay.ScaleFactor = 0.009087764099240303
partialDeformedSurface0vtpDisplay.SelectScaleArray = 'None'
partialDeformedSurface0vtpDisplay.GlyphType = 'Arrow'
partialDeformedSurface0vtpDisplay.GlyphTableIndexArray = 'None'
partialDeformedSurface0vtpDisplay.GaussianRadius = 0.00045438820496201515
partialDeformedSurface0vtpDisplay.SetScaleArray = ['POINTS', 'Normals']
partialDeformedSurface0vtpDisplay.ScaleTransferFunction = 'PiecewiseFunction'
partialDeformedSurface0vtpDisplay.OpacityArray = ['POINTS', 'Normals']
partialDeformedSurface0vtpDisplay.OpacityTransferFunction = 'PiecewiseFunction'
partialDeformedSurface0vtpDisplay.DataAxesGrid = 'GridAxesRepresentation'
partialDeformedSurface0vtpDisplay.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
partialDeformedSurface0vtpDisplay.ScaleTransferFunction.Points = [-0.9780569076538086, 0.0, 0.5, 0.0, 0.17262883484363556, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
partialDeformedSurface0vtpDisplay.OpacityTransferFunction.Points = [-0.9780569076538086, 0.0, 0.5, 0.0, 0.17262883484363556, 1.0, 0.5, 0.0]

# ----------------------------------------------------------------
# finally, restore active source
SetActiveSource(threshold1)
# ----------------------------------------------------------------
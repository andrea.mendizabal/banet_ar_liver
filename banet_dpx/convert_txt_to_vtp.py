import os
import numpy as np
import vtk
from vtk.util import numpy_support
from vtk import *

file_idx_to_convert = [216, 85, 126]#[216, 126, 85][85, 216, 126][126, 216, 85][126, 85, 216]
disk_with_data = "/media/andrea/data"
j = 0

for NUM in file_idx_to_convert:
    # Load RGBD pcd
    pcdfile = os.path.join(disk_with_data, "patient3/imagesPatient3Liver1_0/pcd/segmented_and_clean", "pcd{:06d}.txt".format(NUM))
    complete_pcd = np.loadtxt(pcdfile).reshape((-1, 3))

    # Build VTK data
    VTK_data = numpy_support.numpy_to_vtk(num_array=complete_pcd, deep=True, array_type=vtk.VTK_FLOAT)
    VTK_obj = vtkPolyData()
    points = vtkPoints()
    verts = vtkCellArray()
    for i in range(len(complete_pcd)):
        points.InsertNextPoint(complete_pcd[i][0], complete_pcd[i][1], complete_pcd[i][2])
        verts.InsertNextCell(1, [points.GetNumberOfPoints() - 1])

    VTK_obj.SetPoints(points)
    VTK_obj.SetVerts( verts )

    # Write VTK to file
    filename = 'partialDeformedSurface' + str(j) + '.vtp'
    folder = "/media/andrea/data/post_doc_verona/banet/liver_patient3_3frames/{:06d}".format(NUM)
    if not os.path.exists(folder):
        os.makedirs(folder)
    filepath = os.path.join(folder, filename)
    print("Writing to {}".format(filepath))
    writer = vtkXMLPolyDataWriter()
    writer.SetFileName(filepath)
    writer.SetInputData(VTK_obj)
    writer.Update()

    j += 1


